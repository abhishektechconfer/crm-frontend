import React,{useEffect} from 'react';
import Avatar from '@material-ui/core/Avatar';
import Button from '@material-ui/core/Button';
import CssBaseline from '@material-ui/core/CssBaseline';
import TextField from '@material-ui/core/TextField';

import Link from '@material-ui/core/Link';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import { makeStyles } from '@material-ui/core/styles';
import Container from '@material-ui/core/Container';

import {postData} from '../FetchServices';
import {isMobile} from 'react-device-detect';

const useStyles = makeStyles((theme) => ({
  paper: {
    marginTop: theme.spacing(8),
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
  },
  avatar: {
    margin: theme.spacing(1),
    backgroundColor: theme.palette.secondary.main,
  },
  form: {
    width: '100%', // Fix IE 11 issue.
    marginTop: theme.spacing(1),
  },
  submit: {
    margin: theme.spacing(3, 0, 2),
    backgroundColor:'#1e83a9',
  },
  title:{
    color:'#1e83a9',
},
  image: {
    width:'40%',
    marginBottom:'10%',
   // marginTop:'3%',
  },
}));

export default function SignIn(props) {
  const classes = useStyles();

  const [useremail,setuseremail] = React.useState('')
  const [userpassword,setuserpassword] = React.useState('')
  const [test,setTest] = React.useState([])

  
function handleEnter(event) {
  if (event.keyCode === 13) {
    checkLogin()
  }
}

  const checkLogin =async() => {
	  
    let body = {
      'email':useremail,
      'password':userpassword,
      'lat':latitude,
      'lng':longitude,
      'ostype':ostype

    }
    console.log(body)

    let result = await postData('login',body)
    console.log(result)
    
    setTest(result.data.siteInfo)
    if (result.status==1){

     // alert(true)
      localStorage.setItem('LOGIN', JSON.stringify(result)) 
      props.history.replace({pathname:'/Linked', state:{data:test} })

    }else{
      alert ('invalid Id/Password')
      props.history.push({pathname:'/'})
    }
  }

  const [ostype,setOstype] = React.useState('')
const OstypeDetect = () => {
if (isMobile) {
    setOstype('mobile')
  }else{
    setOstype('PC')  
  }
}

  const [latitude,setLatitude] = React.useState('')
  const [longitude,setLongitude] = React.useState('')
  const [geoPoint,setgeoPoint] = React.useState('')

  function getLocation() {
    if (navigator.geolocation) {
      navigator.geolocation.getCurrentPosition(showPosition);
    } else { 
      alert("Geolocation is not supported by this browser.")
    }
  }
  
  function showPosition(position) {
    return(
     setLatitude( position.coords.latitude) ,
      setLongitude (position.coords.longitude)
     // setgeoPoint(position.coords.(latitude.concat(longitude)) )
    )
  }

  useEffect(()=>{
    getLocation() 
    OstypeDetect()
  })

  return (
    <Container component="main" maxWidth="xs">
      <CssBaseline />
      <div className={classes.paper}>
      <img src="./images/TrafikSol_Logo.jpg" className={classes.image}/>
        <Typography component="h1" variant="h5" className={classes.title}>
          Sign in To CRM View
        </Typography>
        <form className={classes.form} noValidate>
          <TextField
            variant="outlined"
            margin="normal"
            required
            fullWidth
            id="email"
            label="Email Address"
            autoComplete="email"
            autoFocus
            type="email"
            value={useremail}
            onChange={(event)=>setuseremail(event.target.value)}
            
          />
          <TextField
            variant="outlined"
            margin="normal"
            required
            fullWidth
            name="password"
            label="Password"
            type="password"
            id="password"
            autoComplete="current-password"
            value={userpassword}
            onChange={(event)=>setuserpassword(event.target.value)}
            onKeyDown={handleEnter}
          />
         
          <Button
            onClick={checkLogin}
            fullWidth
            variant="contained"
            color="primary"
            className={classes.submit}
          >
            Log In
          </Button>
          
        </form>
      </div>
      
    </Container>
  );
}