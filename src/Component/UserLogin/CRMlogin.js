import React, { useEffect } from 'react';
import Avatar from '@material-ui/core/Avatar';
import Button from '@material-ui/core/Button';
import CssBaseline from '@material-ui/core/CssBaseline';
import TextField from '@material-ui/core/TextField';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Checkbox from '@material-ui/core/Checkbox';
import Link from '@material-ui/core/Link';
import Paper from '@material-ui/core/Paper';
import InputBase from '@material-ui/core/InputBase';
import Grid from '@material-ui/core/Grid';
import LockOutlinedIcon from '@material-ui/icons/LockOutlined';
import MailOutlineIcon from '@material-ui/icons/MailOutline';
import Typography from '@material-ui/core/Typography';
import { makeStyles } from '@material-ui/core/styles';
import PermIdentityIcon from '@material-ui/icons/PermIdentity';

import { postData } from '../FetchServices';

const useStyles = makeStyles((theme) => ({
  root: {
    height: '100vh',
  },
  input: {
    marginBottom: '5%',
    backgroundColor: '#aaaaaa2e'
  },
  image: {
    width: '40%',
    marginBottom: '10%',
    // marginTop:'3%',
  },

  title: {
    color: '#1e83a9',
    marginBottom: '10%',
  },
  paper: {
    margin: theme.spacing(4),
    marginTop: theme.spacing(18),
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
  },
  paper1: {
    margin: theme.spacing(4),
    marginTop: theme.spacing(18),
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
  },
  grid: {
    backgroundColor: '#1e83a9'
  },
  avatar: {
    margin: theme.spacing(1),
    backgroundColor: theme.palette.secondary.main,
  },
  form: {
    width: '100%', // Fix IE 11 issue.
    marginTop: theme.spacing(1),
  },
  submit: {
    margin: theme.spacing(3, 0, 2),
    width: '50%',
    backgroundColor: '#1e83a9',
    color: 'white',
    borderRadius: '25px',
  },
  submit1: {
    margin: theme.spacing(3, 0, 2),
    width: '50%',
    color: 'white',
    border: '1px solid white',
    borderRadius: '25px',
  },
  typo: {
    color: 'white',
    padding: '13px'
  }
}));

export default function CRMlogin(props) {
  const classes = useStyles();

  const [useremail, setuseremail] = React.useState('')
  const [userpassword, setuserpassword] = React.useState('')
  const [test, setTest] = React.useState([])

  const checkLogin = async () => {

    let body = {
      'email': useremail,
      'password': userpassword,
      'lat': latitude,
      'lng': longitude,
      'ostype': 'test'

    }
    console.log(body)



    let result = await postData('login', body)
    console.log(result)

    setTest(result.data.siteInfo)


    if (result.status == 1) {

      // alert(true)
      localStorage.setItem('LOGIN', JSON.stringify(result))
      props.history.replace({ pathname: '/Linked', state: { data: test } })

    } else {
      alert('invalid Id/Password')
      props.history.push({ pathname: '/Login' })
    }
  }

  const [latitude, setLatitude] = React.useState('')
  const [longitude, setLongitude] = React.useState('')
  const [geoPoint, setgeoPoint] = React.useState('')

  function getLocation() {
    if (navigator.geolocation) {
      navigator.geolocation.getCurrentPosition(showPosition);
    } else {
      alert("Geolocation is not supported by this browser.")
    }
  }

  function showPosition(position) {
    return (
      setLatitude(position.coords.latitude),
      setLongitude(position.coords.longitude)
      // setgeoPoint(position.coords.(latitude.concat(longitude)) )
    )
  }

  useEffect(() => {
    getLocation()
  })



  return (
    console.log(latitude),
    console.log(longitude),
    console.log(geoPoint),
    <Grid container component="main" className={classes.root}>
      <CssBaseline />
      <Grid item xs={false} sm={4} md={5} className={classes.grid}>
        <div className={classes.paper1}>
          <img src="./images/TrafikSol_Logo.jpg" className={classes.image} />

          <Typography component="h1" variant="h4" className={classes.typo}>
            CRM View
          </Typography>
          <form className={classes.form} noValidate>
            <Typography className={classes.typo}>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean
              luctus ut est sed faucibus.</Typography>
            {/* <Button
              //type="submit"
              //fullWidth
              variant="outlined"
              //color="primary"
              className={classes.submit1}
            > Welcome Back!
            </Button>  */}

          </form>
        </div>
      </Grid>



      <Grid item xs={12} sm={8} md={7} component={Paper} elevation={6} square>
        <div className={classes.paper}>
          {/* <Avatar className={classes.avatar}>
            <LockOutlinedIcon />
          </Avatar> */}
          <Typography component="h1" variant="h5" className={classes.title}>
            Sign In to TrafikSol CRM
          </Typography>
          <form className={classes.form} noValidate>


            <Grid container spacing={2}>
              <Grid item xs={12} xs={12} sm={8} md={2}>
                <PermIdentityIcon />
              </Grid>

              <Grid item xs={12} xs={12} sm={8} md={8}>

                <InputBase
                  className={classes.input}
                  placeholder="Email"
                  required
                  fullWidth
                  value={useremail}
                  name="email"
                  type="email"
                  onChange={(event) => setuseremail(event.target.value)}
                  autoComplete="current-password"
                />
              </Grid>
            </Grid>



            <Grid container spacing={2}>
              <Grid item xs={12} xs={12} sm={8} md={2}>
                <LockOutlinedIcon />
              </Grid>

              <Grid item xs={12} xs={12} sm={8} md={8}>
                <InputBase
                  className={classes.input}
                  placeholder="Password"
                  required
                  fullWidth
                  type="password"
                  id="password"
                  value={userpassword}
                  onChange={(event) => setuserpassword(event.target.value)}
                  autoComplete="current-password"
                />
              </Grid>
            </Grid>

            <Button
              //type="submit"
              onClick={checkLogin}
              fullWidth
              variant="contained"
              color="primary"
              className={classes.submit}
            >
              Log In
            </Button>

          </form>
        </div>
      </Grid>
    </Grid>
  );
}