import React from 'react';
//import {BrowserRouter as Router,Route,Redirect} from 'react-router-dom'
import { HashRouter as Router, Route, Redirect } from 'react-router-dom'

import CRMlogin from './UserLogin/CRMlogin';
import Userlogin from './UserLogin/Userlogin';
import User_Dashboard from './Admin/Dashboard/User_Dashboard';
import LoginLinkedDash from './Admin/Dashboard/LoginLinkedDash';


export default function AdminRouter(props) {
    return (
        <Router>
            <div>
                <div>

                    <Redirect to="/" />

                    <Route path='/' exact strict component={Userlogin} history={props.history} />

                    <Route path='/test' exact strict component={CRMlogin} history={props.history} />
                    <Route path='/Linked' exact strict component={LoginLinkedDash} history={props.history} />
                    <Route path='/UserDashboard' exact strict component={User_Dashboard} history={props.history} />


                </div>
            </div>
        </Router>

    )
}