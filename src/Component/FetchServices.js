var axios = require("axios")
const BaseURL = "http://83.136.219.147/trafiksol/public/api/v1"

const postData = async (url, body) => {
  var response = await fetch(`${BaseURL}/${url}`, {

    method: "POST",
    mode: "cors",
    headers: {
      //'Authorization': "Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOlwvXC9hYXJvZ3lhaW5kaWEuaW5cL3RyYWZpa3NvbFwvcHVibGljXC9hcGlcL3YxXC9sb2dpbiIsImlhdCI6MTU5ODYyMTI1NywiZXhwIjoxNjAxMjEzMjU3LCJuYmYiOjE1OTg2MjEyNTcsImp0aSI6Im05eHptYkpVMG03b2RFSFoiLCJzdWIiOjEsInBydiI6Ijg3ZTBhZjFlZjlmZDE1ODEyZmRlYzk3MTUzYTE0ZTBiMDQ3NTQ2YWEifQ.5dZT8vs2pTL1PXbMKUkxZKVNAmKJJFI6czmtPQ_1Alg",
      "Content-Type": "application/json; charset=utf-8"
    },
    body: JSON.stringify(body)
  })
  var result = await response.json()
  return result
}

const postDataAndImage = async (url, formData, config) => {
  try {
    var response = await axios.post(`${BaseURL}/${url}`, formData, config)
    return response
  }
  catch (e) {
    console.log(e)
  }
}

const getData = async (url) => {
  var response = await fetch(`${BaseURL}/${url}`, {
    method: "GET",
    //mode:"cors",
    headers: {
      // "Authorization": "Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOlwvXC9sb2NhbGhvc3Q6ODA4MVwvdHJhZmlrc29sXC9wdWJsaWNcL2FwaVwvdjFcL2xvZ2luIiwiaWF0IjoxNTk4MzQ2NDA0LCJleHAiOjE2MDA5Mzg0MDQsIm5iZiI6MTU5ODM0NjQwNCwianRpIjoiYnBjYXdnYkFiUHVjUGZ6RSIsInN1YiI6MSwicHJ2IjoiODdlMGFmMWVmOWZkMTU4MTJmZGVjOTcxNTNhMTRlMGIwNDc1NDZhYSJ9.yJuEmEd-0U3QX5fl2j7atFN9gnnoMhBdopG1-qxsXtc",
      "Content-Type": "application/json; charset=utf-8"
    },
  })
  var result = await response.json()
  return result
}

export { postData, getData, postDataAndImage, BaseURL }