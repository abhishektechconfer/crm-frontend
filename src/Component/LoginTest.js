import React from 'react';
import Avatar from '@material-ui/core/Avatar';
import Button from '@material-ui/core/Button';
import CssBaseline from '@material-ui/core/CssBaseline';
import TextField from '@material-ui/core/TextField';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Checkbox from '@material-ui/core/Checkbox';
import Link from '@material-ui/core/Link';
import Paper from '@material-ui/core/Paper';
import InputBase from '@material-ui/core/InputBase';
import Grid from '@material-ui/core/Grid';
import LockOutlinedIcon from '@material-ui/icons/LockOutlined';
import MailOutlineIcon from '@material-ui/icons/MailOutline';
import Typography from '@material-ui/core/Typography';
import { makeStyles } from '@material-ui/core/styles';


const useStyles = makeStyles((theme) => ({
  root: {
    height: '100vh',
  },
  input:{
    marginBottom:'5%',
    backgroundColor:'#aaaaaa2e'
},
title:{
    color:'#1e83a9'
},
  paper: {
    margin: theme.spacing(8, 4),
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
  },
  paper1: {
    margin: theme.spacing(30, 4),
  //  marginTop:theme.spacing(20),
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
  },
 grid:{
    backgroundColor:'#1e83a9'
 },
  avatar: {
    margin: theme.spacing(1),
    backgroundColor: theme.palette.secondary.main,
  },
  form: {
    width: '100%', // Fix IE 11 issue.
    marginTop: theme.spacing(1),
  },
  submit: {
    margin: theme.spacing(3, 0, 2),
    width:'50%',
    backgroundColor:'#1e83a9',
    color: 'white',
    borderRadius: '25px',
  },
  submit1: {
    margin: theme.spacing(3, 0, 2),
    width:'50%',
    color:'white',
    border: '1px solid white',
    borderRadius: '25px',
  },
  typo:{
      color:'white',
      padding:'13px'
  }
}));

export default function SignInSide() {
  const classes = useStyles();



  const [timerDays, setTimerDays] = useState('00');
  const [timerHours, setTimerHours] = useState('00');
  const [timerMinutes, setTimerMinutes] = useState('00');
  const [timerSeconds, setTimerSeconds] = useState('00');

  let interval = useRef();

  const startTimer = () => {
    const countdownDate = new Date('Aug 25, 2020 00:00:00').getTime();

    interval = setInterval(() => {
        const now = new Date().getTime();
        const distance = countdownDate - now;
        
        const days = Math.floor(distance / (1000 * 60 * 60 * 24));
        const hours = Math.floor((distance % (1000 * 60 * 60 * 24) / (1000 * 60 * 60)));
        const minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
        const seconds = Math.floor((distance % (1000 * 60 )) / 1000);

        if (distance > 0){
          //stop timer
          clearInterval(interval.current);
        }else{
          //update timer
          setTimerDays(days);
           setTimerHours(hours);
           setTimerMinutes(minutes);
           setTimerSeconds(seconds);

        }

    }, 1000);
  };

  //componentDidMount
  useEffect(() => {
    startTimer();
    return () => {
      clearInterval(interval.current);
    }

  })


  return (
    <Grid container component="main" className={classes.root}>
      <CssBaseline />
      <Grid item xs={false} sm={4} md={5} className={classes.grid}>
      <div className={classes.paper1}>
          
          <Typography component="h1" variant="h4" className={classes.typo}>
            Welcome Back!
          </Typography>
          <form className={classes.form} noValidate>
            <Typography className={classes.typo}>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean
              luctus ut est sed faucibus.</Typography>
             <Button
              type="submit"
              //fullWidth
              variant="outlined"
              //color="primary"
              className={classes.submit1}
            > Log In
            </Button> 

          </form>
        </div>
      </Grid>



      <Grid item xs={12} sm={8} md={7} component={Paper} elevation={6} square>
        <div className={classes.paper}>
          {/* <Avatar className={classes.avatar}>
            <LockOutlinedIcon />
          </Avatar> */}
          <Typography component="h1" variant="h5" className={classes.title}>
            Sign Up to TrafikSol CRM
          </Typography>
          <form className={classes.form} noValidate>
          <Grid container spacing={2}>
          <Grid item xs={12} xs={12} sm={8} md={2}>
          <MailOutlineIcon/>
            </Grid>

            <Grid item xs={12} xs={12} sm={8} md={10}>
          
            <InputBase
            className={classes.input}
            placeholder="Email" 
            required
            fullWidth 
            name="email"
            type="email"
            autoComplete="current-password"
            />
            </Grid>
            </Grid>

            <Grid container spacing={2}>
          <Grid item xs={12} xs={12} sm={8} md={2}>
          <LockOutlinedIcon/>
            </Grid>

            <Grid item xs={12} xs={12} sm={8} md={10}>
             <InputBase
            className={classes.input}
            placeholder="Password" 
              required
              fullWidth
              name="password"
              type="password"
              id="password"
              autoComplete="current-password"
            />
         </Grid>
            </Grid>
            {timerDays}:{timerHours}:{timerMinutes}:{timerSeconds}
            <Button
              type="submit"
              fullWidth
              variant="contained"
              color="primary"
              className={classes.submit}
            >
              Sign Up
              
            </Button>
            
          </form>
        </div>
      </Grid>
    </Grid>
  );
}