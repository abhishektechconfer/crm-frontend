import React,{useEffect} from 'react';
import Link from '@material-ui/core/Link';
import { makeStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import {TableRow} from '@material-ui/core';
import Typography from '@material-ui/core/Typography';
import { Grid ,TableContainer,Paper,NativeSelect,Button,TablePagination} from '@material-ui/core';
import AttendLogDetail from './AttendLogDetail';

import {postData} from '../FetchServices';


const ColoredLine = ({ color }) => (
  <hr
      style={{
          color: color,
          backgroundColor: color,
          height: 5
      }}
  />
);

  const columns = [

    { id: 'name', label: 'Date', minWidth: 60 },
    { id: 'description', label: 'Login', minWidth: 60 },
    {  label: 'Time', minWidth: 60, align:'left' },
    { label: 'Geolocation', minWidth: 60 },
    
    { id: 'action', label: 'Action', minWidth: 70},
  
  ];


function preventDefault(event) {
  event.preventDefault();
}

const useStyles = makeStyles((theme) => ({
  seeMore: {
    marginTop: theme.spacing(3),
  },
  formControl: {
    marginTop: '6%',
   /// minWidth: 400,
    backgroundColor:'#aaaaaa2e',
    width:'100%',
  },
  new:{
    marginTop:'8%',
  },
  rowdata:{
    color:'#1e83a9'
  },
  root: {
    width: '100%',
  },
  container: {
    maxHeight: 440,
  },
  typo:{
    marginTop:'1%'
  },

}));

export default function AttendanceDetail(props) {
  const classes = useStyles();
  const [page, setPage] = React.useState(0);
  const [rowsPerPage, setRowsPerPage] = React.useState(10);
  
  const handleChangePage = (event, newPage) => {
    setPage(newPage);
  };

  const handleChangeRowsPerPage = (event) => {
    setRowsPerPage(+event.target.value);
    setPage(0);
  };

  const [rows,setRows]=React.useState([])

  const BaseURL="http://83.136.219.147/trafiksol/public/api/v1"

const postData1=async(url,body)=>{
    var response=await fetch(`${BaseURL}/${url}`,{
    method:"POST",
    mode:"cors",
    headers:{
     // "Authorization":"Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOlwvXC9hYXJvZ3lhaW5kaWEuaW5cL3RyYWZpa3NvbFwvcHVibGljXC9hcGlcL3YxXC9sb2dpbiIsImlhdCI6MTU5OTEyMzA1MiwiZXhwIjoxNjAxNzE1MDUyLCJuYmYiOjE1OTkxMjMwNTIsImp0aSI6IlJQVmttMHhmcFRzZHpHaGoiLCJzdWIiOjEsInBydiI6Ijg3ZTBhZjFlZjlmZDE1ODEyZmRlYzk3MTUzYTE0ZTBiMDQ3NTQ2YWEifQ.DFp2qevRXpL5DKJNSyrryVAnGD0BWXm-GiMVQeNq5LQ",
     "Authorization": "Bearer " + token,
     "Content-Type": "application/json; charset=utf-8"},
     body:JSON.stringify(body)})
     var result=await response.json()
  return result
}

  let rec=JSON.parse(localStorage.getItem('LOGIN'))
   console.log(rec)
   let token=rec.data.user.token;

const readAllRecords=async()=>{
  console.log('start')

     let result= await postData1('users/getAttendance')
   console.log(result)

  setRows(result.data.data)
  console.log(rows)
 }

 useEffect(()=>{
  readAllRecords()
},[])


const View=()=>{
  return (
    console.log(rows),
    <React.Fragment>


    <div className={classes.new}>
   <Paper>
   <Grid container>
       <Grid item xs={12} sm={12} md={12}>
       <Typography  variant="h6" className={classes.typo}>Attendance Details</Typography>

       </Grid>
     </Grid>
     <ColoredLine color="#1e83a9" />
      </Paper>
      
      <Paper className={classes.root}>

<TableContainer className={classes.container}>
<Table stickyHeader aria-label="sticky table">
  <TableHead>
    <TableRow>
      {columns.map((column) => (
        <TableCell
          key={column.id}
          align={column.align}
          style={{ minWidth: column.minWidth }}
        ><b>
          {column.label}
          </b>
        </TableCell>
      ))}
    </TableRow>
  </TableHead>
 
  <TableBody>

        {rows.slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage).map(row=>(
          <TableRow key={row.id}>
       
     <TableCell className={classes.rowdata}>{row.date_column1}</TableCell>
                 <TableCell className={classes.rowdata}>{row.entry_type}</TableCell>
                 <TableCell className={classes.rowdata}>{row.time_column}</TableCell>
                 <TableCell className={classes.rowdata}>{row.geo_point}</TableCell>
     <TableCell className={classes.rowdata}>
     <Button onClick={()=>handleClick(<AttendLogDetail viewDate={row.date_column1} date={row.date_column} changeView={props.changeView}/>)} color="primary" className={classes.button}>
    View</Button>
   </TableCell>
   </TableRow>
      ))}

        </TableBody>
</Table>
</TableContainer>
<TablePagination
rowsPerPageOptions={[10, 25, 100]}
component="div"
count={rows.length}
rowsPerPage={rowsPerPage}
page={page}
onChangePage={handleChangePage}
onChangeRowsPerPage={handleChangeRowsPerPage}
/>

</Paper>
      </div>

       </React.Fragment>
  )}

  const handleClick=(view)=>{
    props.changeView(view)
    }

  return (
    <div>
         {View()}
    </div>
   );
}
