import React,{useEffect} from 'react';
import Avatar from '@material-ui/core/Avatar';

import Button from '@material-ui/core/Button';
import CssBaseline from '@material-ui/core/CssBaseline';

import LockOutlinedIcon from '@material-ui/icons/LockOutlined';
import {Hidden ,Typography,Container,InputBase,Grid,Paper,Input }
 from '@material-ui/core';
import ArrowBackIosIcon from '@material-ui/icons/ArrowBackIos';

import { makeStyles } from '@material-ui/core/styles';
import {postDataAndImage} from '../FetchServices';
import Profile from './Profile';

const ColoredLine = ({ color }) => (
  <hr
      style={{
          color: color,
          backgroundColor: color,
          height: 5,
          marginBottom:'7%'
      }}
  />
); 

const useStyles = makeStyles((theme) => ({
    upload:{
      backgroundColor:'#1e83a9',
      color:'#fff',
      borderRadius:'0px',
      boxShadow:'0px',
      height:'33px',
  
        '&:hover': {
          // color:'primary',
           fontStyle:"bold",
           backgroundColor:'#1e83a9',
           },
      },
  paper: {
    marginTop: theme.spacing(4),
    display: 'flex',
    flexDirection: 'column',
    width:'100%',
    //marginLeft:'10%'
   //alignItems: 'center',
  },
  image: {
    display: 'none',
  },
  avatar: {
    padding:14,
    float:'left',
    width: 100,
    height: 100,
    marginBottom: '50px',
  },

  form: {
    width: '100%', // Fix IE 11 issue.
    marginTop: theme.spacing(3),
  },
  submit: {
    marginTop: '2%',
    marginBottom: '7%',
    backgroundColor:'#1e83a9',
    color:'#fff',
    //width:'40%'
   // float:'right',
    //marginLeft:'9%',
    
    '&:hover': {
  backgroundColor:'#1e83a9',
  fontStyle:"bold" 
  },
  },
  field1:{
float:'left'
  },
  backIcon:{
    fontSize:'medium'
  },
  title: {
    flexGrow: 1,
    textAlign:'center',
    padding:'5px',
   // marginBottom: '10%',
    //backgroundColor:'#1e83a9',
    color:'black',
    //marginLeft: '4%',
  },
  formControl: {
    marginBottom: theme.spacing(3),
   /// minWidth: 400,
    backgroundColor:'#aaaaaa2e',
    width:'100%',
    
  },
   input:{
    marginBottom: theme.spacing(3),
    backgroundColor:'#aaaaaa2e'
},
addRespons: {
    backgroundColor:'#1e83a9',
    color: 'white',
    padding: '0px',
    textAlign: 'center',
    fontSize: '12px',
    marginTop: '16%',
  },
typo:{
    textAlign:'left'
},
new:{
  //marginLeft: theme.spacing(2),
  marginRight: theme.spacing(2),
  width:'100%',
},
newRespon:{
  marginLeft: theme.spacing(2),
  marginRight: theme.spacing(2),
  width:'100%',
},


}));


export default function EditProfile(props) {
  const classes = useStyles();
  
  const BaseURL="http://83.136.219.147/trafiksol/public/api/v1"

  const postData1=async(url,body)=>{
      var response=await fetch(`${BaseURL}/${url}`,{
      method:"POST",
      mode:"cors",
      headers:{
       // "Authorization":"Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOlwvXC9hYXJvZ3lhaW5kaWEuaW5cL3RyYWZpa3NvbFwvcHVibGljXC9hcGlcL3YxXC9sb2dpbiIsImlhdCI6MTU5OTEyMzA1MiwiZXhwIjoxNjAxNzE1MDUyLCJuYmYiOjE1OTkxMjMwNTIsImp0aSI6IlJQVmttMHhmcFRzZHpHaGoiLCJzdWIiOjEsInBydiI6Ijg3ZTBhZjFlZjlmZDE1ODEyZmRlYzk3MTUzYTE0ZTBiMDQ3NTQ2YWEifQ.DFp2qevRXpL5DKJNSyrryVAnGD0BWXm-GiMVQeNq5LQ",
       "Authorization": "Bearer " + token,
       "Content-Type": "application/json; charset=utf-8"},
       body:JSON.stringify(body)})
       var result=await response.json()
    return result
  }
  
    let rec=JSON.parse(localStorage.getItem('LOGIN'))
     console.log(rec)
     let token=rec.data.user.token;

 
  const [phone,setPhone] = React.useState('')
  const [getPicture, setPicture]=React.useState('')
  const [getPicturePath, setPicturePath]=React.useState('')

  const handlePicture=(event)=>{
    setPicturePath(URL.createObjectURL(event.target.files[0]))
    setPicture(event.target.files[0])
  
   }

   const readProfileData=async()=>{
    const body={
      'name' : '',
      'file[]':'',
      'phone':'',
      
                }
   console.log(body)
               
   let result=await postData1('editMyProfile',body)
   console.log(result)
  
   setPhone(result.data.phone)
   setPicturePath(result.data.image)
 
  }
   

   const handleEdit=async()=>{
    //alert('hii')
    if(getPicture==''){
      const body={
            //'name' : name,
            'file[]':'',
            'phone':phone,
            
                      }
         console.log(body)
                     
         let result=await postData1('editMyProfile',body)
         console.log(result)
                            
        if(result)
        {
          //alert("Your Profile has been Edited..")
       // localStorage.setItem('PROFILE', JSON.stringify(result)) 
        clearValues()  
          }
    else
         {
         alert("Fail to Edit profile...")  } 

        } else{
          
          const formData=new FormData()
         /// formData.append('name',name)
          formData.append('file[]',getPicture)
          formData.append('phone',phone)
          
          
          const config={headers:{
            //'Authorization':'Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOlwvXC9hYXJvZ3lhaW5kaWEuaW5cL3RyYWZpa3NvbFwvcHVibGljXC9hcGlcL3YxXC9sb2dpbiIsImlhdCI6MTU5OTEyMzA1MiwiZXhwIjoxNjAxNzE1MDUyLCJuYmYiOjE1OTkxMjMwNTIsImp0aSI6IlJQVmttMHhmcFRzZHpHaGoiLCJzdWIiOjEsInBydiI6Ijg3ZTBhZjFlZjlmZDE1ODEyZmRlYzk3MTUzYTE0ZTBiMDQ3NTQ2YWEifQ.DFp2qevRXpL5DKJNSyrryVAnGD0BWXm-GiMVQeNq5LQ',
            'Authorization' : "Bearer " + token,
            'content-type':'multipart/form-data'}}
          const result=await postDataAndImage('editMyProfile',formData,config)
  
          console.log(formData)
          console.log(result)
         
          if(result)
                {alert("Fail To Edit Data..")
                clearValues()
                  }
          else
                 {
                 //alert("Data And File Edited...")
                 clearValues()
                 }
            }}
            
            const clearValues=()=>{
                setPhone('')
                setPicture('')
                setPicturePath('')
                      }  
                      useEffect(()=>{
                        readProfileData()
                    },[])

   const View=()=>{
    return (
    <Container component="main">
      <CssBaseline />
      
      <div>

      <Paper className={classes.paper}>
      <Hidden mdDown implementation="css">
       <div className={classes.new}>
       
        <form className={classes.form} noValidate>

        <Grid container spacing={2}>
         <Grid item  md={2}>
        
        <Button className={classes.upload}
        onClick={()=>handleClick(<Profile changeView={props.changeView}/>)}>
      <ArrowBackIosIcon className={classes.backIcon}/>
 Back</Button>
        </Grid>

        <Grid item md={8}>
           <Typography component="h1" variant="h6" color="inherit" noWrap className={classes.title}>
          Edit Profile
          </Typography>
          </Grid>

</Grid>
          <ColoredLine color="#1e83a9" />

 <Grid container maxWidth="md">

<Grid item xs={12} sm={12} md={12}>

  
  <Grid container spacing={2}>
<Grid item xs={12} sm={6} md={3}>
  <Typography>Phone</Typography>
  </Grid>
  <Grid item xs={12} sm={6} md={7}>
  <Input
  value={phone}
  onChange={(event)=>setPhone(event.target.value=Math.max(0, parseInt(event.target.value) ).toString().slice(0,10) )}
className={classes.input}
placeholder="              Phone" 
required
fullWidth 
type="number"
inputProps={{ 'aria-label': 'description' }} />
  </Grid>
  </Grid>


  <Grid container spacing={3}>
    <Grid item xs={12} sm={2} md={3}>
    <Typography  className={classes.field}>Image</Typography>
    </Grid>
    <Grid item xs={12} sm={6} md={5}>
    <Input
         value={getPicturePath}
       // onChange={(event)=>handlePicture(event)}
       disabled 
       id="standard-disabled"
            className={classes.input}
            placeholder="Image" 
            required
            fullWidth 
            type="text"
            inputProps={{ 'aria-label': 'description' }} />
       </Grid>
       
    {/* <Grid item xs={12} sm={6} md={4}>
    { <Avatar variant="square" alt="Picture" src={getPicturePath} className={classes.bigAvatar} value={getPicturePath} style={{width:120,height:100}}/> }
      </Grid> */}
    <Grid item xs={12} sm={2} md={2}>
    <input
        required
        accept="image/*"
        className={classes.image}
        id="Picture"
        multiple
        type="file"
        onChange={(event)=>handlePicture(event)}
        fullWidth
      />
      <label htmlFor="Picture">
        <Button variant="contained" component="span" className={classes.upload} >
         Upload
        </Button>
      </label>
    </Grid>

    
</Grid>


  <Grid container spacing={2}>
<Grid item xs={12} sm={6} md={5}>
  <Typography></Typography>
  </Grid>
  <Grid item xs={12} sm={12} md={12}>
  <Button
          onClick={handleEdit}
          variant="contained"
          color="green"
          className={classes.submit}
        >
          Save
        </Button>
  </Grid>
  </Grid>

  

</Grid>

</Grid> 

        </form>
        </div></Hidden>
        <Hidden mdUp implementation="css">
        

<Grid container spacing={2}>
 <Grid item xs={3} sm={2}>
 <button size="small" aria-label="add" className={classes.addRespons}>
     <ArrowBackIosIcon onClick={()=>handleClick(<Profile changeView={props.changeView}/>)} /></button>
</Grid>

<Grid item xs={6} sm={8}>
   <Typography component="h1" variant="h6" color="inherit" noWrap className={classes.title}>
  Edit Profile
  </Typography>
  </Grid>

</Grid>
  <ColoredLine color="#1e83a9" />
  <div className={classes.newRespon}>
        <form className={classes.form} noValidate>
<Grid container maxWidth="md">

<Grid item xs={12} sm={12} md={12}>


<Grid container spacing={2}>
<Grid item xs={12} sm={2}>
<Typography style={{float:'left'}}>Phone</Typography>
</Grid>
<Grid item xs={10} sm={10}>
<Input
value={phone}
onChange={(event)=>setPhone(event.target.value=Math.max(0, parseInt(event.target.value) ).toString().slice(0,10) )}
className={classes.input}
placeholder="              Phone" 
required
fullWidth 
type="number"
inputProps={{ 'aria-label': 'description' }} />
</Grid>
</Grid>


<Grid container spacing={3}>
<Grid item xs={12} sm={2} >
<Typography  className={classes.field1}>Image</Typography>
</Grid>
<Grid item xs={10} sm={7}>
<Input
 value={getPicturePath}
// onChange={(event)=>handlePicture(event)}
disabled 
id="standard-disabled"
    className={classes.input}
    placeholder="Image" 
    required
    fullWidth 
    type="text"
    inputProps={{ 'aria-label': 'description' }} />
</Grid>

{/* <Grid item xs={12} sm={6} md={4}>
{ <Avatar variant="square" alt="Picture" src={getPicturePath} className={classes.bigAvatar} value={getPicturePath} style={{width:120,height:100}}/> }
</Grid> */}
<Grid item xs={6} sm={2} md={3}>
<input
required
accept="image/*"
className={classes.image}
id="Picture"
multiple
type="file"
onChange={(event)=>handlePicture(event)}
fullWidth
/>
<label htmlFor="Picture">
<Button variant="contained" component="span" className={classes.upload} >
 Upload
</Button>
</label>
</Grid>


</Grid>


<Grid container spacing={2}>
<Grid item xs={12} sm={6} md={5}>
<Typography></Typography>
</Grid>
<Grid item xs={12} sm={12} md={12}>
<Button
  onClick={handleEdit}
  variant="contained"
  color="green"
  className={classes.submit}
>
  Save
</Button>
</Grid>
</Grid>



</Grid>

</Grid> 

</form>
        </div>
 
</Hidden>
        </Paper>
      
      </div>
    </Container>
   );
  }
  const handleClick=(view)=>{
    props.changeView(view)
    }
  
  return (
    <div>
         {View()}
    </div>
   );
  }