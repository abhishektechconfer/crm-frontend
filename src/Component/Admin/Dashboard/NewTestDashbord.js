import React,  { useEffect } from 'react';
import PropTypes from 'prop-types';

import {  makeStyles, useTheme  } from '@material-ui/core/styles';

import {Drawer,
        Hidden,
        List,
        Divider ,
        Typography,
        Toolbar,
        CssBaseline,
        AppBar,
        Grid,
        Avatar,
        Button,

        } from '@material-ui/core';

import IconButton from '@material-ui/core/IconButton';
import MenuIcon from '@material-ui/icons/Menu';
import ExitToAppIcon from '@material-ui/icons/ExitToApp';
import AccountCircle from '@material-ui/icons/AccountCircle';
import Badge from '@material-ui/core/Badge';
import NotificationsIcon from '@material-ui/icons/Notifications';
import MenuItem from '@material-ui/core/MenuItem';
import Menu from '@material-ui/core/Menu';
import TrackChangesIcon from '@material-ui/icons/TrackChanges';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import {TablePagination,Box,Link,TableRow,
  Select,Paper,InputBase} from '@material-ui/core';
import Tooltip from '@material-ui/core/Tooltip';

import ClickAwayListener from '@material-ui/core/ClickAwayListener';
import User_MainList from './User_MainList';
import Notifications from '../Notifications';
import Create from '../Create';
import Dashboard from '../dash/Dashboard';



///////////////////////////////////////////////////////////

const drawerWidth = 250;

const useStyles = makeStyles(theme => ({
  root: {
    display: 'flex',
  },

  root1: {
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
     minHeight: 'fit-content',
     marginTop:20
  },
  avatar: {
    width: 60,
    height: 60
  },
  avat:{
    marginLeft:15
  },
  title:{
    flexGrow: 1,
    textAlign:'center',
    marginTop:'2%',
    color:'#1e83a9',
  },

  appBar: {
    backgroundColor:'white',
     //backgroundColor:'#1e83a9',
    //  backgroundColor:'#f5f5f5',
   zIndex: theme.zIndex.drawer + 1,
   opacity: '1',

  },
  drawer: {
    width: drawerWidth,
    flexShrink: 0,
  },
  drawerPaper: {
    width: drawerWidth,
  },
  content: {
    flexGrow: 1,
    padding: theme.spacing(3),
  },
  toolbar: theme.mixins.toolbar,

  menuButton: {
    marginRight: theme.spacing(2),
    color:'#1e83a9',
  },
  grow: {
    flexGrow: 1,
  },

  notification: {
  display: 'flex',
  },
  image: {
    width: '84%',
    marginTop: '2%',
   // marginRight: '50%',
   // marginBottom:'6%',
  },
  
  /**Header Style** */
   paper: {
     margin: theme.spacing(8, 4),
     display: 'flex',
     flexDirection: 'column',
     alignItems: 'center',
   },
  

}));



/////////////////////////////////////////////////////////////////////////////////

export default function User_Dashboard(props) {
    const { container } = props;

    const classes = useStyles();
    const [view,setView] = React.useState(
    
      <Dashboard/>
      
    )

    const [siteId,setSiteId] = React.useState('')

    const [username,setUserName] = React.useState('')
    const [address,setAddress] = React.useState('')
     const [sitename,setSiteName] = React.useState('')
     const [aliasname,setAliasName] = React.useState('')
     const [image,setImage] = React.useState('')
     

     const handleLogout=async()=>{
      //alert('hii')
         
      let body = {
        'lat':latitude,
        'lng':longitude,
      }
      console.log(body)

         let result=await postData1('apilogout',body)
         console.log(result)
         if(result)
        {
        localStorage.clear();
        props.history.replace({pathname:'/Login'})
          }
    else
         { alert("Fail to Logout...")  } 

        } 
        
    
  const [latitude,setLatitude] = React.useState('')
  const [longitude,setLongitude] = React.useState('')

  function getLocation() {
    if (navigator.geolocation) {
      navigator.geolocation.getCurrentPosition(showPosition);
    } else { 
      alert("Geolocation is not supported by this browser.")
    }
  }
  
  function showPosition(position) {
    return(
     setLatitude( position.coords.latitude) ,
      setLongitude (position.coords.longitude)
    )
  }  

     const ChangeSite=async(id)=>{

      const body={
        'site_id':id,
                  }
            console.log(body)
            
         let result= await postData1('sites/getSiteInfoBySiteId',body)
       console.log(result)
       setAddress(result.data.address1)
       setSiteName(result.data.name)
       setAliasName(result.data.alias_name)
       setImage(result.data.client_logo)
         

       if(result.status==1){
         setOnn(false)
        
       }
     }


  

    const theme = useTheme();

    const handleDrawerToggle = () => {
        setMobileOpen(!mobileOpen);
      };

    const changeView=(View)=>{
      if(View==="Logout")
      { 
    props.history.replace({pathname:'/Login'})
      }
     else
     {
       
      setView(View)
     }
      }

    const [mobileOpen, setMobileOpen] = React.useState(false);
                  const [onn, setOnn] = React.useState(false);
                
                  const handleOff = () => {
                    setOnn(false);
                  }; 
                  const [open, setOpen] = React.useState(false);

                  const [up, setUp] = React.useState(false);

                  const handleTooltipClose = () => {
                    setUp(false);
                  };
                
                  const handleTooltipOpen = () => {
                    setUp(true);
                  };
                             

    useEffect(function(){
    try{
      readProfileData()
        getLocation() 
        //setSla(props.sla_end)
        setSiteId(props.location.state.site_id)
        ChangeSite(props.location.state.site_id)
    }
    catch(e){}

},[])

localStorage.setItem('SITEID', JSON.stringify(siteId)) 


const readProfileData=async()=>{
  const body={
    'name' : '',
    'file[]':'',
    'phone':'',
    
              }
 console.log(body)           
 let result=await postData1('editMyProfile',body)
 console.log(result)  
 setUserName(result.data.name)
}


const columns = [
  
  {
    id: 'ticketname',  label: 'Ticket Name',  minWidth: 70,
    format: (value) => value.toLocaleString('en-US'),
  },
  {
    id: 'category',label: 'Category', minWidth: 70, format: (value) => value.toLocaleString('en-US'),
  },
  {
    id: 'priority', label: 'Priority',
    minWidth: 70,
    format: (value) => value.toLocaleString('en-US'),
  },
  {
    id: 'stretch',label: 'Stretch Point', minWidth: 70},
  {
    id: 'status', label: 'Status', minWidth: 70,  format: (value) => value.toLocaleString('en-US'),
  },
  {
    id: 'action', label: 'Action',minWidth: 70, 
  },
];


const [rows,setRows]=React.useState([])

const readAllRecords=async(id)=>{
  console.log('start')

  var body={
    "type":"all",
    "search":"",
    "site_id":id
   }
   console.log(body)

     let result= await postData1('tickets/getTicketByType',body)
   console.log(result)

  setRows(result.data.data)
 }

 useEffect(()=>{
  readAllRecords(props.location.state.site_id)
},[])


const [page, setPage] = React.useState(0);
const [rowsPerPage, setRowsPerPage] = React.useState(10);

const handleClick=(view)=>{
  props.changeView(view)
  }


const handleChangePage = (event, newPage) => {
  setPage(newPage);
};

const handleChangeRowsPerPage = (event) => {
  setRowsPerPage(+event.target.value);
  setPage(0);
};

 
const BaseURL="http://83.136.219.147/trafiksol/public/api/v1"

const postData1=async(url,body)=>{
    var response=await fetch(`${BaseURL}/${url}`,{
    method:"POST",
    mode:"cors",
    headers:{
     // "Authorization":"Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOlwvXC9hYXJvZ3lhaW5kaWEuaW5cL3RyYWZpa3NvbFwvcHVibGljXC9hcGlcL3YxXC9sb2dpbiIsImlhdCI6MTU5OTEyMzA1MiwiZXhwIjoxNjAxNzE1MDUyLCJuYmYiOjE1OTkxMjMwNTIsImp0aSI6IlJQVmttMHhmcFRzZHpHaGoiLCJzdWIiOjEsInBydiI6Ijg3ZTBhZjFlZjlmZDE1ODEyZmRlYzk3MTUzYTE0ZTBiMDQ3NTQ2YWEifQ.DFp2qevRXpL5DKJNSyrryVAnGD0BWXm-GiMVQeNq5LQ",
     "Authorization": "Bearer " + token,
     "Content-Type": "application/json; charset=utf-8"},
     body:JSON.stringify(body)})
     var result=await response.json()
  return result
}

  let rec=JSON.parse(localStorage.getItem('LOGIN'))
   console.log(rec)
   let token=rec.data.user.token;

   /* ---- Current date ----- */
   var today = new Date();
var dd = today.getDate();
var mm = today.getMonth()+1; 
var yyyy = today.getFullYear();
if(dd<10) 
{ dd='0'+dd;} 

if(mm<10) 
{   mm='0'+mm;} 
today = mm+'-'+dd+'-'+yyyy;

  ////////////////////////////////////////////////////////////////////////
  const drawer = (
    <div className={classes.root1}>

   <Typography  variant="h6" >Welcome</Typography>
   <Typography  variant="h6" >{username}!</Typography>
    {/* <Avatar alt="Person" className={classes.avatar}  src="" />
      <Link href="/Dashboard">
            <img style={{width:100, height:100, alignItems:'center'}} src="./images/logo.png" />
            </Link>      */}
      <List>
        <User_MainList changeView={changeView}/>
      </List>
    </div>
  );



  return (
    console.log(sitename),

    <div className={classes.root}>
      <CssBaseline />
      <AppBar className={classes.appBar}>
        <Toolbar>

        <Grid container spacing={3}>

          <Grid item xs={2} sm={2} md={2}>

          <Hidden smDown implementation="css">
          <img src="./images/TrafikSol_Logo.jpg" className={classes.image}/>
         </Hidden>

        <Hidden mdUp implementation="css">
        <IconButton
            color="inherit"
            aria-label="open drawer"
            edge="start"
            onClick={handleDrawerToggle}
            className={classes.menuButton}
          >
            <MenuIcon />
          </IconButton>
          </Hidden>

        </Grid>

         <Grid item xs={5} sm={4} md={8}>
         <Hidden smDown implementation="css">
          <Typography variant="h6" noWrap className={classes.title}>
           TrafikSol CRM
          </Typography>
          </Hidden>
          <Hidden mdUp implementation="css">
          <img src="./images/TrafikSol.jpg" className={classes.image}/>
         </Hidden>
          </Grid>


           <div className={classes.grow} />
           <Grid item xs={5} sm={4} md={2}>
          <div className={classes.notification}>
          <Tooltip disableFocusListener title="Change your Site">
          <TrackChangesIcon style={{color:"#1e83a9",marginTop:'6%'}} onClick={()=>(props.history.push({pathname:'/Linked' }) )} />
            </Tooltip>
           
            <Notifications changeView={changeView} />
           
           <Create changeView={changeView}/>
            
            <Tooltip disableFocusListener title="Logout">
           <IconButton
              edge="end"
              aria-label="account of current user"
              aria-haspopup="true"
              color="inherit"
            >
              {/* <ExitToAppIcon onClick={()=>changeView('Logout')} /> */}
              <ExitToAppIcon style={{color:"#1e83a9"}} onClick={handleLogout} />
            </IconButton>
          </Tooltip>

          <Grid container spacing={2}>

<Grid item>
    <ClickAwayListener onClickAway={handleTooltipClose}>
      <div>
        <Tooltip
          PopperProps={{
            disablePortal: true,
          }}
          onClose={handleTooltipClose}
          open={open}
          disableFocusListener
          title="Add"
        >
          <Button onClick={handleTooltipOpen}></Button>
        </Tooltip>
      </div>
    </ClickAwayListener>
  </Grid>

  </Grid>

          </div>
          </Grid>

           </Grid>

        </Toolbar>
      </AppBar>


     {/* /////////////////////////// For Web////////////////////////////////// */}
     <Hidden smDown implementation="css">

      <Drawer
        className={classes.drawer}
        variant="permanent"
        classes={{
          paper: classes.drawerPaper,
        }}
      >
        <div className={classes.toolbar} />

         {drawer}


      </Drawer>
    </Hidden>

 {/* ///////////////////////////// For Mobile //////////////////////////////// */}

        <Hidden smUp implementation="css">
          <Drawer
            container={container}
            variant="temporary"
            anchor={theme.direction === 'rtl' ? 'right' : 'left'}
            open={mobileOpen}
            onClose={handleDrawerToggle}
            classes={{
              paper: classes.drawerPaper,
            }}
            ModalProps={{
              keepMounted: true, // Better open performance on mobile.
            }}
          >
            {drawer}
          </Drawer>
        </Hidden>


      <main className={classes.content}>
        <div className={classes.toolbar} />
        
       {view}
       <Paper className={classes.root}>
    <Typography component="h1" variant="h6" color="inherit" noWrap className={classes.title}>
            Open Ticket
          </Typography>
          {/* <ColoredLine color="#1e83a9" /> */}
    <div className={classes.new1}>

  
<Divider/>
{/* <div className={classes.search}>
            <div className={classes.searchIcon}>
              <SearchIcon />
            </div>
            <InputBase
              placeholder="Search…"
              classes={{
                root: classes.inputRoot,
                input: classes.inputInput,
              }}
              inputProps={{ 'aria-label': 'search' }}
            />
          </div> */}

      <TableContainer className={classes.container}>
        <Table stickyHeader aria-label="sticky table">
          <TableHead>
            <TableRow>
              {columns.map((column) => (
                <TableCell
                  key={column.id}
                  align={column.align}
                  style={{ minWidth: column.minWidth }}
                >
                  {column.label}
                    
                </TableCell>
                
              ))}
            </TableRow>
          </TableHead>
          <TableBody>
            
          {rows.map(item=>(
            <TableRow>
         {/* <TableCell component="th" scope="row">
         {item.id}
       </TableCell> */}
       <TableCell className={classes.rowdata}>{item.subject}</TableCell>
       <TableCell className={classes.rowdata}>{item.ticket_category_name}</TableCell>
       <TableCell className={classes.rowdata}>{item.priority}</TableCell>
       <TableCell className={classes.rowdata}>{item.stretch_point}</TableCell>
       <TableCell className={classes.rowdata}>{item.status}</TableCell>
       
     </TableRow>
        ))}
      
             {/* {
          
             // icon:'edit',
              //tooltip:'Edit Ingredient',
            <Button  onClick={()=>handleClick(<ViewTicket changeView={props.changeView}/>)}>Click</Button>
          
             } */}
          </TableBody>
        </Table>
      </TableContainer>
      <TablePagination
        rowsPerPageOptions={[10, 25, 100]}
        component="div"
        count={rows.length}
        rowsPerPage={rowsPerPage}
        page={page}
        onChangePage={handleChangePage}
        onChangeRowsPerPage={handleChangeRowsPerPage}
      />

      </div>
    </Paper>
    
      </main>
    </div>
  );
}


User_Dashboard.propTypes = {
    container: PropTypes.any,
  };