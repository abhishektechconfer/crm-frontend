import React,  { useEffect } from 'react';
import PropTypes from 'prop-types';

import {  makeStyles, useTheme  } from '@material-ui/core/styles';

import {Drawer,
        Hidden,
        List,
        Divider ,
        Typography,
        Toolbar,
        CssBaseline,
        AppBar,
        Grid,
        Button,

        } from '@material-ui/core';

import IconButton from '@material-ui/core/IconButton';
import MenuIcon from '@material-ui/icons/Menu';
import ExitToAppIcon from '@material-ui/icons/ExitToApp';
import AccountCircle from '@material-ui/icons/AccountCircle';
import Badge from '@material-ui/core/Badge';
import NotificationsIcon from '@material-ui/icons/Notifications';
import MenuItem from '@material-ui/core/MenuItem';
import Menu from '@material-ui/core/Menu';


import {Modal,Fade,Backdrop} from '@material-ui/core';
import Notifications from '../Notifications';
import Create from '../Create';
import {postData} from '../../FetchServices';
import DummyListItems from './DummyListItems';
import {isMobile} from 'react-device-detect';
import DeviceOrientation, { Orientation } from 'react-screen-orientation'


///////////////////////////////////////////////////////////

const drawerWidth = 250;

const useStyles = makeStyles(theme => ({
  root: {
    display: 'flex',
  },

  root1: {
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
     minHeight: 'fit-content',
     marginTop:20
  },
  avatar: {
    width: 60,
    height: 60
  },
  avat:{
    marginLeft:15
  },
  title:{
    flexGrow: 1,
    textAlign:'center',
    marginTop:'2%',
    color:'#1e83a9',
  },

  appBar: {
    backgroundColor:'white',
     //backgroundColor:'#1e83a9',
    //  backgroundColor:'#f5f5f5',
   zIndex: theme.zIndex.drawer + 1,
   opacity: '1',

  },
  drawer: {
    width: drawerWidth,
    flexShrink: 0,
  },
  drawerPaper: {
    width: drawerWidth,
  },
  content: {
    flexGrow: 1,
    padding: theme.spacing(3),
  },
  toolbar: theme.mixins.toolbar,

  menuButton: {
    marginRight: theme.spacing(2),

  },
  grow: {
    flexGrow: 1,
  },

  notification: {

      display: 'flex',

  },
  image: {
    width: '84%',
    marginTop: '2%',
   // marginRight: '50%',
   // marginBottom:'6%',
  },
  modal: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
  },
  modalcontent: {
    backgroundColor: theme.palette.background.paper,
    border: '2px solid #000',
    boxShadow: theme.shadows[5],
    padding: theme.spacing(2, 4, 3),
    height:'220px',
    overflowY:'scroll',
  },
  portraitContent:{
    backgroundColor: theme.palette.background.paper,
    border: '2px solid #000',
    boxShadow: theme.shadows[5],
    padding: theme.spacing(2, 4, 3),
  },
  /**Header Style** */
  rootHead: {
    // height: '100vh',
   // marginTop :theme.spacing(1),
    background:'white',
    overflowY:'scroll',
   },
   paper: {
     margin: theme.spacing(8, 4),
     display: 'flex',
     flexDirection: 'column',
     alignItems: 'center',
   },
  
   form: {
     width: '100%', // Fix IE 11 issue.
     marginTop: theme.spacing(1),
   },
   para1:{
     marginRight:'8%',
     textAlign:'right',
     color:'black',
     marginTop:'-2%',
   },
   para2:{
     marginRight:'8%',
     textAlign:'right',
     color:'black',
     marginTop:'3%',
   },
   para3:{
     marginRight:'8%',
     textAlign:'right',
     color:'black',
     marginTop:'-2%',
   },
   
   type1:{
      marginRight:'10%', 
     textAlign:'right',
     color:'black',
     //marginTop:'-3%',
   },
   type2:{
     marginRight:'39%', 
    //textAlign:'right',
    color:'black',
    marginTop:'-3%',
  },
  type3:{
   marginRight:'10%', 
 // textAlign:'right',
  color:'black',
  marginTop:'-3%',
 },
 type4:{
   marginRight:'23%', 
 // textAlign:'right',
  color:'black',
  marginTop:'-3%',
 },
   titleHead:{
     //marginTop:'5%',
    color:'black'
   },
  gmr:{
    width:'40%',
    marginTop:'12%',
  }

}));



/////////////////////////////////////////////////////////////////////////////////

export default function LinkedDash(props) {
    const { container } = props;

    const classes = useStyles();
    const [view,setView] = React.useState('')

   ///  const [token,setToken] = React.useState('')
     const [rows,setRows]=React.useState([])

const BaseURL="http://83.136.219.147/trafiksol/public/api/v1"

const postData1=async(url,body)=>{
    var response=await fetch(`${BaseURL}/${url}`,{
    method:"POST",
    mode:"cors",
    headers:{
     // "Authorization":"Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOlwvXC9hYXJvZ3lhaW5kaWEuaW5cL3RyYWZpa3NvbFwvcHVibGljXC9hcGlcL3YxXC9sb2dpbiIsImlhdCI6MTU5OTEyMzA1MiwiZXhwIjoxNjAxNzE1MDUyLCJuYmYiOjE1OTkxMjMwNTIsImp0aSI6IlJQVmttMHhmcFRzZHpHaGoiLCJzdWIiOjEsInBydiI6Ijg3ZTBhZjFlZjlmZDE1ODEyZmRlYzk3MTUzYTE0ZTBiMDQ3NTQ2YWEifQ.DFp2qevRXpL5DKJNSyrryVAnGD0BWXm-GiMVQeNq5LQ",
     "Authorization": "Bearer " + token,
     "Content-Type": "application/json; charset=utf-8"},
     body:JSON.stringify(body)})
     var result=await response.json()
  return result
}


  let rec=JSON.parse(localStorage.getItem('LOGIN'))
   console.log(rec)
   let token=rec.data.user.token;

     const readAllSites=async()=>{

          let result= await postData1('sites/getSiteInfoByUser')
        console.log(result)
       setRows(result.data)
       
       if(result.data==''){
        setOpen(true);
    }else{
       setOnn(true);
    }
      }

   useEffect(function(){ 
    getLocation() 
    OstypeDetect()
    readAllSites();
    props.history.push({pathname:'/Linked' })
   // setOnn(true);
  },[])

   
  const handleLogout=async()=>{
    //alert('hii')
       
    let body = {
      'lat':latitude,
      'lng':longitude,
      'ostype':ostype,
    }
    console.log(body)

       let result=await postData1('apilogout',body)
       console.log(result)
       if(result)
      {
      localStorage.clear();
      props.history.replace({pathname:'/'})
        }
  else
       { alert("Fail to Logout...")  } 

      } 

const [ostype,setOstype] = React.useState('')
const OstypeDetect = () => {
if (isMobile) {
  setOstype('mobile')
}else{
  setOstype('PC')  
}
}
      
  
const [latitude,setLatitude] = React.useState('')
const [longitude,setLongitude] = React.useState('')

function getLocation() {
  if (navigator.geolocation) {
    navigator.geolocation.getCurrentPosition(showPosition);
  } else { 
    alert("Geolocation is not supported by this browser.")
  }
}

function showPosition(position) {
  return(
   setLatitude( position.coords.latitude) ,
    setLongitude (position.coords.longitude)
  )
}  

   

  //   const {data} = this.props.location
    //const admin =useSelector(state=>state.admin)
   // console.log("Admin Name.........",admin.adminname)

    const theme = useTheme();

    const handleDrawerToggle = () => {
        setMobileOpen(!mobileOpen);
      };


    const [mobileOpen, setMobileOpen] = React.useState(false);
                  const [onn, setOnn] = React.useState(false);
                
                  const handleOff = () => {
                    setOnn(false);
                  }; 
            
                  const [open, setOpen] = React.useState(false);
                
                  const handleClose = () => {
                    setOpen(false);
                  }; 

                  const changeView=(View)=>{
                    setView(View)
                    }    
                  //   const handleSelectSite = (id) => {
                  //     localStorage.setItem('SELECTID',id) 
                  // props.history.push({pathname:'/UserDashboard' })
                  //   };   
                    
                    const handleSelectSite = (item) => {
                      //alert(item)
                      console.log(item)
                      localStorage.setItem('SELECTSITE',JSON.stringify(item) )
                  props.history.push({pathname:'/UserDashboard' })
                    };

                    
                    
/* ---- Current date ----- */
var today = new Date();
var dd = today.getDate();
var mm = today.getMonth()+1; 
var yyyy = today.getFullYear();
if(dd<10) 
{ dd='0'+dd;} 

if(mm<10) 
{   mm='0'+mm;} 
today = mm+'-'+dd+'-'+yyyy;

  ////////////////////////////////////////////////////////////////////////
  const drawer = (
    <div className={classes.root1}>

   <Typography  variant="h6" >Welcome</Typography>
    {/* <Avatar alt="Person" className={classes.avatar}  src="" />
      <Link href="/Dashboard">
            <img style={{width:100, height:100, alignItems:'center'}} src="./images/logo.png" />
            </Link>      */}
      <List>
        <DummyListItems/>
      </List>
    </div>
  );



  return (
   console.log(token),
   console.log(rows),

    <div className={classes.root}>
      <CssBaseline />
      <AppBar className={classes.appBar}>
        <Toolbar>

        <Grid container spacing={3}>

          <Grid item xs={2} sm={2} md={2}>

          <Hidden smDown implementation="css">
          <img src="./images/TrafikSol_Logo.jpg" className={classes.image}/>
          </Hidden>

        <Hidden mdUp implementation="css">
        <IconButton
            color="inherit"
            aria-label="open drawer"
            edge="start"
            onClick={handleDrawerToggle}
            className={classes.menuButton}
          >
            <MenuIcon />
          </IconButton>
          </Hidden>

        </Grid>

         <Grid item xs={6} sm={4} md={8}>
          <Typography variant="h6" noWrap className={classes.title}>
           CRM View
          </Typography>
          </Grid>


           <div className={classes.grow} />
           <Grid item xs={4}sm={2} xs={3} md={2}>
          <div className={classes.notification}>

            <Notifications changeView={changeView} />

           <Create changeView={changeView}/>

           <IconButton
              edge="end"
              aria-label="account of current user"
              aria-haspopup="true"
              color="inherit"
            >
              <ExitToAppIcon style={{color:"#1e83a9"}} onClick={handleLogout}/>
            </IconButton>

          </div>
          </Grid>

           </Grid>

        </Toolbar>
      </AppBar>


     {/* /////////////////////////// For Web////////////////////////////////// */}
     <Hidden smDown implementation="css">

      <Drawer
        className={classes.drawer}
        variant="permanent"
        classes={{
          paper: classes.drawerPaper,
        }}
      >
        <div className={classes.toolbar} />

         {drawer}


      </Drawer>
    </Hidden>

 {/* ///////////////////////////// For Mobile //////////////////////////////// */}

        <Hidden smUp implementation="css">
          <Drawer
            container={container}
            variant="temporary"
            anchor={theme.direction === 'rtl' ? 'right' : 'left'}
            open={mobileOpen}
            onClose={handleDrawerToggle}
            classes={{
              paper: classes.drawerPaper,
            }}
            ModalProps={{
              keepMounted: true, // Better open performance on mobile.
            }}
          >
            {drawer}
          </Drawer>
        </Hidden>

        <Modal
        aria-labelledby="transition-modal-title"
        aria-describedby="transition-modal-description"
        className={classes.modal}
        open={open}
        onClose={handleClose}
        closeAfterTransition
        BackdropComponent={Backdrop}
        BackdropProps={{
          timeout: 500,
        }}
      >
        <Fade in={open}>
          <div className={classes.portraitContent}>
            <h2 id="transition-modal-title">No site is assigned to you.</h2>
            <Button color="primary" onClick={handleLogout}>Ok</Button> 
     
          </div>
        </Fade>
      </Modal>

      

        <Modal
        aria-labelledby="transition-modal-title"
        aria-describedby="transition-modal-description"
        className={classes.modal}
        open={onn}
       // onClose={handleOff}
        closeAfterTransition
        BackdropComponent={Backdrop}
        BackdropProps={{
          timeout: 500,
        }}
      >
        <Fade in={onn}>
          <DeviceOrientation lockOrientation={'landscape'}>
          <Orientation orientation='landscape' alwaysRender={false}>
          <div className={classes.modalcontent}>
            <h2 id="transition-modal-title">Mandatory to Select Site</h2>
            {rows.map(item=>(
                <MenuItem id="transition-modal-description"   onClick={()=>handleSelectSite(item) }>{item.name} ({item.alias_name})</MenuItem>
            // <MenuItem id="transition-modal-description"  onClick={()=>(props.history.push({pathname:'/UserDashboard',state:{site_id:item.id} }) )}>{item.name}</MenuItem>
            ))}
     
          </div>
          </Orientation>
          <Orientation orientation='portrait' alwaysRender={false}>
          <div className={classes.portraitContent}>
            <p style={{color:'red'}}>Please rotate your device to select site !</p>
          </div>
        </Orientation>
        </DeviceOrientation>
        </Fade>
      </Modal>
      


      <main className={classes.content}>
        <div className={classes.toolbar} />
       {view}
      </main>
    </div>
  );
}


LinkedDash.propTypes = {
    container: PropTypes.any,
  };