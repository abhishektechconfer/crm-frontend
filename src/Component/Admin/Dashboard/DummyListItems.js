import React from 'react';
import {  makeStyles, useTheme  } from '@material-ui/core/styles';

import {ListItem,
  Divider,
  Collapse,
  List
} from '@material-ui/core';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import ListSubheader from '@material-ui/core/ListSubheader';
import DashboardIcon from '@material-ui/icons/Dashboard';
import AssignmentIndIcon from '@material-ui/icons/AssignmentInd';
import ListAltIcon from '@material-ui/icons/ListAlt';
import ExpandLess from '@material-ui/icons/ExpandLess';
import ExpandMore from '@material-ui/icons/ExpandMore';

/////////////////////////////////////////////////////

const useStyles = makeStyles(theme => ({
color:{

// fontStyle:"bold" ,

'&:hover': {
color:'#1e83a9',
fontStyle:"bold" 
},
},

IconMargin:{
  marginRight:10,
},



}));



/////////////////////////////

export default function DummyListItems (props){
  const classes = useStyles();
  const [open1, setOpen1] = React.useState(false);


  const handleClick=(view)=>{
    props.changeView(view)
    }

    const handleClick1=(view)=>{
     // props.changeView(view)
      setOpen1(!open1);
    } 


  const DummyListItems = (

  <div >
   <Divider style={{marginTop:5, marginBottom:5}}/>

<ListItem className={classes.color}>
<ListItemIcon><DashboardIcon /></ListItemIcon>
<ListItemText primary="Dashboard" />
</ListItem>

<Divider style={{marginTop:5,marginBottom:5}}/>
<ListItem className={classes.color}>
<ListItemIcon><ListAltIcon /></ListItemIcon>
<ListItemText primary="Create Ticket" />
</ListItem>

<Divider style={{marginTop:5, marginBottom:5}}/>


<ListItem button onClick={()=>handleClick1()}>
<ListItemIcon>
<DashboardIcon />
</ListItemIcon>
<ListItemText primary="Ticket List" className={classes.color} />  
{open1 ? <ExpandLess /> : <ExpandMore />}  
</ListItem>

<Collapse in={open1} timeout="auto" unmountOnExit>
<List component="div" disablePadding>

<Divider style={{marginTop:5,marginBottom:5}}/>
<ListItem className={classes.color}>
<ListItemIcon><ListAltIcon /></ListItemIcon>
<ListItemText primary="Open Ticket" />
</ListItem>

<ListItem className={classes.color}>
<ListItemIcon><ListAltIcon /></ListItemIcon>
<ListItemText primary="Answered Ticket" />
</ListItem>

<ListItem className={classes.color}>
<ListItemIcon><ListAltIcon /></ListItemIcon>
<ListItemText primary="My Ticket" />
</ListItem>

<ListItem className={classes.color}>
<ListItemIcon><ListAltIcon /></ListItemIcon>
<ListItemText primary="Closed Ticket" />
</ListItem>

</List>
</Collapse>



{/* <Divider style={{marginTop:5,marginBottom:5}}/>
<ListItem button  onClick={()=>handleClick(<TicketTabs changeView={props.changeView}/>)} className={classes.color}>
<ListItemIcon><ListAltIcon /></ListItemIcon>
<ListItemText primary="Ticket Tabs" />
</ListItem> */}



<Divider style={{marginTop:5,marginBottom:5}}/>
<ListItem className={classes.color}>
<ListItemIcon><ListAltIcon /></ListItemIcon>
<ListItemText primary="Create Store" />
</ListItem>
<Divider style={{marginTop:5,marginBottom:5}}/>
<ListItem className={classes.color}>
<ListItemIcon><ListAltIcon /></ListItemIcon>
<ListItemText primary="Store List" />
</ListItem>

  

  </div>
  );
  return(<div>
    {DummyListItems}
  </div>);
 };

 