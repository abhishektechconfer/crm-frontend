import React from 'react';
import { makeStyles, useTheme } from '@material-ui/core/styles';

import {
  ListItem, Hidden,
  Divider,
  Collapse,
  List
} from '@material-ui/core';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import AddBoxIcon from '@material-ui/icons/AddBox';
import DashboardIcon from '@material-ui/icons/Dashboard';
import AssignmentIndIcon from '@material-ui/icons/AssignmentInd';
import ListAltIcon from '@material-ui/icons/ListAlt';
import ExpandLess from '@material-ui/icons/ExpandLess';
import ExpandMore from '@material-ui/icons/ExpandMore';


import TicketCreate from '../TicketManagement/TicketCreate';
import OpenTicket from '../TicketManagement/OpenTicket';
import AnsweredTicket from '../TicketManagement/AnsweredTicket';
import MyTicket from '../TicketManagement/MyTicket';
import ClosedTicket from '../TicketManagement/ClosedTicket';
import StoreItemList from '../TicketManagement/StoreItemList';
import ReturnItemList from '../TicketManagement/ReturnItemList';
import CreateItem from '../TicketManagement/CreateItem';
import ReturnItem from '../TicketManagement/ReturnItem';
import Crosscheck from '../TicketManagement/Crosscheck';

import Dashboard from '../dash/Dashboard';


/////////////////////////////////////////////////////

const useStyles = makeStyles(theme => ({
  color: {

    // fontStyle:"bold" ,

    '&:hover': {
      color: '#1e83a9',
      fontStyle: "bold"
    },
  },

  IconMargin: {
    marginRight: 10,
  },



}));



/////////////////////////////

export default function User_MainList(props) {
  const classes = useStyles();
  const [open1, setOpen1] = React.useState(false);
  const [open2, setOpen2] = React.useState(false);


  const handleClick = (view) => {
    props.changeView(view)
  }

  const handleClick1 = (view) => {
    // props.changeView(view)
    setOpen1(!open1);
  }

  const handleClick2 = (view) => {
    // props.changeView(view)
    setOpen2(!open2);
  }



  const UserMainListItems = (

    <div >

      <Divider style={{ marginTop: 5, marginBottom: 5 }} />
      <Hidden mdDown implementation="css">
        <ListItem button onClick={() => handleClick(<Dashboard changeView={props.changeView} />)} className={classes.color}>
          <ListItemIcon style={{ color: '#1e83a9' }}><DashboardIcon /></ListItemIcon>
          <ListItemText primary="Dashboard" />
        </ListItem>
      </Hidden>
      <Divider style={{ marginTop: 5, marginBottom: 5 }} />
      <ListItem button onClick={() => handleClick(<TicketCreate changeView={props.changeView} />)} className={classes.color}>
        <ListItemIcon style={{ color: '#1e83a9' }}><AddBoxIcon /></ListItemIcon>
        <ListItemText primary="Create Ticket" />
      </ListItem>

      <Divider style={{ marginTop: 5, marginBottom: 5 }} />


      <ListItem button onClick={() => handleClick1()}>
        <ListItemIcon>
          <img src="./images/Ticket_list.png" style={{ width: '26px' }} />
        </ListItemIcon>
        <ListItemText primary="Ticket List" className={classes.color} />
        {open1 ? <ExpandLess /> : <ExpandMore />}
      </ListItem>

      <Collapse in={open1} timeout="auto" unmountOnExit>
        <List component="div" disablePadding>

          <Divider style={{ marginTop: 5, marginBottom: 5 }} />
          <ListItem button onClick={() => handleClick(<OpenTicket changeView={props.changeView} />)} className={classes.color}>
            <ListItemIcon style={{ color: '#1e83a9' }}><ListAltIcon /></ListItemIcon>
            <ListItemText primary="Open Ticket" />
          </ListItem>

          <ListItem button onClick={() => handleClick(<AnsweredTicket changeView={props.changeView} />)} className={classes.color}>
            <ListItemIcon style={{ color: '#1e83a9' }}><ListAltIcon /></ListItemIcon>
            <ListItemText primary="Assigned Ticket" />
          </ListItem>

          <ListItem button onClick={() => handleClick(<MyTicket changeView={props.changeView} />)} className={classes.color}>
            <ListItemIcon style={{ color: '#1e83a9' }}><ListAltIcon /></ListItemIcon>
            <ListItemText primary="My Ticket" />
          </ListItem>

          <ListItem button onClick={() => handleClick(<ClosedTicket changeView={props.changeView} />)} className={classes.color}>
            <ListItemIcon style={{ color: '#1e83a9' }}><ListAltIcon /></ListItemIcon>
            <ListItemText primary="Closed Ticket" />
          </ListItem>

        </List>
      </Collapse>



      {/* <Divider style={{marginTop:5,marginBottom:5}}/>
    <ListItem button  onClick={()=>handleClick(<TicketTabs changeView={props.changeView}/>)} className={classes.color}>
      <ListItemIcon><ListAltIcon /></ListItemIcon>
      <ListItemText primary="Ticket Tabs" />
    </ListItem> */}




      {/*<Divider style={{marginTop:5,marginBottom:5}}/>
    <ListItem button  onClick={()=>handleClick(<StoreItemList changeView={props.changeView}/>)} className={classes.color}>
      <ListItemIcon><img src="./images/storeIcon.png" style={{width:'26px'}}/></ListItemIcon>
      <ListItemText primary="Site Inventory" />
  </ListItem> */}

      {/* <Divider style={{marginTop:5,marginBottom:5}}/>
    <ListItem button  onClick={()=>handleClick(<Crosscheck changeView={props.changeView}/>)} className={classes.color}>
      <ListItemIcon><img src="./images/storeIcon.png" style={{width:'26px'}}/></ListItemIcon>
      <ListItemText primary="CrossCheck" />
  </ListItem> */}



      <ListItem button onClick={() => handleClick2()}>
        <ListItemIcon>
          <img src="./images/storeIcon.png" style={{ width: '26px' }} />
        </ListItemIcon>
        <ListItemText primary="Site Inventory" className={classes.color} />
        {open2 ? <ExpandLess /> : <ExpandMore />}
      </ListItem>

      <Collapse in={open2} timeout="auto" unmountOnExit>
        <List component="div" disablePadding>

          <Divider style={{ marginTop: 5, marginBottom: 5 }} />
          <ListItem button onClick={() => handleClick(<StoreItemList changeView={props.changeView} />)} className={classes.color}>
            <ListItemIcon style={{ color: '#1e83a9' }}><ListAltIcon /></ListItemIcon>
            <ListItemText primary="Item List" />
          </ListItem>

          <ListItem button onClick={() => handleClick(<ReturnItemList changeView={props.changeView} />)} className={classes.color}>
            <ListItemIcon style={{ color: '#1e83a9' }}><ListAltIcon /></ListItemIcon>
            <ListItemText primary="Return Item List" />
          </ListItem>

        </List>
      </Collapse>



    </div>
  );
  return (<div>
    {UserMainListItems}
  </div>);
};

