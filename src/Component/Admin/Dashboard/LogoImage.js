import React,{useEffect} from 'react';
import {  makeStyles, useTheme ,withStyles } from '@material-ui/core/styles';

import {ListItem,
  Divider,
  Collapse,
  Button
} from '@material-ui/core';

import MenuItem from '@material-ui/core/MenuItem';
import {Menu,Avatar} from '@material-ui/core';
import AccountCircle from '@material-ui/icons/AccountCircle';
import IconButton from '@material-ui/core/IconButton';
import Modal from '@material-ui/core/Modal';
import Backdrop from '@material-ui/core/Backdrop';
import Fade from '@material-ui/core/Fade';

import Tooltip from '@material-ui/core/Tooltip';
import ClickAwayListener from '@material-ui/core/ClickAwayListener';

import Dashboard from '../dash/Dashboard';

/////////////////////////////////////////////////////

const useStyles = makeStyles(theme => ({

    image: {
        width: '84%',
        marginTop: '2%',
       // marginRight: '50%',
       // marginBottom:'6%',
      },
      
color:{

// fontStyle:"bold" ,

'&:hover': {
color:'#ff3278',
fontStyle:"bold" 
},
},

IconMargin:{
  marginRight:10,
},
avatar: {
  width: 30,
  height: 30
},


}));





/////////////////////////////

export default function LogoImage (props){
  const classes = useStyles();
  

  const [anchorEl, setAnchorEl] = React.useState(null);
  const open = Boolean(anchorEl);
  const [mobileOpen, setMobileOpen] = React.useState(false);


  const handleOpen = (event) => {
    setAnchorEl(event.currentTarget);
  };

  const handleClose = () => {
    setAnchorEl(null);
  };


  const [onn, setOnn] = React.useState(false);

  const handleOnn = () => {
    setOnn(true);
  };

  const handleOff = () => {
    setOnn(false);
  };

  const handleClick=(view)=>{
    props.changeView(view)
    }

    const [up, setUp] = React.useState(false);

    const handleTooltipClose = () => {
      setUp(false);
    };
  
    const handleTooltipOpen = () => {
      setUp(true);
    };

  const Create = (


       <div>
        

        <img src="./images/TrafikSol_Logo.jpg" onClick={()=>handleClick(<Dashboard changeView={props.changeView}/>)} className={classes.image}/>
                
            </div>
   
  
  );
  return(<div>
    {Create}
  </div>);
 };

 