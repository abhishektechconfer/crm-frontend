import React,  { useEffect } from 'react';
import PropTypes from 'prop-types';

import {  makeStyles, useTheme  } from '@material-ui/core/styles';

import {Drawer,Accordion,AccordionSummary,AccordionDetails,Hidden,List,
        
        Paper ,
        Typography,
        Toolbar,
        CssBaseline,
        AppBar,
        Grid,
        Container
        ,
        Button,

        } from '@material-ui/core';

import IconButton from '@material-ui/core/IconButton';
import MenuIcon from '@material-ui/icons/Menu';
import ExitToAppIcon from '@material-ui/icons/ExitToApp';
import AccountCircle from '@material-ui/icons/AccountCircle';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import NotificationsIcon from '@material-ui/icons/Notifications';
import MenuItem from '@material-ui/core/MenuItem';
import Menu from '@material-ui/core/Menu';
import TrackChangesIcon from '@material-ui/icons/TrackChanges';

import Tooltip from '@material-ui/core/Tooltip';
import ClickAwayListener from '@material-ui/core/ClickAwayListener';
import User_MainList from './User_MainList';
import LogoImage from './LogoImage';
import Notifications from '../Notifications';
import Create from '../Create';
import Dashboard from '../dash/Dashboard';
import {isMobile} from 'react-device-detect';
import NoticeMobile from '../NoticeMobile';



///////////////////////////////////////////////////////////

const drawerWidth = 250;

const useStyles = makeStyles(theme => ({
  root: {
    display: 'flex',
  },

  root1: {
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
     minHeight: 'fit-content',
     marginTop:20
  },
  avatar: {
    width: 60,
    height: 60
  },
  avat:{
    marginLeft:15
  },
  title:{
    flexGrow: 1,
    textAlign:'center',
    marginTop:'1%',
    color:'#1e83a9',
  },

  appBar: {
    backgroundColor:'white',
     //backgroundColor:'#1e83a9',
    //  backgroundColor:'#f5f5f5',
   zIndex: theme.zIndex.drawer + 1,
   opacity: '1',

  },
  drawer: {
    width: drawerWidth,
    flexShrink: 0,
  },
  drawerPaper: {
    width: drawerWidth,
  },
  content: {
    flexGrow: 1,
    padding: theme.spacing(3),
  },
  toolbar: theme.mixins.toolbar,

  menuButton: {
    marginRight: theme.spacing(2),
    color:'#1e83a9',
  },
  grow: {
    flexGrow: 1,
  },

  notification: {
  display: 'flex',
  },
  image: {
    width: '84%',
    marginTop: '2%',
   // marginRight: '50%',
   // marginBottom:'6%',
  },
  
  /**Header Style** */
  rootHead: {
    // height: '100vh',
   marginBottom :'3%',
    background:'white',
    padding:'10px'

   },
   paper: {
     margin: theme.spacing(8, 4),
     display: 'flex',
     flexDirection: 'column',
     alignItems: 'center',
   },

   form: {
     width: '100%', // Fix IE 11 issue.
     marginTop: theme.spacing(1),
   },
   para1:{
     marginRight:'8%',
     textAlign:'right',
     color:'black',
     marginTop:'-2%',
   },
   para2:{
     marginRight:'8%',
     textAlign:'right',
     color:'black',
     marginTop:'3%',
   },
   para3:{
     marginRight:'8%',
     textAlign:'right',
     color:'black',
     marginTop:'-2%',
   },

   type1:{
      //marginRight:'10%',
     textAlign:'left',
     color:'black',
     marginTop:'2%',
   },
   type2:{
     //marginRight:'39%',
    textAlign:'left',
    color:'black',
    marginTop:'-1%',
  },
  type3:{
   marginRight:'10%',
 // textAlign:'right',
  color:'black',
  marginTop:'-3%',
 },
 type4:{
   //marginRight:'23%',
   textAlign:'left',
  color:'black',
  marginTop:'-1%',
 },
   titleHead:{
     //marginTop:'5%',
    color:'black'
   },
  gmr:{
    width:'60%',
    marginTop:'6%',
    //marginRight:'7%'
  },
  imageRes:{
    width: '150px',
  },
  text1:{
    fontSize:'small',
    marginTop:'3%',
    marginBottom:'3%',
  },
  clientLogo:{
    width:'35%',
    marginTop:'6%',
  },
  

}));



/////////////////////////////////////////////////////////////////////////////////

export default function User_Dashboard(props) {
    const { container } = props;

    const classes = useStyles();
    const [view,setView] = React.useState('')
    const [dash,setDash] = React.useState(true)
    const [siteId,setSiteId] = React.useState('')

     const [username,setUserName] = React.useState('')
      const [address,setAddress] = React.useState('')
     const [sitename,setSiteName] = React.useState('')
     const [aliasname,setAliasName] = React.useState('')
     const [image,setImage] = React.useState('')
     

     const handleLogout=async()=>{
      //alert('hii')
         
      let body = {
        'lat':latitude,
        'lng':longitude,
        'ostype':ostype,
      }
      console.log(body)

         let result=await postData1('apilogout',body)
         console.log(result)
         if(result)
        {
        localStorage.clear();
        props.history.replace({pathname:'/'})
          }
    else
         { alert("Fail to Logout...")  } 

        } 

 const [ostype,setOstype] = React.useState('')
const OstypeDetect = () => {
if (isMobile) {
    setOstype('mobile')
  }else{
    setOstype('PC')  
  }
}
        
    
  const [latitude,setLatitude] = React.useState('')
  const [longitude,setLongitude] = React.useState('')

  function getLocation() {
    if (navigator.geolocation) {
      navigator.geolocation.getCurrentPosition(showPosition);
    } else { 
      alert("Geolocation is not supported by this browser.")
    }
  }
  
  function showPosition(position) {
    return(
     setLatitude( position.coords.latitude) ,
      setLongitude (position.coords.longitude)
    )
  }  

     const ChangeSite=async(id)=>{

      const body={
        'site_id':id,
                  }
            console.log(body)
            
         let result= await postData1('sites/getSiteInfoBySiteId',body)
       console.log(result)
       setAddress(result.data.address1)
       setSiteName(result.data.name)
       setAliasName(result.data.alias_name)
       setImage(result.data.client_logo)

         

       if(result.status==1){
         setOnn(false)
        
       }
     }
  

    const theme = useTheme();

    const handleDrawerToggle = () => {
        setMobileOpen(!mobileOpen);
      };

    const changeView=(View)=>{
      if(View!="")
      { 
        setDash(false)
        setView(View)
      }
     else
     {
      //setView(View)
     }
      }

    const [mobileOpen, setMobileOpen] = React.useState(false);
                  const [onn, setOnn] = React.useState(false);
                
                  const handleOff = () => {
                    setOnn(false);
                  }; 
                  const [open, setOpen] = React.useState(false);

                  const [up, setUp] = React.useState(false);

                  const handleTooltipClose = () => {
                    setUp(false);
                  };
                
                  const handleTooltipOpen = () => {
                    setUp(true);
                  };
                             
                //  let SiTeId=localStorage.getItem('SELECTID')
                  let SiteItem=JSON.parse(localStorage.getItem('SELECTSITE'))
                  console.log(SiteItem)
                  let SiTeId = SiteItem.id;
                  let SiteAddress = SiteItem.site_address;

    useEffect(function(){
    try{
      props.history.push({pathname:'/UserDashboard' ,SiTeId})
      readProfileData()
        getLocation() 
        OstypeDetect()
        //setSla(props.sla_end)
        setSiteId(SiTeId)
        ChangeSite(SiTeId)
        AdminInfoBySite(SiTeId)
    }
    catch(e){}

},[])


const [AdminList,setAdminList]= React.useState('')
const [reviewer,setReviewer]= React.useState('')
const AdminInfoBySite=async(id)=>{

 const body={
   'site_id':id,
             }
       console.log(body)
    let result= await postData1('sites/getAdminUserBySite',body)
  console.log(result)
  if(result.status==1){
   setAdminList(result.data)
   let AdminName = result.data.map((item)=>(
     item.name
    ))
    setReviewer(AdminName)
  }
}

localStorage.setItem('SITEID', JSON.stringify(siteId)) 


const readProfileData=async()=>{
  const body={
    'name' : '',
    'file[]':'',
    'phone':'',
    
              }
 console.log(body)           
 let result=await postData1('editMyProfile',body)
 console.log(result)  
 setUserName(result.data.name)
}

 
const BaseURL="http://83.136.219.147/trafiksol/public/api/v1"

const postData1=async(url,body)=>{
    var response=await fetch(`${BaseURL}/${url}`,{
    method:"POST",
    mode:"cors",
    headers:{
     // "Authorization":"Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOlwvXC9hYXJvZ3lhaW5kaWEuaW5cL3RyYWZpa3NvbFwvcHVibGljXC9hcGlcL3YxXC9sb2dpbiIsImlhdCI6MTU5OTEyMzA1MiwiZXhwIjoxNjAxNzE1MDUyLCJuYmYiOjE1OTkxMjMwNTIsImp0aSI6IlJQVmttMHhmcFRzZHpHaGoiLCJzdWIiOjEsInBydiI6Ijg3ZTBhZjFlZjlmZDE1ODEyZmRlYzk3MTUzYTE0ZTBiMDQ3NTQ2YWEifQ.DFp2qevRXpL5DKJNSyrryVAnGD0BWXm-GiMVQeNq5LQ",
     "Authorization": "Bearer " + token,
     "Content-Type": "application/json; charset=utf-8"},
     body:JSON.stringify(body)})
     var result=await response.json()
  return result
}

  let rec=JSON.parse(localStorage.getItem('LOGIN'))
   console.log(rec)
   let token=rec.data.user.token;

   /* ---- Current date ----- */
   var today = new Date();
var dd = today.getDate();
var mm = today.getMonth()+1; 
var yyyy = today.getFullYear();
if(dd<10) 
{ dd='0'+dd;} 

if(mm<10) 
{   mm='0'+mm;} 
today = dd+'-'+mm+'-'+yyyy;

  ////////////////////////////////////////////////////////////////////////
  const drawer = (
    <div className={classes.root1}>

   <Typography  variant="h6" >Welcome</Typography>
   <Typography  variant="h6" >{username}</Typography>
    {/* <Avatar alt="Person" className={classes.avatar}  src="" />
      <Link href="/Dashboard">
            <img style={{width:100, height:100, alignItems:'center'}} src="./images/logo.png" />
            </Link>      */}
      <List>
        <User_MainList changeView={changeView}/>
      </List>
    </div>
  );



  return (
    console.log(sitename),

    <div className={classes.root}>
      <CssBaseline />
      <AppBar className={classes.appBar}>
        <Toolbar>

        <Grid container spacing={3}>

          <Grid item xs={2} sm={2} md={2}>

          <Hidden smDown implementation="css">
            <LogoImage changeView={changeView} />
          {/* <img src="./images/TrafikSol_Logo.jpg" className={classes.image}/> */}
         </Hidden>

        <Hidden mdUp implementation="css">
        <IconButton
            color="inherit"
            aria-label="open drawer"
            edge="start"
            onClick={handleDrawerToggle}
            className={classes.menuButton}
          >
            <MenuIcon />
          </IconButton>
          </Hidden>

        </Grid>

         <Grid item xs={3} sm={4} md={8}>
         <Hidden smDown implementation="css">
          <Typography variant="h6" noWrap className={classes.title}>
           CRM View
          </Typography>
          </Hidden>
          <Hidden mdUp implementation="css">
          <img src="./images/Tlogo.png" className={classes.imageRes}/>
         </Hidden>
          </Grid>


           <div className={classes.grow} />
           <Grid item xs={7} sm={4} md={2}>
          <div className={classes.notification}>
          <Tooltip disableFocusListener title="Change your Site">
             <IconButton
              edge="end"
              aria-label="account of current user"
              aria-haspopup="true"
              color="inherit"
            >
          <TrackChangesIcon style={{color:"#1e83a9"}} onClick={()=>(props.history.push({pathname:'/Linked' }) )} />
          </IconButton> 
          </Tooltip>
           
          <Hidden mdDown implementation="css">
            <Notifications changeView={changeView} />
            </Hidden>
            <Hidden mdUp implementation="css">
            <NoticeMobile changeView={changeView} />
            </Hidden>
           
           <Create changeView={changeView}/>
            
            <Tooltip disableFocusListener title="Logout">
           <IconButton
              edge="end"
              aria-label="account of current user"
              aria-haspopup="true"
              color="inherit"
            >
              {/* <ExitToAppIcon onClick={()=>changeView('Logout')} /> */}
              <ExitToAppIcon style={{color:"#1e83a9"}} onClick={handleLogout} />
            </IconButton>
          </Tooltip>

          <Grid container spacing={2}>

<Grid item>
    <ClickAwayListener onClickAway={handleTooltipClose}>
      <div>
        <Tooltip
          PopperProps={{
            disablePortal: true,
          }}
          onClose={handleTooltipClose}
          open={open}
          disableFocusListener
          title="Add"
        >
          <Button onClick={handleTooltipOpen}></Button>
        </Tooltip>
      </div>
    </ClickAwayListener>
  </Grid>

  </Grid>

          </div>
          </Grid>

           </Grid>

        </Toolbar>
      </AppBar>


     {/* /////////////////////////// For Web////////////////////////////////// */}
     <Hidden smDown implementation="css">

      <Drawer
        className={classes.drawer}
        variant="permanent"
        classes={{
          paper: classes.drawerPaper,
        }}
      >
        <div className={classes.toolbar} />

         {drawer}


      </Drawer>
    </Hidden>

 {/* ///////////////////////////// For Mobile //////////////////////////////// */}

        <Hidden smUp implementation="css">
          <Drawer
            container={container}
            variant="temporary"
            anchor={theme.direction === 'rtl' ? 'right' : 'left'}
            open={mobileOpen}
            onClose={handleDrawerToggle}
            classes={{
              paper: classes.drawerPaper,
            }}
            ModalProps={{
              keepMounted: true, // Better open performance on mobile.
            }}
          >
            {drawer}
          </Drawer>
        </Hidden>


      <main className={classes.content}>
        <div className={classes.toolbar} />
        {/* -----Header Content------- */}
        <Hidden mdDown implementation="css">
        <Grid container component="main" className={classes.rootHead}>
      <CssBaseline />

     
      <Grid item xs={4} sm={4} md={9}>
      <h5 className={classes.type1}>
            Form No. &nbsp;&nbsp;: &nbsp;<strong style={{color:'#1e83a9'}}>TSITSPL-CRM-{aliasname}-1001</strong> 
          </h5>
          <h5 className={classes.type4}>
           Address &nbsp;&nbsp;&nbsp; : &nbsp; <strong style={{color:'#1e83a9'}}>{SiteAddress}</strong> 
          </h5>
          <h5 className={classes.type2}>
          Site &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;: &nbsp;<strong style={{color:'#1e83a9'}}>{sitename}</strong>
          </h5>
          <h5 className={classes.type4}>

          </h5>
      </Grid>

      <Grid item xs={4} sm={4} md={3} >
         <img  src={image} className={classes.gmr}/>

      </Grid>

    </Grid>
    </Hidden>

    <Hidden mdUp implementation="css">
   
     <Grid container component="main" className={classes.root}>
      <CssBaseline />

      <Grid item xs={12} sm={12} >
         <img  src={image} className={classes.clientLogo}/>

      </Grid>

      <Grid item xs={12} sm={12}>
      <Accordion>
        <AccordionSummary
          expandIcon={<ExpandMoreIcon />}
          aria-controls="panel1a-content"
          id="panel1a-header"
        >
          <Typography className={classes.heading}>Site Info</Typography>
        </AccordionSummary>
        
        <Grid item xs={12}>
        <Typography className={classes.text1}>
            FORM NO: TSITSPL-DPR-{aliasname}-1001
          </Typography></Grid>
          <Grid item xs={12} >
            <Typography className={classes.text1}>
           ADDRESS: {SiteAddress}
          </Typography ></Grid>
          <Grid item xs={12} ><Typography className={classes.text1}>
          SITE: {sitename}
          </Typography></Grid>
        
      </Accordion>
      </Grid>

    </Grid>
    </Hidden> 
    {/* <Hidden mdDown implementation="css"> */}
    {dash && <Dashboard changeView={changeView}/>}
    {/* </Hidden> */}
       
       {view}
      </main>
    </div>
  );
}


User_Dashboard.propTypes = {
    container: PropTypes.any,
  };