import React, {useEffect, useState} from 'react';
import {fade, makeStyles } from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import {TablePagination,Box,Link,TableRow,Button,Grid,Typography,Divider,
Select,MenuItem,InputBase} from '@material-ui/core';
import SearchIcon from '@material-ui/icons/Search';

import {postData} from '../../FetchServices';

const ColoredLine = ({ color }) => (
  <hr
      style={{
          color: color,
          backgroundColor: color,
          height: 5
      }}
  />
);



const useStyles = makeStyles((theme) => ({
  root: {
    width: '100%',
  },
  search: {
    padding: theme.spacing(0, 2),
    position: 'relative',
    borderRadius: theme.shape.borderRadius,
    backgroundColor: fade(theme.palette.common.white, 0.15),
    '&:hover': {
      backgroundColor: fade(theme.palette.common.white, 0.25),
    },
    //marginLeft: 0,
    float:'right',
    width: '100%',
    [theme.breakpoints.up('sm')]: {
      marginLeft: theme.spacing(1),
      marginTop: theme.spacing(2),
      marginBottom: theme.spacing(2),
      width: 'auto',
    },
  },
  searchIcon: {
    padding: theme.spacing(0, 2),
    height: '100%',
    position: 'absolute',
    pointerEvents: 'none',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
  },
  inputRoot: {
    color: 'inherit',
    backgroundColor:'#aaaaaa2e',
    
  },
  inputInput: {
    padding: theme.spacing(1, 1, 1, 0),
    // vertical padding + font size from searchIcon
    paddingLeft: `calc(1em + ${theme.spacing(4)}px)`,
    transition: theme.transitions.create('width'),
    width: '100%',
    [theme.breakpoints.up('sm')]: {
      width: '12ch',
      '&:focus': {
        width: '20ch',
      },
    },
  },
  container: {
    maxHeight: 440,
    overflowX: 'auto',
  },
  submit:{
 // marginLeft: theme.spacing(115),
  backgroundColor:'#ff6a00',
  color:'#fff',
  width:'10%',
  float:'right',
    
  '&:hover': {
backgroundColor:'#ff6a00',
fontStyle:"bold" 
},
  },
  rowdata:{
    color:'#1e83a9',
    minWidth: 100,
  },
  staff:{
    backgroundColor:'#ff6a00',
    textAlign:'center'
  },
  title: {
    flexGrow: 1,
    textAlign:'center',
   /// marginRight:'5%',
   paddingTop:'3px',
    //backgroundColor:'#1e83a9',
    color:'#1e83a9',
  },
  
 
  select:{
    marginBottom: theme.spacing(3),
    minWidth: 120,
    backgroundColor:'#aaaaaa2e' 
  },
  new1:{
   //margin:theme.spacing(2),
    marginTop:theme.spacing(1) 
  },
  text:{
    //paddingTop:'26px',
    paddingBottom:'17px'
  }
}));

export default function AssignByTicket(props) {

  const classes = useStyles();


  const columns = [
  
    {
      id: 'ticketname',  label: 'Subject',  minWidth: 70,
      format: (value) => value.toLocaleString('en-US'),
    },
    {
      id: 'category',label: 'Description', minWidth: 70, format: (value) => value.toLocaleString('en-US'),
    },
    {
      id: 'priority', label: 'Priority',
      minWidth: 70,
      format: (value) => value.toLocaleString('en-US'),
    },
    {
      id: 'stretch',label: 'Stretch Point', minWidth: 70},
    {
      id: 'status', label: 'Status', minWidth: 70,  format: (value) => value.toLocaleString('en-US'),
    },
    
  ];
  
  const BaseURL="http://83.136.219.147/trafiksol/public/api/v1"

const postData1=async(url,body)=>{
    var response=await fetch(`${BaseURL}/${url}`,{
    method:"POST",
    mode:"cors",
    headers:{
     // "Authorization":"Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOlwvXC9hYXJvZ3lhaW5kaWEuaW5cL3RyYWZpa3NvbFwvcHVibGljXC9hcGlcL3YxXC9sb2dpbiIsImlhdCI6MTU5OTEyMzA1MiwiZXhwIjoxNjAxNzE1MDUyLCJuYmYiOjE1OTkxMjMwNTIsImp0aSI6IlJQVmttMHhmcFRzZHpHaGoiLCJzdWIiOjEsInBydiI6Ijg3ZTBhZjFlZjlmZDE1ODEyZmRlYzk3MTUzYTE0ZTBiMDQ3NTQ2YWEifQ.DFp2qevRXpL5DKJNSyrryVAnGD0BWXm-GiMVQeNq5LQ",
     "Authorization": "Bearer " + token,
     "Content-Type": "application/json; charset=utf-8"},
     body:JSON.stringify(body)})
     var result=await response.json()
  return result
}

  let rec=JSON.parse(localStorage.getItem('LOGIN'))
   console.log(rec)
   let token=rec.data.user.token;


   let SiteItem=JSON.parse(localStorage.getItem('SELECTSITE'))
   console.log(SiteItem)
   let SiTeId = SiteItem.id;
   console.log(SiTeId)
  
  const [rows,setRows]=React.useState([])
  const [error,setError]=React.useState(false)
  const [table,setTable] = React.useState(false)

  const fetchAssignBy=async()=>{
    var body={
      "site_id":SiTeId,
      "list":1
     }
     console.log(body)

     let result= await postData1('dashboard',body)
     console.log(result)
    if(result.data.assignByMeTicket!=''){
    setRows(result.data.assignByMeTicket)
    setTable(true)
       }else{
         setError(true)
       }
   }

   useEffect(()=>{
    fetchAssignBy()
},[])
  

  const [page, setPage] = React.useState(0);
  const [rowsPerPage, setRowsPerPage] = React.useState(5);

  const handleClick=(view)=>{
    props.changeView(view)
    }


  const handleChangePage = (event, newPage) => {
    setPage(newPage);
  };

  const handleChangeRowsPerPage = (event) => {
    setRowsPerPage(+event.target.value);
    setPage(0);
  };


    const View=()=>{

  return (
    console.log(rows),
    <div>

    
    <Paper className={classes.root}>
    <Typography component="h1" variant="h6" color="inherit" noWrap className={classes.title}>
            Created By Me Ticket
          </Typography>
          <ColoredLine color="#1e83a9" />
          </Paper>
          {
              error && <div style={{color:'red',marginTop:'3%'}}>No Ticket is Assigned by Me</div>
            }
            {table && <div>
    <div className={classes.new1}>
 <Paper className={classes.root}>
  
<Divider/>
{/* <div className={classes.search}>
            <div className={classes.searchIcon}>
              <SearchIcon />
            </div>
            <InputBase
              placeholder="Search…"
              classes={{
                root: classes.inputRoot,
                input: classes.inputInput,
              }}
              inputProps={{ 'aria-label': 'search' }}
            />
          </div> */}

      <TableContainer className={classes.container}>
        <Table stickyHeader aria-label="sticky table">
          <TableHead>
            <TableRow>
              {columns.map((column) => (
                <TableCell
                  key={column.id}
                  align={column.align}
                  style={{ minWidth: column.minWidth }}
                >
                  {column.label}
                    
                </TableCell>
                
              ))}
            </TableRow>
          </TableHead>
          <TableBody>
          
          {rows.slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage).map(item=>(
            <TableRow>
         {/* <TableCell component="th" scope="row">
         {item.id}
       </TableCell> */}
       <TableCell className={classes.rowdata}>{item.subject}</TableCell>
       <TableCell className={classes.rowdata}>{item.description}</TableCell>
       <TableCell className={classes.rowdata}>{item.priority}</TableCell>
       <TableCell className={classes.rowdata}>{item.stretch_point}</TableCell>
       <TableCell className={classes.rowdata}>{item.status}</TableCell>
      
     </TableRow>
        ))}
      
             {/* {
          
             // icon:'edit',
              //tooltip:'Edit Ingredient',
            <Button  onClick={()=>handleClick(<ViewTicket changeView={props.changeView}/>)}>Click</Button>
          
             } */}
          </TableBody>
        </Table>
      </TableContainer>
      <TablePagination
        rowsPerPageOptions={[5, 15, 25]}
        component="div"
        count={rows.length}
        rowsPerPage={rowsPerPage}
        page={page}
        onChangePage={handleChangePage}
        onChangeRowsPerPage={handleChangeRowsPerPage}
      />
      </Paper>
</div>
      </div>
    }
    </div>
  )}

  return(
    <div>{View()}</div>
  );
}
