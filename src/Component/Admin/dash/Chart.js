import React from "react";
import { Bar } from "react-chartjs-2";
import { Container,Paper } from "@material-ui/core";

class Chart extends React.Component {
  state = {
    dataBar: {
        
      labels: ["Energy", "Electronics", "Oil", "Power", "Unemployment", "Health","governance","city", "Health","governance"
      ,"city", "Health","governance","city", "Health","governance","city"],
      datasets: [
        {
          label: "Total Tickets",
          data: [12, 19, 3, 5, 12, 3,16,25, 13,6,5, 8,12,19, 7,18,21],
          backgroundColor: [
            "#16a980d4",
            "#16a980d4",
            "#16a980d4",
            "#16a980d4",
            "#16a980d4",
            "#16a980d4", "#16a980d4","#16a980d4", "#16a980d4","#16a980d4", "#16a980d4","#16a980d4","#16a980d4", "#16a980d4","#16a980d4",
            "#16a980d4",
            "#16a980d4"
          ],
          borderWidth: 2,
          borderColor: [
            "#16a980d4",
            "#16a980d4",
            "#16a980d4",
            "#16a980d4",
            "#16a980d4",
            "#16a980d4",
            "#16a980d4","#16a980d4", "#16a980d4","#16a980d4", "#16a980d4","#16a980d4",
            "#16a980d4",
            "#16a980d4"
          ]
        }
      ]
    },
    barChartOptions: {
      responsive: true,
      maintainAspectRatio: false,
      scales: {
        xAxes: [
          {
            barPercentage: 1,
            gridLines: {
              display: true,
              color: "rgba(0, 0, 0, 0.1)"
            }
          }
        ],
        yAxes: [
          {
            gridLines: {
              display: true,
              color: "rgba(0, 0, 0, 0.1)"
            },
            ticks: {
              beginAtZero: true
            }
          }
        ]
      }
    }
  }


  render() {
    return (
      <Container>
          
          <Paper>
        <h3 className="mt-5">Total Tickets V/S Closed Tickets</h3>
        <Bar data={this.state.dataBar} options={this.state.barChartOptions} />
        </Paper>
      </Container>
    );
  }
}

export default Chart;