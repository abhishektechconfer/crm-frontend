import React,{useEffect} from 'react';
import clsx from 'clsx';
import { makeStyles } from '@material-ui/core/styles';
import CssBaseline from '@material-ui/core/CssBaseline';

import {Typography,Avatar,ListItemAvatar} from '@material-ui/core';

import ListItemSecondaryAction from '@material-ui/core/ListItemSecondaryAction';
import EventAvailableIcon from '@material-ui/icons/EventAvailable';
import {Grid,Paper,List,Container,ListItem,ListItemText,Divider} from '@material-ui/core';

import MenuIcon from '@material-ui/icons/Menu';
import ChevronLeftIcon from '@material-ui/icons/ChevronLeft';
import NotificationsIcon from '@material-ui/icons/Notifications';

import Fab from '@material-ui/core/Fab';
import TodayTicket from './TodayTicket';
import MonthTicket from './MonthTicket';
import AssignByMe from './AssignByMe';
import AssignToMe from './AssignToMe';
import Password from '../Password';
import AttendanceDetail from '../AttendLogDetail';
//import Orders from './Orders';

const ColoredLine = ({ color }) => (
  <hr
      style={{
          color: color,
          backgroundColor: color,
          height: 5
      }}
  />
);

const useStyles = makeStyles((theme) => ({
  root: {
    display: 'flex',
  },
 
  title: {
    flexGrow: 1,
    textAlign:'center',
    paddingTop:'3px',
    color:'#1e83a9',
  },
  
  container: {
    //paddingTop: theme.spacing(1),
    //paddingBottom: theme.spacing(1),
  },
  paper: {
   //overflowY:'scroll',
  },
  
  pending: {
   // padding: theme.spacing(2),
    display: 'flex',
    height: 130,
    width:162,
    flexDirection: 'column',
  // background:'#4d4df5'
  },
    list: {
   // padding: theme.spacing(2),
    display: 'flex',
    height: 130,
    width:162,
    flexDirection: 'column',
   // background:'#fbb434'
  },
  closed: {
    //padding: theme.spacing(2),
    display: 'flex',
    height: 130,
    width:162,
    flexDirection: 'column',
    //background:'#7da924'
  },

  priority: {
    padding: theme.spacing(2),
    display: 'flex',
    height: 140,
    flexDirection: 'column',
   // background:'#fbb434'
  },
  
  fixedHeight: {
    height: 290,
  },
  name:{
   //fontSize:'16px',
   padding:'12px'
  },
   
  numbers:{
    fontSize: '32px',
   // padding: '6px',
    lineHeight: '1.5em',
    paddingRight: '23%',
  },

  high:{
    padding:'10px',
    color:'#4d4df5'
  },
  medium:{
    padding:'10px',
    color:'#fbb434'
  },
  low:{
    padding:'10px',
    color:'#7da924'
  },
  table:{
   marginTop:'3%'
  },
  button:{
    height: '50px',
    width: '50px',
      borderRadius:'50%',
      backgroundColor:'#16a980d4',
      textAlign:'center',
  },
    
}));

function generate(element) {
  return [0, 1, 2].map((value) =>
    React.cloneElement(element, {
      key: value,
    }),
  );
}

export default function Dashboard() {
  const classes = useStyles();
  
  const [dense, setDense] = React.useState(false);
  const [secondary, setSecondary] = React.useState(false);

  // const handleDrawerClose = () => {
  //   setOpen(false);
  // };
  const fixedHeightPaper = clsx(classes.paper, classes.fixedHeight);
  const BaseURL="http://83.136.219.147/trafiksol/public/api/v1"

  const postData1=async(url,body)=>{
      var response=await fetch(`${BaseURL}/${url}`,{
      method:"POST",
      mode:"cors",
      headers:{
       // "Authorization":"Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOlwvXC9hYXJvZ3lhaW5kaWEuaW5cL3RyYWZpa3NvbFwvcHVibGljXC9hcGlcL3YxXC9sb2dpbiIsImlhdCI6MTU5OTEyMzA1MiwiZXhwIjoxNjAxNzE1MDUyLCJuYmYiOjE1OTkxMjMwNTIsImp0aSI6IlJQVmttMHhmcFRzZHpHaGoiLCJzdWIiOjEsInBydiI6Ijg3ZTBhZjFlZjlmZDE1ODEyZmRlYzk3MTUzYTE0ZTBiMDQ3NTQ2YWEifQ.DFp2qevRXpL5DKJNSyrryVAnGD0BWXm-GiMVQeNq5LQ",
       "Authorization": "Bearer " + token,
       "Content-Type": "application/json; charset=utf-8"},
       body:JSON.stringify(body)})
       var result=await response.json()
    return result
  }
  
    let rec=JSON.parse(localStorage.getItem('LOGIN'))
     console.log(rec)
     let token=rec.data.user.token;
  
  
     let SiteItem=JSON.parse(localStorage.getItem('SELECTSITE'))
     console.log(SiteItem)
     let SiTeId = SiteItem.id;
     console.log(SiTeId)

     const [todayTicket,setTodayTicket]=React.useState('')
     const [monthTicket,setMonthTicket]=React.useState('')
     const [assignToMeTicket,setAssignToMeTicket]=React.useState('')
     const [assignByMeTicket,setAssignByMeTicket]=React.useState('')
     
     const getDashboardData=async()=>{
       const body={
         'site_id' : SiTeId,
         'list':0
             }
      console.log(body)           
      let result=await postData1('dashboard',body)
      console.log(result)  
      setTodayTicket(result.data.todaysTicket)
      setMonthTicket(result.data.monthTicket)
      setAssignToMeTicket(result.data.assignToMeTicket)
      setAssignByMeTicket(result.data.assignByMeTicket)
     }

     const [LowSlaTime,setLowSlaTime]=React.useState('')
     const getTicketByLowSlaTime=async()=>{
      const body={
        "site_id":SiTeId,
        "percentage":25
            }
     console.log(body)           
     let result=await postData1('tickets/getTicketByRemainingTime',body)
     console.log(result) 
     if(result.data!=''){
       setLowSlaTime('')
     } else{setLowSlaTime(0)}
         }

         const [MediumSlaTime,setMediumSlaTime]=React.useState('')
         const getTicketByMediumSlaTime=async()=>{
          const body={
            "site_id":SiTeId,
            "percentage":50
                }
         console.log(body)           
         let result=await postData1('tickets/getTicketByRemainingTime',body)
         console.log(result) 
         if(result.data!=''){
           setMediumSlaTime('')
         } else{setMediumSlaTime(0)}
             }

             const [HighSlaTime,setHighSlaTime]=React.useState('')
             const getTicketByHighSlaTime=async()=>{
              const body={
                "site_id":SiTeId,
                "percentage":100
                    }
             console.log(body)           
             let result=await postData1('tickets/getTicketByRemainingTime',body)
             console.log(result) 
             if(result.data!=''){
               setHighSlaTime('')
             } else{setHighSlaTime(0)}
                 }
  
     useEffect(()=>{
      getDashboardData()
      getTicketByLowSlaTime()
      getTicketByMediumSlaTime()
      getTicketByHighSlaTime()
  },[])

  const [today,setToday]=React.useState(false)
     const [month,setMonth]=React.useState(true)
     const [assignTo,setAssignTo]=React.useState(false)
     const [assignBy,setAssignBy]=React.useState(false)
     
  const handleToday=()=>{
    setToday(true)
    setMonth(false)
    setAssignBy(false)
    setAssignTo(false)
  }

  const handleMonth=()=>{
    setToday(false)
    setMonth(true)
    setAssignBy(false)
    setAssignTo(false)
  }
  const handleAssignBy=()=>{
    setToday(false)
    setMonth(false)
    setAssignBy(true)
    setAssignTo(false)
  }
  const handleAssignTo=()=>{
    setToday(false)
    setMonth(false)
    setAssignBy(false)
    setAssignTo(true)
  }
    

  return (
    <div component="main" className={classes.root}>
      <CssBaseline />

        <Container>

          <Grid container spacing={4}>

          {/* <Grid item xs={12} sm={12} md={2}>
            
            <Paper>
            <Typography variant="h6" style={{padding:"25px"}}>Ticket Status</Typography>
            </Paper>
          </Grid> */}

            {/* Chart */}
            <Grid item xs={6} sm={6} md={3}>

              <Paper onClick={handleToday}>
              {/* <p className={classes.name}>Pending Tickets</p> */}
              <Typography className={classes.name}>Today Tickets</Typography>
              <Grid container spacing={2}>
                <Grid item xs={12} sm={6} md={6}>
            <img src="./images/Due_Tickets.png" style={{paddingLeft:"7%",width:"50px"}} />
              </Grid>
          <Grid item xs={12} sm={6} md={6}>
            <Typography variamt="h6" className={classes.numbers}>
            {todayTicket}
          </Typography>
          </Grid>
                </Grid>
      
              </Paper>
            </Grid>

            {/* Recent Deposits */}
            <Grid item xs={6} sm={6} md={3}>
              <Paper onClick={handleMonth}>
              <Typography className={classes.name}>Month Tickets</Typography>
              <Grid container spacing={2}>
                <Grid item xs={12} sm={6} md={6}>
                <img src="./images/New_Tickets.png" style={{paddingLeft:"7%",width:"50px"}} />
                </Grid>
                <Grid item xs={12} sm={6} md={6}>
                <Typography variamt="h6" className={classes.numbers}>
                       {monthTicket}
                    </Typography>
                </Grid>
                </Grid>
            
              </Paper>
            </Grid>
            <Grid item xs={6} sm={6} md={3}>
              <Paper onClick={handleAssignTo}>
              <Typography className={classes.name}>Assigned To Me</Typography>
              <Grid container spacing={2}>
                <Grid item xs={12} sm={6} md={6}>
                <img src="./images/High_Priority.png" style={{paddingLeft:"7%",width:"50px"}} />
                </Grid>
                <Grid item xs={12} sm={6} md={6}>
                <Typography variamt="h6" className={classes.numbers}>
                       {assignToMeTicket}
                    </Typography>
                </Grid>
                </Grid>
            
              </Paper>
            </Grid>
            <Grid item xs={6} sm={6} md={3}>
              <Paper onClick={handleAssignBy}>
              <Typography className={classes.name}>Created By Me</Typography>
              <Grid container spacing={2}>
                <Grid item xs={12} sm={6} md={6}>
                <img src="./images/New_Tickets.png" style={{paddingLeft:"7%",width:"50px"}} />
                </Grid>
                <Grid item xs={12} sm={6} md={6}>
                <Typography variamt="h6" className={classes.numbers}>
                       {assignByMeTicket}
                    </Typography>
                </Grid>
                </Grid>
            
              </Paper>
            </Grid>
            
          </Grid>
       

           {/*-----------Table And SLA View---------- */}
           <Grid container spacing={2}>
          <Grid item xs={12} md={8} className={classes.table}>
         {/* <Paper> */}
         {today && <TodayTicket/>}
        {month && <MonthTicket/>}
        {assignTo && <AssignToMe/>}
        {assignBy && <AssignByMe/>}
         {/* </Paper> */}
            </Grid>

            <Grid item xs={12} md={4} className={classes.table}>
              <Paper>
              <Typography component="h1" variant="h6" color="inherit" noWrap className={classes.title}>
            SLA Status
          </Typography>
          <ColoredLine color="#1e83a9" />
            <div className={classes.demo}>
            <List dense={dense}>

            <ListItem> 
            <ListItemText><b>Remaining Time</b></ListItemText>
                  <ListItemSecondaryAction>
                  <ListItemText><b>No. of Tickets</b></ListItemText>
                  </ListItemSecondaryAction>
                </ListItem>
              <Divider/>
                <ListItem> 
                  <ListItemText
                    primary="More than 50%"
                    secondary={secondary ? 'Secondary text' : null}
                  />
                  <ListItemSecondaryAction>
                  <ListItemAvatar>
                    <Avatar style={{backgroundColor:'green'}}>
                      {HighSlaTime}
                    </Avatar>
                  </ListItemAvatar>
                  </ListItemSecondaryAction>
                </ListItem>
                <ListItem> 
                  <ListItemText
                    primary="50% to 25%"
                    secondary={secondary ? 'Secondary text' : null}
                  />
                  <ListItemSecondaryAction>
                  <ListItemAvatar>
                    <Avatar style={{backgroundColor:'orange'}}>
                      {MediumSlaTime}
                    </Avatar>
                  </ListItemAvatar>
                  </ListItemSecondaryAction>
                </ListItem>
                <ListItem> 
                  <ListItemText
                    primary="Less than 25%"
                    secondary={secondary ? 'Secondary text' : null}
                  />
                  <ListItemSecondaryAction>
                  <ListItemAvatar>
                    <Avatar style={{backgroundColor:'red'}}>
                     {LowSlaTime}
                    </Avatar>
                  </ListItemAvatar>
                  </ListItemSecondaryAction>
                </ListItem>
              
            </List>
          </div>
          </Paper>
          </Grid>

          </Grid>

        </Container>
    
    </div>
  );
}