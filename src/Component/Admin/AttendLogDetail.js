import React,{useEffect} from 'react';
import Link from '@material-ui/core/Link';
import { makeStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import {TableRow} from '@material-ui/core';
import Typography from '@material-ui/core/Typography';
import { Grid ,TableContainer,Paper,Hidden,Button,TablePagination} from '@material-ui/core';
import IconButton from '@material-ui/core/IconButton';
import PhotoCamera from '@material-ui/icons/PhotoCamera';
import ArrowBackIosIcon from '@material-ui/icons/ArrowBackIos';


import Attendance from './Attendance';
import {postData} from '../FetchServices';


const ColoredLine = ({ color }) => (
    <hr
        style={{
            color: color,
            backgroundColor: color,
            height: 1,
            marginTop: "-1%"
        }}
    />
  );

const columns = [

  { id: 'name', label: 'Login/Logout', minWidth: 60 },
  
  {  label: 'Time', minWidth: 60, align:'left' },
  { label: 'Geolocation', minWidth: 60 },
  
  { id: 'action', label: 'Image', minWidth: 70},

];

const useStyles = makeStyles((theme) => ({
  seeMore: {
    marginTop: theme.spacing(3),
  },
  formControl: {
    marginRight: theme.spacing(2),
   /// minWidth: 400,
    backgroundColor:'#aaaaaa2e',
    width:'100%',
  },
  new:{
    marginTop:'8%',
  },
  backIcon:{
    fontSize:'medium'
  },
  back:{
    backgroundColor:'#1e83a9',
    color:'#fff',
    marginTop:'11%',
    marginLeft:'18%',
  
    '&:hover': {
   // color:'primary',
    fontStyle:"bold",
    backgroundColor:'#1e83a9',
    },
    },
    addRespons: {
      backgroundColor:'#1e83a9',
      color: 'white',
      padding: '0px',
      textAlign: 'center',
      fontSize: '12px',
      marginTop: '16%',
    },
    rowdata:{
      color:'#1e83a9'
    },
    root: {
      width: '100%',
    },
    container: {
      maxHeight: 440,
    },
    typo:{
      marginTop:'1%'
    },

}));

export default function AttendanceLogDetail(props) {
  const classes = useStyles();
  const [page, setPage] = React.useState(0);
  const [rowsPerPage, setRowsPerPage] = React.useState(10);
  
  const handleChangePage = (event, newPage) => {
    setPage(newPage);
  };

  const handleChangeRowsPerPage = (event) => {
    setRowsPerPage(+event.target.value);
    setPage(0);
  };

 
  const BaseURL="http://83.136.219.147/trafiksol/public/api/v1"

  const postData1=async(url,body)=>{
      var response=await fetch(`${BaseURL}/${url}`,{
      method:"POST",
      mode:"cors",
      headers:{
       // "Authorization":"Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOlwvXC9hYXJvZ3lhaW5kaWEuaW5cL3RyYWZpa3NvbFwvcHVibGljXC9hcGlcL3YxXC9sb2dpbiIsImlhdCI6MTU5OTEyMzA1MiwiZXhwIjoxNjAxNzE1MDUyLCJuYmYiOjE1OTkxMjMwNTIsImp0aSI6IlJQVmttMHhmcFRzZHpHaGoiLCJzdWIiOjEsInBydiI6Ijg3ZTBhZjFlZjlmZDE1ODEyZmRlYzk3MTUzYTE0ZTBiMDQ3NTQ2YWEifQ.DFp2qevRXpL5DKJNSyrryVAnGD0BWXm-GiMVQeNq5LQ",
       "Authorization": "Bearer " + token,
       "Content-Type": "application/json; charset=utf-8"},
       body:JSON.stringify(body)})
       var result=await response.json()
    return result
  }
  
    let rec=JSON.parse(localStorage.getItem('LOGIN'))
     console.log(rec)
     let token=rec.data.user.token;

  const [rows,setRows]=React.useState([])
  const [LogDate,setLogDate]=React.useState('')

  const readAllRecords=async(date)=>{

    var body = {
      'date':date
    }
    console.log(body)
  
       let result= await postData1('users/getAttendance',body)
     console.log(result)
  
    setRows(result.data.data)
   }
  
   useEffect(()=>{
    readAllRecords(props.date)
    setLogDate(props.viewDate)
  },[])

  
const View=()=>{
  return (
    console.log(rows),
    console.log(LogDate),
    <React.Fragment>


    <div className={classes.new}>
   <Paper>
   <Grid container spacing={2}>
   
   <Grid item xs={2} sm={2} md={1}>
   <Hidden mdDown implementation="css">
       <Button className={classes.back}
        onClick={()=>handleClick(<Attendance changeView={props.changeView}/>)}>
        <ArrowBackIosIcon className={classes.backIcon}/>
Back</Button> 
</Hidden><Hidden mdUp implementation="css">
        <button size="small" aria-label="add" className={classes.addRespons}>
     <ArrowBackIosIcon onClick={()=>handleClick(<Attendance changeView={props.changeView}/>)} /></button>
        </Hidden>
        </Grid>

       <Grid item xs={6} sm={6} md={8}>
       <Typography variant="h6" className={classes.typo}>Attendance Log Details</Typography>
   
       </Grid>

       <Grid item xs={3} sm={3} md={2}>
       
       <h3 style={{color:"#1e83a9"}}>{LogDate}</h3>
       </Grid>
   
       </Grid>
         <ColoredLine color="grey" />
   
      </Paper>

      <Paper className={classes.root}>

<TableContainer className={classes.container}>
<Table stickyHeader aria-label="sticky table">
  <TableHead>
    <TableRow>
      {columns.map((column) => (
        <TableCell
          key={column.id}
          align={column.align}
          style={{ minWidth: column.minWidth }}
        ><b>
          {column.label}
          </b>
        </TableCell>
      ))}
    </TableRow>
  </TableHead>
 
  <TableBody>

        {rows.slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage).map(row=>(
          <TableRow key={row.id}>
       
                 <TableCell className={classes.rowdata}>{row.entry_type}</TableCell>
                 <TableCell className={classes.rowdata}>{row.time_column}</TableCell>
                 <TableCell className={classes.rowdata}>{row.geo_point}</TableCell>
     <TableCell className={classes.rowdata}>
     <PhotoCamera />
   </TableCell>
   </TableRow>
      ))}

        </TableBody>
</Table>
</TableContainer>
<TablePagination
rowsPerPageOptions={[10, 25, 100]}
component="div"
count={rows.length}
rowsPerPage={rowsPerPage}
page={page}
onChangePage={handleChangePage}
onChangeRowsPerPage={handleChangeRowsPerPage}
/>

</Paper>
      </div>
   
       </React.Fragment>
  )}

  const handleClick=(view)=>{
    props.changeView(view)
    }

  return (
    <div>
         {View()}
    </div>
   );
}