import React,{useEffect} from 'react';
import Avatar from '@material-ui/core/Avatar';

import Button from '@material-ui/core/Button';
import CssBaseline from '@material-ui/core/CssBaseline';

import LockOutlinedIcon from '@material-ui/icons/LockOutlined';
import {Radio ,RadioGroup,FormControlLabel,Typography,Container,Grid,TextField,AppBar,Toolbar,Paper
,FormControl,InputBase,Input,Switch }
 from '@material-ui/core';

 import clsx from 'clsx';
import { makeStyles } from '@material-ui/core/styles';

import EditProfile from './EditProfile';

const ColoredLine = ({ color }) => (
  <hr
      style={{
          color: color,
          backgroundColor: color,
          height: 5,
          marginBottom:'7%'
      }}
  />
); 

const useStyles = makeStyles((theme) => ({
    
  paper: {
    marginTop: theme.spacing(4),
    display: 'flex',
    flexDirection: 'column',
    width:'100%',
    //marginLeft:'10%'
    alignItems: 'center',
  },

  img: {
   
    marginBottom:'-3%'
  },
  avatar: {
    padding:14,
    //float:'left',
    width: 130,
    height: 130,
    marginBottom: '50px',
  },

  form: {
    width: '100%', // Fix IE 11 issue.
    marginTop: theme.spacing(3),
  },
  submit: {
    marginTop: '5%',
    marginBottom: '4%',
    backgroundColor:'#1e83a9',
    color:'#fff',
    //width:'40%'
   // float:'right',
  // marginRight:'9%',
    
    '&:hover': {
  backgroundColor:'#1e83a9',
  fontStyle:"bold" 
  },
  },
  
  title: {
    flexGrow: 1,
   // textAlign:'left',
    padding:'15px',
  },
  formControl: {
    marginBottom: theme.spacing(3),
   /// minWidth: 400,
    backgroundColor:'#aaaaaa2e',
    width:'100%',
    
  },
   input:{
    marginBottom: theme.spacing(3),
    backgroundColor:'#aaaaaa2e'
},
typo:{
    textAlign:'left'
},
new:{
  marginLeft: theme.spacing(2),
  marginRight: theme.spacing(2),
  width:'50%',
},
icon: {
  borderRadius: '50%',
  width: 16,
  height: 16,
  boxShadow: 'inset 0 0 0 1px rgba(16,22,26,.2), inset 0 -1px 0 rgba(16,22,26,.1)',
  backgroundColor: '#f5f8fa',
  backgroundImage: 'linear-gradient(180deg,hsla(0,0%,100%,.8),hsla(0,0%,100%,0))',
  '$root.Mui-focusVisible &': {
    outline: '2px auto rgba(19,124,189,.6)',
    outlineOffset: 2,
  },
  'input:hover ~ &': {
    backgroundColor: '#ebf1f5',
  },
  
},
checkedIcon: {
  backgroundColor: '#137cbd',
  backgroundImage: 'linear-gradient(180deg,hsla(0,0%,100%,.1),hsla(0,0%,100%,0))',
  '&:before': {
    display: 'block',
    width: 16,
    height: 16,
    backgroundImage: 'radial-gradient(#fff,#fff 28%,transparent 32%)',
    content: '""',
  },
  'input:hover ~ &': {
    backgroundColor: '#106ba3',
  },
},
}));


function StyledRadio(props) {
  const classes = useStyles();

  return (
    <Radio
      className={classes.root}
      disableRipple
      color="default"
      checkedIcon={<span className={clsx(classes.icon, classes.checkedIcon)} />}
      icon={<span className={classes.icon} />}
      {...props}
    />
  );
}

export default function Profile(props) {
  const classes = useStyles();
  const [name,setName] = React.useState('')
  const [email,setEmail] = React.useState('')
  const [phone,setPhone] = React.useState('')
  const [gender,setGender] = React.useState('')
  const [image,setImage] = React.useState('')
  // const [token,setToken] = React.useState('')
  const [state, setState] = React.useState({
    checkedA: true,
    checkedB: true,
  });

  const handleChange = (event) => {
    setState({ ...state, [event.target.name]: event.target.checked });
  };

  useEffect(function(){
    readProfileData(); 
  },[])
 
  const [error,setError]=React.useState(false)
  const [blank,setBlank]=React.useState(true)
  const [male,setMale]=React.useState(false)
  const [female,setFemale]=React.useState(true)

  const readProfileData=async()=>{
    const body={
      'name' : '',
      'file[]':'',
      'phone':'',
      
                }
   console.log(body)
               
   let result=await postData1('editMyProfile',body)
   console.log(result)
       
   setName(result.data.name)
   setEmail(result.data.email)
   setPhone(result.data.phone)
   setGender(result.data.gender)

   if(result.data.image==null){
    setError(true)
    setBlank(false)
 }
  else{
    setImage(result.data.image)
    setBlank(true)
  }
  if(result.data.gender=='M'){
      setMale(true)
      setFemale(false)
  }else{
    setMale(false)
    setFemale(true)
  }
  }

  const BaseURL="http://83.136.219.147/trafiksol/public/api/v1"

const postData1=async(url,body)=>{
    var response=await fetch(`${BaseURL}/${url}`,{
    method:"POST",
    mode:"cors",
    headers:{
     // "Authorization":"Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOlwvXC9hYXJvZ3lhaW5kaWEuaW5cL3RyYWZpa3NvbFwvcHVibGljXC9hcGlcL3YxXC9sb2dpbiIsImlhdCI6MTU5OTEyMzA1MiwiZXhwIjoxNjAxNzE1MDUyLCJuYmYiOjE1OTkxMjMwNTIsImp0aSI6IlJQVmttMHhmcFRzZHpHaGoiLCJzdWIiOjEsInBydiI6Ijg3ZTBhZjFlZjlmZDE1ODEyZmRlYzk3MTUzYTE0ZTBiMDQ3NTQ2YWEifQ.DFp2qevRXpL5DKJNSyrryVAnGD0BWXm-GiMVQeNq5LQ",
     "Authorization": "Bearer " + token,
     "Content-Type": "application/json; charset=utf-8"},
     body:JSON.stringify(body)})
     var result=await response.json()
  return result
}

  let rec=JSON.parse(localStorage.getItem('LOGIN'))
   console.log(rec)
   let token=rec.data.user.token;

  //  const setuserdata = () =>{
  //   let rec=JSON.parse(localStorage.getItem('LOGIN'))
  //    console.log(rec)
  //    setName(rec.data.user.name)
  //    setEmail(rec.data.user.email)
  //    setPhone(rec.data.user.phone)
  //    setGender(rec.data.user.gender)
  //    setToken(rec.data.user.token)

  //    if(rec.data.user.image==null){
  //     setError(true)
  //     setBlank(false)
  //  }
  //   else{
  //     setImage(rec.data.user.image)
  //     setBlank(true)
  //   }
  //   if(rec.data.user.gender=='M'){
  //       setMale(true)
  //       setFemale(false)
  //   }else{
  //     setMale(false)
  //     setFemale(true)
  //   }
  // }

 
const View=()=>{
  return (
    <Container component="main">
      <CssBaseline />
      
      <Paper className={classes.paper}>

      
 
       <div className={classes.new}>
        <form className={classes.form} noValidate>
          
        
           <Typography component="h1" variant="h6" color="inherit" noWrap className={classes.title}>
          User Profile
          </Typography>

          <ColoredLine color="#1e83a9" />

   
         
         {/* <Grid item xs={12} sm={4} md={4} className={classes.img}> */}
         {
        error && <img alt="Person" className={classes.avatar}  src="./images/Avatar.jpg"/>
      }
          {
          blank && <img className={classes.avatar}  src={image} />
          }
        
       {/* </Grid> */}
       


       {/*---------Container for Detail Start-------------- */}

         
 <Grid container maxWidth="md">

<Grid item xs={12} sm={12} md={12}>           
           

            <Grid container spacing={2}>
            <Grid item xs={12} sm={4} md={5}>
              <Typography>Name</Typography>
              </Grid>
              <Grid item xs={12} sm={8} md={7}>
              <TextField disabled id="standard-disabled" fullWidth className={classes.input} value={name} />
              {/* <TextField
              value={name}
              onChange={(event)=>setName(event.target.value)}
            className={classes.input}
            placeholder="                      Name" 
            id="standard-disabled"
            fullWidth 
            type="text"
            /> */}
              </Grid>
              </Grid>
              <Grid container spacing={2}>
            <Grid item xs={12} sm={4} md={5}>
              <Typography>Email</Typography>
              </Grid>
              <Grid item xs={12} sm={8} md={7}>
              <TextField disabled id="standard-disabled" fullWidth className={classes.input} value={email} />
              {/* <Input
              value={email}
              onChange={(event)=>setEmail(event.target.value)}
            className={classes.input}
            placeholder="                      Email" 
            required
            fullWidth 
            name="email"
            type="email"
            inputProps={{ 'aria-label': 'description' }} /> */}
              </Grid>
              </Grid>


           {/* <Grid container spacing={2}>
            <Grid item xs={6} sm={7} md={6}>
              <Typography className={classes.typo}>SMS Alert Activation</Typography>
              </Grid>
              <Grid item xs={4} sm={2} md={4}>
              <Switch
        checked={state.checkedB}
        onChange={handleChange}
        color="primary"
        name="checkedB"
        inputProps={{ 'aria-label': 'primary checkbox' }}
      />
              </Grid>
              </Grid> */}

              
     
            

             <Grid container spacing={2}>
            <Grid item xs={12} sm={4} md={5}>
              <Typography>Phone</Typography>
              </Grid>
              <Grid item xs={12} sm={8} md={7}>
              <TextField disabled id="standard-disabled" fullWidth className={classes.input} value={phone} />
              {/* <Input
              value={phone}
              onChange={(event)=>setPhone(event.target.value)}
            className={classes.input}
            placeholder="                     Phone" 
            required
            fullWidth 
            name="phone"
            type="number"
            inputProps={{ 'aria-label': 'description' }} /> */}
              </Grid>
              </Grid>

              <Grid container spacing={2}>
            <Grid item xs={12} sm={4} md={5}>
              <Typography>Gender</Typography>
              </Grid>
              <Grid item xs={12} sm={8} md={7}>
              {
        male && <TextField disabled id="standard-disabled" fullWidth className={classes.input} value="Male" />
      }
       {
        female && <TextField disabled id="standard-disabled" fullWidth className={classes.input} value="Female" />
      }
              
              {/* <FormControl component="fieldset">
      
      <RadioGroup defaultValue="female" aria-label="gender" name="customized-radios">
        <FormControlLabel value="female" control={<StyledRadio />} label="Female" />
        <FormControlLabel value="male" control={<StyledRadio />} label="Male" />
        
      </RadioGroup>
    </FormControl> */}
              </Grid>
              </Grid>


              

               </Grid>

            
          </Grid>


          <Button
              onClick={()=>handleClick(<EditProfile changeView={props.changeView}/>)}
              variant="contained"
              color="green"
              className={classes.submit}
            >
              Edit
            </Button>
         
        </form>
        </div>
        </Paper>
      
      
    </Container>
  );
}
const handleClick=(view)=>{
  props.changeView(view)
  }

return (
  <div>
       {View()}
  </div>
 );
}