import React,{useEffect} from 'react';
import Avatar from '@material-ui/core/Avatar';

import Button from '@material-ui/core/Button';
import CssBaseline from '@material-ui/core/CssBaseline';

import LockOutlinedIcon from '@material-ui/icons/LockOutlined';
import {MenuItem ,ListItem,ListItemAvatar,ListItemText,Container,List,ListItemSecondaryAction,AppBar,Toolbar,Paper
,Hidden,Typography,Input,Menu }
 from '@material-ui/core';

 import clsx from 'clsx';
import { makeStyles,withStyles } from '@material-ui/core/styles';
import CommentIcon from '@material-ui/icons/Comment';
import DeleteIcon from '@material-ui/icons/Delete';
import IconButton from '@material-ui/core/IconButton';

import EditProfile from './EditProfile';

const ColoredLine = ({ color }) => (
  <hr
      style={{
          color: color,
          backgroundColor: color,
          height: 5,
          marginBottom:'7%'
      }}
  />
); 

const useStyles = makeStyles((theme) => ({
    
  paper: {
    marginTop: theme.spacing(4),
    display: 'flex',
    flexDirection: 'column',
    width:'100%',
    //marginLeft:'10%'
    alignItems: 'center',
  },

  img: {
   
    marginBottom:'-3%'
  },
  avatar: {
    padding:14,
    //float:'left',
    width: 130,
    height: 130,
    marginBottom: '50px',
  },

  form: {
    width: '100%', // Fix IE 11 issue.
    marginTop: theme.spacing(3),
  },
  submit: {
    marginTop: '5%',
    marginBottom: '4%',
    backgroundColor:'#1e83a9',
    color:'#fff',
    //width:'40%'
   // float:'right',
  // marginRight:'9%',
    
    '&:hover': {
  backgroundColor:'#1e83a9',
  fontStyle:"bold" 
  },
  },
  
  title: {
    flexGrow: 1,
   // textAlign:'left',
    padding:'15px',
  },
  formControl: {
    marginBottom: theme.spacing(3),
   /// minWidth: 400,
    backgroundColor:'#aaaaaa2e',
    width:'100%',
    
  },
   input:{
    marginBottom: theme.spacing(3),
    backgroundColor:'#aaaaaa2e'
},
typo:{
    textAlign:'left'
},
new:{
  marginLeft: theme.spacing(2),
  marginRight: theme.spacing(2),
  width:'100%',
},
icon: {
  borderRadius: '50%',
  width: 16,
  height: 16,
  boxShadow: 'inset 0 0 0 1px rgba(16,22,26,.2), inset 0 -1px 0 rgba(16,22,26,.1)',
  backgroundColor: '#f5f8fa',
  backgroundImage: 'linear-gradient(180deg,hsla(0,0%,100%,.8),hsla(0,0%,100%,0))',
  '$root.Mui-focusVisible &': {
    outline: '2px auto rgba(19,124,189,.6)',
    outlineOffset: 2,
  },
  'input:hover ~ &': {
    backgroundColor: '#ebf1f5',
  },
  
},
checkedIcon: {
  backgroundColor: '#137cbd',
  backgroundImage: 'linear-gradient(180deg,hsla(0,0%,100%,.1),hsla(0,0%,100%,0))',
  '&:before': {
    display: 'block',
    width: 16,
    height: 16,
    backgroundImage: 'radial-gradient(#fff,#fff 28%,transparent 32%)',
    content: '""',
  },
  'input:hover ~ &': {
    backgroundColor: '#106ba3',
  },
},
}));

const StyledMenu = withStyles({
    paper: {
      border: '1px solid #d3d4d5',
    },
  })((props) => (
    <Menu
      elevation={0}
      getContentAnchorEl={null}
      anchorOrigin={{
        vertical: 'bottom',
        horizontal: 'center',
      }}
      transformOrigin={{
        vertical: 'top',
        horizontal: 'center',
      }}
      {...props}
    />
  ));
  
  const StyledMenuItem = withStyles((theme) => ({
    root: {
      '&:focus': {
        backgroundColor: theme.palette.primary.main,
        '& .MuiListItemIcon-root, & .MuiListItemText-primary': {
          color: theme.palette.common.white,
        },
      },
    },
  }))(MenuItem);

export default function NoticeResponse(props) {
  const classes = useStyles();

  const [dense, setDense] = React.useState(false);

  const [anchorEl, setAnchorEl] = React.useState(null);
  const handleClose = () => {
    setAnchorEl(null);
  };

  const BaseURL="http://83.136.219.147/trafiksol/public/api/v1"

  const postData1=async(url,body)=>{
      var response=await fetch(`${BaseURL}/${url}`,{
      method:"POST",
      mode:"cors",
      headers:{
       // "Authorization":"Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOlwvXC9hYXJvZ3lhaW5kaWEuaW5cL3RyYWZpa3NvbFwvcHVibGljXC9hcGlcL3YxXC9sb2dpbiIsImlhdCI6MTU5OTEyMzA1MiwiZXhwIjoxNjAxNzE1MDUyLCJuYmYiOjE1OTkxMjMwNTIsImp0aSI6IlJQVmttMHhmcFRzZHpHaGoiLCJzdWIiOjEsInBydiI6Ijg3ZTBhZjFlZjlmZDE1ODEyZmRlYzk3MTUzYTE0ZTBiMDQ3NTQ2YWEifQ.DFp2qevRXpL5DKJNSyrryVAnGD0BWXm-GiMVQeNq5LQ",
       "Authorization": "Bearer " + token,
       "Content-Type": "application/json; charset=utf-8"},
       body:JSON.stringify(body)})
       var result=await response.json()
    return result
  }
  
    let rec=JSON.parse(localStorage.getItem('LOGIN'))
     console.log(rec)
     let token=rec.data.user.token;
  
     const [list,setList]=React.useState([])
     const [count,setCount]=React.useState('')
    const [error,setError]=React.useState(false)
  
    const readNotifications=async()=>{
  
      let result= await postData1('ajaxNotificationData')
       console.log(result)
       if(result.ntcount==0){
       setCount('0')
       }
       else{
         setCount(result.ntcount)
       }
       if(result.data!=''){
        setList(result.data)
    }
      else{
        setError(true)
       }
     }
  
     useEffect(()=>{
      readNotifications()
  },[])

 
const View=()=>{
  return (
    <Container component="main">
      <CssBaseline />
      
      <Paper className={classes.paper}>

      
 
       <div className={classes.new}>


                  <div>
                  {
        error && <div style={{color: `red`}}>No Notifications</div>
      }
       <Typography variant="h6" className={classes.title}>
            Notifications
          </Typography>
                <div>
                <List dense={dense} className={classes.content}>
                {list.map(item=>(
                  <MenuItem>
                    <ListItem>
                      <ListItemAvatar>
                        <Avatar>
                        <CommentIcon />
                        </Avatar>
                     </ListItemAvatar>
                      <ListItemText
                        primary={item.notification_message}
                        secondary={item.subject}
                      />
                    <Hidden xsDown implementation="css">
                     <ListItemText
                       edge="end"
                        secondary={item.created}
                      />
                      </Hidden>
                  
                     
                    </ListItem>
                    </MenuItem>
                  ) )}
                </List>
              </div>
            
                

                </div>
               
            
        </div>
        </Paper>
      
      
    </Container>
  );
}
const handleClick=(view)=>{
  props.changeView(view)
  }

return (
  <div>
       {View()}
  </div>
 );
}