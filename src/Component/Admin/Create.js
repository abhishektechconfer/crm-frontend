import React,{useEffect} from 'react';
import {  makeStyles, useTheme ,withStyles } from '@material-ui/core/styles';

import {ListItem,
  Divider,
  Collapse,
  Button
} from '@material-ui/core';

import MenuItem from '@material-ui/core/MenuItem';
import {Menu,Avatar} from '@material-ui/core';
import AccountCircle from '@material-ui/icons/AccountCircle';
import IconButton from '@material-ui/core/IconButton';
import Modal from '@material-ui/core/Modal';
import Backdrop from '@material-ui/core/Backdrop';
import Fade from '@material-ui/core/Fade';

import Tooltip from '@material-ui/core/Tooltip';
import ClickAwayListener from '@material-ui/core/ClickAwayListener';

import Attendance from './Attendance';
import ChangeSite from './ChangeSite';
import Profile from './Profile';
import Password from './Password';

/////////////////////////////////////////////////////

const useStyles = makeStyles(theme => ({

  modal: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
  },
  paper: {
    backgroundColor: theme.palette.background.paper,
    border: '2px solid #000',
    boxShadow: theme.shadows[5],
    padding: theme.spacing(2, 4, 3),
  },
color:{

// fontStyle:"bold" ,

'&:hover': {
color:'#ff3278',
fontStyle:"bold" 
},
},

IconMargin:{
  marginRight:10,
},
avatar: {
  width: 25,
  height: 25
},


}));

const StyledMenu = withStyles({
  paper: {
    border: '1px solid #d3d4d5',
  },
})((props) => (
  <Menu
    elevation={0}
    getContentAnchorEl={null}
    anchorOrigin={{
      vertical: 'bottom',
      horizontal: 'center',
    }}
    transformOrigin={{
      vertical: 'top',
      horizontal: 'center',
    }}
    {...props}
  />
));

const StyledMenuItem = withStyles((theme) => ({
  root: {
    '&:focus': {
      backgroundColor: theme.palette.primary.main,
      '& .MuiListItemIcon-root, & .MuiListItemText-primary': {
        color: theme.palette.common.white,
      },
    },
  },
}))(MenuItem);




/////////////////////////////

export default function Create (props){
  const classes = useStyles();
  

  const [anchorEl, setAnchorEl] = React.useState(null);
  const open = Boolean(anchorEl);
  const [mobileOpen, setMobileOpen] = React.useState(false);


  const handleOpen = (event) => {
    setAnchorEl(event.currentTarget);
  };

  const handleClose = () => {
    setAnchorEl(null);
  };


  const [onn, setOnn] = React.useState(false);

  const handleOnn = () => {
    setOnn(true);
  };

  const handleOff = () => {
    setOnn(false);
  };

  const handleClick=(view)=>{
    props.changeView(view)
    }

    const [up, setUp] = React.useState(false);

    const handleTooltipClose = () => {
      setUp(false);
    };
  
    const handleTooltipOpen = () => {
      setUp(true);
    };


     
const BaseURL="http://83.136.219.147/trafiksol/public/api/v1"

const postData1=async(url,body)=>{
    var response=await fetch(`${BaseURL}/${url}`,{
    method:"POST",
    mode:"cors",
    headers:{
     // "Authorization":"Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOlwvXC9hYXJvZ3lhaW5kaWEuaW5cL3RyYWZpa3NvbFwvcHVibGljXC9hcGlcL3YxXC9sb2dpbiIsImlhdCI6MTU5OTEyMzA1MiwiZXhwIjoxNjAxNzE1MDUyLCJuYmYiOjE1OTkxMjMwNTIsImp0aSI6IlJQVmttMHhmcFRzZHpHaGoiLCJzdWIiOjEsInBydiI6Ijg3ZTBhZjFlZjlmZDE1ODEyZmRlYzk3MTUzYTE0ZTBiMDQ3NTQ2YWEifQ.DFp2qevRXpL5DKJNSyrryVAnGD0BWXm-GiMVQeNq5LQ",
     "Authorization": "Bearer " + token,
     "Content-Type": "application/json; charset=utf-8"},
     body:JSON.stringify(body)})
     var result=await response.json()
  return result
}

  let rec=JSON.parse(localStorage.getItem('LOGIN'))
   console.log(rec)
   let token=rec.data.user.token;
   
    const [image,setImage] = React.useState('')
    const readProfileData=async()=>{
      const body={
        'name' : '',
        'file[]':'',
        'phone':'',
        
                  }
     console.log(body)
                 
     let result=await postData1('editMyProfile',body)
     console.log(result)
     setImage(result.data.image)
    } 
    
    useEffect(function(){
      readProfileData(); 
    },[])

  const Create = (


       <div>
         <Tooltip disableFocusListener title="User Details">
               <IconButton
                aria-controls="customized-menu"
                aria-haspopup="true"
                variant="contained"
                color="inherit"
                onClick={handleOpen}
              >
           <Avatar alt="Person" className={classes.avatar}  src={image} />
              </IconButton>
              </Tooltip>
              <StyledMenu
        id="customized-menu"
        anchorEl={anchorEl}
        keepMounted
        open={Boolean(anchorEl)}
        onClose={handleClose}
      >
                <div>
      {/* <MenuItem onClick={handleOnn}>
        Change Site
      </MenuItem> */}
      <Modal
        aria-labelledby="transition-modal-title"
        aria-describedby="transition-modal-description"
        className={classes.modal}
        open={onn}
        onClose={handleOff}
        closeAfterTransition
        BackdropComponent={Backdrop}
        BackdropProps={{
          timeout: 500,
        }}
      >
        <Fade in={onn}>
          <div className={classes.paper}>
            <h2 id="transition-modal-title">Change Site</h2>
            <MenuItem id="transition-modal-description">Site 1</MenuItem>
            <MenuItem id="transition-modal-description">Site 2</MenuItem>
            <MenuItem id="transition-modal-description">Site 3</MenuItem>
            <MenuItem id="transition-modal-description">Site 4</MenuItem>
          </div>
        </Fade>
      </Modal>
    </div>
                <MenuItem  onClick={()=>handleClick(<Profile changeView={props.changeView}/>)}>View Profile</MenuItem>
                <MenuItem  onClick={()=>handleClick(<Password changeView={props.changeView}/>)}>Change Password</MenuItem>
                <MenuItem  onClick={()=>handleClick(<Attendance changeView={props.changeView}/>)}>Attendance Detail</MenuItem>
                </StyledMenu>


                
            </div>
   
  
  );
  return(<div>
    {Create}
  </div>);
 };

 