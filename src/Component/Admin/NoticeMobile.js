import React,{useEffect} from 'react';
import {  makeStyles, useTheme ,withStyles } from '@material-ui/core/styles';

import {ListItem,
  Divider,
  ListItemText,Button,
  Grid,ListItemAvatar,
  Paper,Typography
} from '@material-ui/core';

import MenuItem from '@material-ui/core/MenuItem';
import {Menu,Avatar,List} from '@material-ui/core';
import AccountCircle from '@material-ui/icons/AccountCircle';
import IconButton from '@material-ui/core/IconButton';
import Badge from '@material-ui/core/Badge';
import CommentIcon from '@material-ui/icons/Comment';

import Tooltip from '@material-ui/core/Tooltip';
import ClickAwayListener from '@material-ui/core/ClickAwayListener';
import NotificationsIcon from '@material-ui/icons/Notifications';
import NoticeResponse from './NoticeResponse';

/////////////////////////////////////////////////////

const useStyles = makeStyles(theme => ({
color:{

// fontStyle:"bold" ,

'&:hover': {
color:'#1e83a9',
fontStyle:"bold" 
},
},

IconMargin:{
  marginRight:10,
},
avatar: {
  width: 30,
  height: 30
},
content:{
    paddinTop:'0px',
    paddingBottom:'0px',
},

para:{
    width:'250px',
    paddingBottom: '3%',
    paddingLeft: '5%',
},
heading:{
    paddingLeft:'5%',
    paddingTop:'4%',
    lineHeight:'0.5em'
},
inline: {
  display: 'inline',
},


}));

const StyledMenu = withStyles({
  paper: {
    border: '1px solid #d3d4d5',
  },
})((props) => (
  <Menu
    elevation={0}
    getContentAnchorEl={null}
    anchorOrigin={{
      vertical: 'bottom',
      horizontal: 'center',
    }}
    transformOrigin={{
      vertical: 'top',
      horizontal: 'center',
    }}
    {...props}
  />
));

const StyledMenuItem = withStyles((theme) => ({
  root: {
    '&:focus': {
      backgroundColor: theme.palette.primary.main,
      '& .MuiListItemIcon-root, & .MuiListItemText-primary': {
        color: theme.palette.common.white,
      },
    },
  },
}))(MenuItem);



/////////////////////////////

export default function NoticeMobile (props){
  const classes = useStyles();

  const [dense, setDense] = React.useState(false);

  const [anchorEl, setAnchorEl] = React.useState(null);
  
  const open = Boolean(anchorEl);
  const [mobileOpen, setMobileOpen] = React.useState(false);
  

  const handleOpen = (event) => {
    setAnchorEl(event.currentTarget);
  };

  const handleClose = () => {
    setAnchorEl(null);
  };

  const handleClick=(view)=>{
    props.changeView(view)
    }

    const [up, setUp] = React.useState(false);

    const handleTooltipClose = () => {
      setUp(false);
    };
  
    const handleTooltipOpen = () => {
      setUp(true);
    };

    const BaseURL="http://83.136.219.147/trafiksol/public/api/v1"

const postData1=async(url,body)=>{
    var response=await fetch(`${BaseURL}/${url}`,{
    method:"POST",
    mode:"cors",
    headers:{
     // "Authorization":"Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOlwvXC9hYXJvZ3lhaW5kaWEuaW5cL3RyYWZpa3NvbFwvcHVibGljXC9hcGlcL3YxXC9sb2dpbiIsImlhdCI6MTU5OTEyMzA1MiwiZXhwIjoxNjAxNzE1MDUyLCJuYmYiOjE1OTkxMjMwNTIsImp0aSI6IlJQVmttMHhmcFRzZHpHaGoiLCJzdWIiOjEsInBydiI6Ijg3ZTBhZjFlZjlmZDE1ODEyZmRlYzk3MTUzYTE0ZTBiMDQ3NTQ2YWEifQ.DFp2qevRXpL5DKJNSyrryVAnGD0BWXm-GiMVQeNq5LQ",
     "Authorization": "Bearer " + token,
     "Content-Type": "application/json; charset=utf-8"},
     body:JSON.stringify(body)})
     var result=await response.json()
  return result
}

  let rec=JSON.parse(localStorage.getItem('LOGIN'))
   console.log(rec)
   let token=rec.data.user.token;

   const [list,setList]=React.useState([])
   const [count,setCount]=React.useState('')
  const [error,setError]=React.useState(false)

  const readNotifications=async()=>{

    let result= await postData1('ajaxNotificationData')
     console.log(result)
     if(result.ntcount==0){
     setCount('0')
     }
     else{
       setCount(result.ntcount)
     }
     if(result.data!=''){
      setList(result.data)
  }
    else{
      setError(true)
     }
   }

   useEffect(()=>{
    readNotifications()
},[])

  const NoticeMobile = (


       <div>
         
          <Tooltip disableFocusListener title="Notifications">
              <IconButton
                aria-controls="customized-menu"
                aria-haspopup="true"
                variant="contained"
                //color="#1e83a9"
                onClick={()=>handleClick(<NoticeResponse changeView={props.changeView}/>)}
              >
           <Badge color="secondary" badgeContent={count}>
              <NotificationsIcon style={{color:"#1e83a9"}}/>
            </Badge>
              </IconButton>
             </Tooltip>  
              </div>


   
  );

  return(<div>
    {NoticeMobile}
  </div>);
 };

 