import React from 'react';
import Avatar from '@material-ui/core/Avatar';

import Button from '@material-ui/core/Button';
import CssBaseline from '@material-ui/core/CssBaseline';

import LockOutlinedIcon from '@material-ui/icons/LockOutlined';
import {Radio ,Typography,Container,InputBase,Grid,Paper
,Input }
 from '@material-ui/core';

 import clsx from 'clsx';
import { makeStyles } from '@material-ui/core/styles';

const ColoredLine = ({ color }) => (
  <hr
      style={{
          color: color,
          backgroundColor: color,
          height: 5
      }}
  />
);


const useStyles = makeStyles((theme) => ({
    
  paper: {
    marginTop: theme.spacing(4),
    display: 'flex',
    flexDirection: 'column',
    width:'100%',
    //marginLeft:'10%'
   alignItems: 'center',
  },
  img: {
   
    marginBottom:'-4%'
  },
  avatar: {
    padding:14,
    float:'left',
    width: 100,
    height: 100,
    marginBottom: '50px',
  },

  form: {
    width: '100%', // Fix IE 11 issue.
    marginTop: theme.spacing(3),
  },
  submit: {
    marginTop: '2%',
    marginBottom: '7%',
    backgroundColor:'#1e83a9',
    color:'#fff',
    //width:'40%'
    float:'right',
    //marginLeft:'9%',
    
    '&:hover': {
  backgroundColor:'#ff6a00',
  fontStyle:"bold" 
  },
  },
  
  title: {
    flexGrow: 1,
    textAlign:'center',
    padding:'15px',
   // marginBottom: '10%',
    //backgroundColor:'#1e83a9',
    color:'#1e83a9',
    //marginLeft: '4%',
  },
  formControl: {
    marginBottom: theme.spacing(3),
   /// minWidth: 400,
    backgroundColor:'#aaaaaa2e',
    width:'100%',
    
  },
   input:{
    marginBottom: theme.spacing(3),
    backgroundColor:'#aaaaaa2e'
},
typo:{
    textAlign:'left'
},
new:{
  marginLeft: theme.spacing(2),
  marginRight: theme.spacing(2),
},


}));


export default function Password() {
  const classes = useStyles();
 

  const BaseURL="http://83.136.219.147/trafiksol/public/api/v1"

  const postData1=async(url,body)=>{
      var response=await fetch(`${BaseURL}/${url}`,{
      method:"POST",
      mode:"cors",
      headers:{
       // "Authorization":"Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOlwvXC9hYXJvZ3lhaW5kaWEuaW5cL3RyYWZpa3NvbFwvcHVibGljXC9hcGlcL3YxXC9sb2dpbiIsImlhdCI6MTU5OTEyMzA1MiwiZXhwIjoxNjAxNzE1MDUyLCJuYmYiOjE1OTkxMjMwNTIsImp0aSI6IlJQVmttMHhmcFRzZHpHaGoiLCJzdWIiOjEsInBydiI6Ijg3ZTBhZjFlZjlmZDE1ODEyZmRlYzk3MTUzYTE0ZTBiMDQ3NTQ2YWEifQ.DFp2qevRXpL5DKJNSyrryVAnGD0BWXm-GiMVQeNq5LQ",
       "Authorization": "Bearer " + token,
       "Content-Type": "application/json; charset=utf-8"},
       body:JSON.stringify(body)})
       var result=await response.json()
    return result
  }
  
    let rec=JSON.parse(localStorage.getItem('LOGIN'))
     console.log(rec)
     let token=rec.data.user.token;

     const [old_password,setOld] = React.useState('')
     const [new_password,setNew] = React.useState('')
     const [confirm_password, setConfirm]=React.useState('')

     const ChangePassword=async()=>{
     // alert('hii')
  
        const body={
          "old_password":old_password,
          "new_password":new_password,
          "password_confirmation":confirm_password
              
                        }
           console.log(body)
                       
           let result=await postData1('changePassword',body)
           console.log(result)
                              
          if(result)
          {
            //alert("Your Password has been Changed..")
          clearValues()  
            }
      else
           {    alert("Fail to Change Password...")}  }

           const clearValues=()=>{
            setOld('')
            setNew('')
            setConfirm('')
                  }  

  return (
    <Container component="main">
      <CssBaseline />
      
      <div>

      <Paper className={classes.paper}>
 
       <div className={classes.new}>
        <form className={classes.form} noValidate>


           <Typography component="h1" variant="h6" color="inherit" noWrap className={classes.title}>
          Change Password
          </Typography>
          <ColoredLine color="#1e83a9" />

 <Grid container maxWidth="md">

<Grid item xs={12} sm={12} md={12}>

<Grid container spacing={2}>
<Grid item xs={12} sm={6} md={5}>
  <Typography>Old Password</Typography>
  </Grid>
  <Grid item xs={12} sm={6} md={7}>
  <Input
    value={old_password}
    onChange={(event)=>setOld(event.target.value)}
className={classes.input}
placeholder="            Old Password" 
required
fullWidth 
type="password"
inputProps={{ 'aria-label': 'description' }} />
  </Grid>
  </Grid>

  
  <Grid container spacing={2}>
<Grid item xs={12} sm={6} md={5}>
  <Typography>New Password</Typography>
  </Grid>
  <Grid item xs={12} sm={6} md={7}>
  <Input
    value={new_password}
    onChange={(event)=>setNew(event.target.value)}
className={classes.input}
placeholder="              New Password" 
required
fullWidth 
type="password"
inputProps={{ 'aria-label': 'description' }} />
  </Grid>
  </Grid>


  <Grid container spacing={2}>
<Grid item xs={12} sm={6} md={5}>
  <Typography>Confirm Password</Typography>
  </Grid>
  <Grid item xs={12} sm={6} md={7}>
 
  <Input
    value={confirm_password}
    onChange={(event)=>setConfirm(event.target.value)}
className={classes.input}
placeholder="          Confirm Password" 
required
fullWidth 
type="password"
inputProps={{ 'aria-label': 'description' }} />
  </Grid>
  </Grid>


  <Grid container spacing={2}>
<Grid item xs={12} sm={6} md={5}>
  <Typography></Typography>
  </Grid>
  <Grid item xs={12} sm={6} md={7}>
  <Button
          onClick={ChangePassword}
          variant="contained"
          color="green"
          className={classes.submit}
        >
          Save
        </Button>
  </Grid>
  </Grid>

  

</Grid>

</Grid> 

          
         
        </form>
        </div>
        </Paper>
      </div>
      
    </Container>
  );
}