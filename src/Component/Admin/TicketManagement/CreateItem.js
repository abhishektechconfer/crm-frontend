import React, { useEffect } from 'react';
import Avatar from '@material-ui/core/Avatar';
import Button from '@material-ui/core/Button';
import CssBaseline from '@material-ui/core/CssBaseline';

import LockOutlinedIcon from '@material-ui/icons/LockOutlined';
import {
  TextareaAutosize, Select, MenuItem, Typography, Container, InputBase, Grid, TextField, AppBar, Toolbar, Paper
  , FormControl, InputLabel, Hidden, Input, Link
}
  from '@material-ui/core';
import ArrowBackIosIcon from '@material-ui/icons/ArrowBackIos';

import { makeStyles } from '@material-ui/core/styles';
import { getData, postData } from '../../FetchServices';
import StoreItemList from './StoreItemList';

const ColoredLine = ({ color }) => (
  <hr
    style={{
      color: color,
      backgroundColor: color,
      height: 5
    }}
  />
);
const useStyles = makeStyles((theme) => ({

  paper: {
    marginTop: theme.spacing(4),
    display: 'flex',
    flexDirection: 'column',
    width: '100%',
    //marginLeft:'10%'
    // alignItems: 'center',
  },
  avatar: {
    width: 40,
    height: 40,
    marginLeft: '5%'
  },
  form: {
    width: '100%', // Fix IE 11 issue.
    marginTop: theme.spacing(3),
  },
  submit: {
    marginTop: '4%',
    marginBottom: '10%',
    backgroundColor: '#1e83a9',
    color: '#fff',
    //marginLeft:'45%',
    //float:'right',

    '&:hover': {
      backgroundColor: '#1e83a9',
      fontStyle: "bold"
    },
  },

  title: {
    flexGrow: 1,
    textAlign: 'center',
    /// marginRight:'5%',
    padding: '10px',
    //backgroundColor:'#1e83a9',
    color: '#1e83a9',
  },
  formControl: {
    marginBottom: theme.spacing(3),
    /// minWidth: 400,
    backgroundColor: '#aaaaaa2e',
    width: '100%',

  },
  input: {
    marginBottom: theme.spacing(3),
    backgroundColor: '#aaaaaa2e'
  },
  file: {
    backgroundColor: '#aaaaaa2e',
    borderColor: 'none',
    width: '100%'
  },
  field: {
    float: 'left'
  },
  image: {
    display: 'none',
  },
  upload: {
    backgroundColor: '#1e83a9',
    color: '#fff',
    borderRadius: '0px',
    boxShadow: '0px',
    height: '33px',

    '&:hover': {
      // color:'primary',
      fontStyle: "bold",
      backgroundColor: '#1e83a9',
    },
  },
  new: {
    marginLeft: theme.spacing(3),
    marginRight: theme.spacing(3),
  },
  addRespons: {
    backgroundColor: '#1e83a9',
    color: 'white',
    padding: '0px',
    textAlign: 'center',
    fontSize: '12px',
    marginTop: '16%',
  },
  back: {
    backgroundColor: '#1e83a9',
    color: '#fff',
    marginTop: '8%',
    '&:hover': {
      // color:'primary',
      fontStyle: "bold",
      backgroundColor: '#1e83a9',
    },
  },
}));

export default function ReturnStore(props) {
  const classes = useStyles();

  const handleClick = (view) => {
    props.changeView(view)
  }
  const [BrandId, setBrandId] = React.useState('')

  const [brand, setBrand] = React.useState([])

  const fetchBrandList = async () => {

    let result = await getData('getBrand')
    console.log(result)

    setBrand(result.data)
  }

  useEffect(() => {
    fetchBrandList()

  }, [])

  const [Model, setModel] = React.useState([])

  const handleChangeBrand = (event) => {
    setBrandId(event.target.value)
    fetchModel(event.target.value)
  }
  const fetchModel = async (BrandId) => {
    let body = { 'brand_id': BrandId }
    var list = await postData('getModelByBrand', body)
    console.log(list)
    setModel(list.data)
  }

  const [Equip, setEquip] = React.useState([])

  const handleChangeModel = (event) => {
    setModelId(event.target.value)
    fetchEquip(event.target.value)
  }
  const fetchEquip = async (ModelId) => {
    let body = { 'model_id': ModelId }
    var list = await postData('getEquipmentByBrandModel', body)
    console.log(list)
    setEquip(list.data)
  }

  const BaseURL = "http://83.136.219.147/trafiksol/public/api/v1"

  const postData1 = async (url, body) => {
    var response = await fetch(`${BaseURL}/${url}`, {
      method: "POST",
      mode: "cors",
      headers: {
        // "Authorization":"Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOlwvXC9hYXJvZ3lhaW5kaWEuaW5cL3RyYWZpa3NvbFwvcHVibGljXC9hcGlcL3YxXC9sb2dpbiIsImlhdCI6MTU5OTEyMzA1MiwiZXhwIjoxNjAxNzE1MDUyLCJuYmYiOjE1OTkxMjMwNTIsImp0aSI6IlJQVmttMHhmcFRzZHpHaGoiLCJzdWIiOjEsInBydiI6Ijg3ZTBhZjFlZjlmZDE1ODEyZmRlYzk3MTUzYTE0ZTBiMDQ3NTQ2YWEifQ.DFp2qevRXpL5DKJNSyrryVAnGD0BWXm-GiMVQeNq5LQ",
        "Authorization": "Bearer " + token,
        "Content-Type": "application/json; charset=utf-8"
      },
      body: JSON.stringify(body)
    })
    var result = await response.json()
    return result
  }

  let rec = JSON.parse(localStorage.getItem('LOGIN'))
  console.log(rec)
  let token = rec.data.user.token;


  let siteId = JSON.parse(localStorage.getItem('SITEID'))
  const [quantity, setQuantity] = React.useState('')
  const [ModelId, setModelId] = React.useState('')
  const [ItemName, setItemName] = React.useState('')
  const [ItemCode, setItemCode] = React.useState('')
  const [DocketNo, setDocketNo] = React.useState('')
  const [equip_id, setEquipId] = React.useState('')
  const [getPicture, setPicture] = React.useState('')
  const [getPicturePath, setPicturePath] = React.useState('')
  const createItem = async () => {

    var body = {
      "site_id": siteId,
      "brand_id": BrandId,
      "model_id": ModelId,
      "item_name": ItemName,
      "item_code": ItemCode,
      "docket_no": DocketNo,
      "quantity": quantity,
      "equipment_id": equip_id
    }

    console.log(body)

    let result = await postData1('stores/addStore', body)
    console.log('result', result)

    if (result.status == 1) {
      //alert('Item Created..')
      clearValues()
    }
    else {
      alert('Fail to Create Item...')
    }


  }

  const clearValues = () => {
    setBrandId('')
    setModelId('')
    setItemName('')
    setItemCode('')
    setDocketNo('')
    setQuantity('')
    setEquipId('')
    setPicturePath('')
    setPicture('')
  }

  const handlePicture = (event) => {
    setPicturePath(URL.createObjectURL(event.target.files[0]))
    setPicture(event.target.files[0])
  }



  return (
    <div>
      <Container component="main">
        <CssBaseline />


        <div className={classes.paper}>

          <Paper>
            <Grid container spacing={2}>
              <Grid item sm={2} xs={3} md={2}>
                <Hidden mdDown implementation="css">
                  <Button className={classes.back}
                    onClick={() => handleClick(<StoreItemList changeView={props.changeView} />)}>
                    <ArrowBackIosIcon className={classes.backIcon} />Back</Button>
                </Hidden>
                <Hidden mdUp implementation="css">
                  <button size="small" aria-label="add" className={classes.addRespons}>
                    <ArrowBackIosIcon onClick={() => handleClick(<StoreItemList changeView={props.changeView} />)} /></button>

                </Hidden>
              </Grid>

              <Grid item sm={8} xs={8} md={8}>
                <Typography component="h1" variant="h6" color="inherit" noWrap className={classes.title}>
                  Create Item

                </Typography>
              </Grid>

            </Grid>
            <ColoredLine color="#1e83a9" />
            <div className={classes.new}>
              <form className={classes.form} noValidate>
                <Grid container spacing={2}>

                  <Grid item xs={12} sm={12} md={6}>

                    <Grid container spacing={2}>
                      <Grid item xs={12} sm={4} md={4}>
                        <Typography className={classes.field}>Item Name</Typography>
                      </Grid>
                      <Grid item xs={12} sm={8} md={6}>
                        <Input
                          value={ItemName}
                          onChange={(event) => setItemName(event.target.value)}
                          className={classes.input}
                          placeholder="             Item Name"
                          required
                          fullWidth
                          type="text"
                          inputProps={{ 'aria-label': 'description' }} />
                      </Grid>
                    </Grid>


                    <Grid container spacing={2}>
                      <Grid item xs={12} sm={4} md={4}>
                        <Typography className={classes.field}>Brand</Typography>
                      </Grid>
                      <Grid item xs={12} sm={8} md={6}>
                        <FormControl fullWidth className={classes.formcontrol}>

                          <TextField
                            className={classes.input}
                            id="outlined-size-small"
                            defaultValue="Small"
                            size="small"
                            select
                            value={BrandId}
                            onChange={(event) => handleChangeBrand(event)}
                          // variant="outlined"
                          >
                            {brand.map(option => (
                              <MenuItem key={option.id} value={option.id}>
                                {option.name}
                              </MenuItem>
                            ))}
                          </TextField>
                        </FormControl>
                      </Grid>
                    </Grid>

                    <Grid container spacing={2}>
                      <Grid item xs={12} sm={4} md={4}>
                        <Typography className={classes.field}>Equipment</Typography>
                      </Grid>
                      <Grid item xs={12} sm={8} md={6}>
                        <FormControl fullWidth className={classes.formcontrol}>

                          <TextField
                            className={classes.input}
                            id="outlined-size-small"
                            defaultValue="Small"
                            size="small"
                            select
                            value={equip_id}
                            onChange={(event) => setEquipId(event.target.value)}
                          // variant="outlined"
                          >
                            {Equip.map(option => (
                              <MenuItem key={option.id} value={option.id}>
                                {option.title}
                              </MenuItem>
                            ))}
                          </TextField>
                        </FormControl>
                      </Grid>
                    </Grid>

                    <Grid container spacing={2}>
                      <Grid item xs={12} sm={4} md={4}>
                        <Typography className={classes.field}>Remarks</Typography>
                      </Grid>
                      <Grid item xs={12} sm={8} md={6}>
                        <Input
                          value={DocketNo}
                          onChange={(event) => setDocketNo(event.target.value)}
                          className={classes.input}
                          placeholder="                 Remarks"
                          required
                          fullWidth
                          type="text"
                          inputProps={{ 'aria-label': 'description' }} />
                      </Grid>
                    </Grid>



                  </Grid>



                  <Grid item xs={12} sm={12} md={6}>

                    <Grid container spacing={2}>
                      <Grid item xs={12} sm={4} md={4}>
                        <Typography className={classes.field}>Item Code</Typography>
                      </Grid>
                      <Grid item xs={12} sm={8} md={6}>

                        <Input
                          value={ItemCode}
                          onChange={(event) => setItemCode(event.target.value)}
                          className={classes.input}
                          placeholder="             Item Code"
                          required
                          fullWidth
                          type="text"
                          inputProps={{ 'aria-label': 'description' }} />
                      </Grid>
                    </Grid>

                    <Grid container spacing={2}>
                      <Grid item xs={12} sm={4} md={4}>
                        <Typography className={classes.field}>Model</Typography>
                      </Grid>
                      <Grid item xs={12} sm={8} md={6}>
                        <FormControl fullWidth className={classes.formcontrol}>

                          <TextField
                            className={classes.input}
                            id="outlined-size-small"
                            defaultValue="Small"
                            size="small"
                            select
                            value={ModelId}
                            //onChange={(event)=>setModelId(event.target.value)}
                            onChange={(event) => handleChangeModel(event)}
                          // variant="outlined"
                          >
                            {Model.map(option => (
                              <MenuItem key={option.id} value={option.id}>
                                {option.model}
                              </MenuItem>
                            ))}
                          </TextField>
                        </FormControl>
                      </Grid>
                    </Grid>


                    <Grid container spacing={2}>
                      <Grid item xs={12} sm={4} md={4}>
                        <Typography className={classes.field}>Quantity</Typography>
                      </Grid>
                      <Grid item xs={12} sm={8} md={6}>
                        <Input
                          value={quantity}
                          onChange={(event) => setQuantity(event.target.value = Math.max(0, parseInt(event.target.value)).toString().slice(0, 10))}
                          className={classes.input}
                          placeholder="              Quantity"
                          required
                          fullWidth
                          type="number"
                          inputProps={{ 'aria-label': 'description' }} />
                      </Grid>
                    </Grid>


                  </Grid>


                </Grid>


                <Grid item xs={12} sm={12}>
                  <Grid container spacing={4}>
                    <Grid item xs={12} sm={4} md={2}>
                      <Typography className={classes.field}>Image</Typography>
                    </Grid>
                    <Grid item xs={12} sm={5} md={7}>
                      <Input
                        value={getPicturePath}
                        // onChange={(event)=>handlePicture(event)}
                        disabled
                        id="standard-disabled"
                        className={classes.input}
                        placeholder="Image"
                        required
                        fullWidth
                        type="text"
                        inputProps={{ 'aria-label': 'description' }} />
                    </Grid>
                    <Grid item xs={12} sm={1} md={2}>
                      <input
                        required
                        accept="image/*"
                        className={classes.image}
                        id="Picture"
                        multiple
                        type="file"
                        onChange={(event) => handlePicture(event)}
                        fullWidth
                      />
                      <label htmlFor="Picture">
                        <Button variant="contained" component="span" className={classes.upload} >
                          Upload
                        </Button>
                      </label>
                    </Grid>


                    {/* <Grid item xs={12} sm={6} md={4}>
    { <Avatar variant="square" alt="Picture" src={getPicturePath} className={classes.bigAvatar} value={getPicturePath} style={{width:120,height:100}}/> }
      </Grid> */}
                  </Grid>
                </Grid>

                <Button
                  onClick={createItem}
                  variant="contained"
                  color="green"
                  className={classes.submit}
                >
                  Add Item
                </Button>



              </form>
            </div>



          </Paper>
        </div>

      </Container>
    </div>
  )
}