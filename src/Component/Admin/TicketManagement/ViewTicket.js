import React, { useState, useEffect, useRef } from 'react';
import Avatar from '@material-ui/core/Avatar';
import Button from '@material-ui/core/Button';
import CssBaseline from '@material-ui/core/CssBaseline';

import LockOutlinedIcon from '@material-ui/icons/LockOutlined';
import {Typography,Container,Fade,Grid,Modal,Backdrop,Paper
,Input,InputBase,Box,Divider,TextField,InputAdornment  }
 from '@material-ui/core';
 
 
 import Fab from '@material-ui/core/Fab';
 import AccountCircle from '@material-ui/icons/AccountCircle';
 import rr from "./rr.jpg"
import Tooltip from '@material-ui/core/Tooltip';
import ClickAwayListener from '@material-ui/core/ClickAwayListener';
import PauseIcon from '@material-ui/icons/Pause';
import LoopIcon from '@material-ui/icons/Loop';
import BackupIcon from '@material-ui/icons/Backup';
import AddCommentIcon from '@material-ui/icons/AddComment';
import Card from '@material-ui/core/Card';
import CardActionArea from '@material-ui/core/CardActionArea';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';

import { makeStyles } from '@material-ui/core/styles';
import {postDataAndImage} from '../../FetchServices';

const ColoredLine = ({ color }) => (
  <hr
      style={{
          color: color,
          backgroundColor: color,
          height: 3
      }}
  />
);


const useStyles = makeStyles((theme) => ({
    grid:{
    //  overflowY:'scroll',
    //  overflowX:'hidden',
    //  height: '220px',
    },
    field:{
       textAlign:'left'    
    },
  paper: {
    marginTop: theme.spacing(4),
    display: 'flex',
    flexDirection: 'column',
    width:'100%',
    //marginLeft:'10%'
   // alignItems: 'center',
  },
  title: {
    flexGrow: 1,
  ///  textAlign:'center',
    //marginRight:'5%',
   // marginTop:'2%',
    //backgroundColor:'#1ab558d4',
    color:'#1e83a9',
  },
  title1: {
    flexGrow: 1,
   // textAlign:'center',
    padding:'10px',
    //backgroundColor:'#1ab558d4',
    color:'white',
  },
 avatar: {
    width: 40,
    height: 40,
    marginLeft:'5%'
  },
  form: {
    width: '100%', // Fix IE 11 issue.
    marginTop: theme.spacing(3),
    marginBottom: theme.spacing(8),
    // overflowY: 'scroll',
    // overflowX: 'hidden',
    // height: '220px',
  },
  form2: {
    width: '100%', // Fix IE 11 issue.
    marginTop: theme.spacing(4),
    marginBottom: theme.spacing(4),
   // overflow:'scroll',
    //height: '180px',
  },
  upload:{
    display:'none',
  },
  hideInput: {
    display: 'none',
  },
  commentPic:{
    backgroundColor:'#1e83a9',
    color:'#fff',
    borderRadius:'0px',
    boxShadow:'0px',
    height:'33px',
    marginTop:'8%',
  
    '&:hover': {
      // color:'primary',
       fontStyle:"bold",
       backgroundColor:'#1e83a9',
       },
  },
  
  submit: {
    marginTop: '2%',
    marginBottom: '2%',
    backgroundColor:'#16a980d4',
    color:'#fff',
    //width:'40%'
    float:'right',
    marginRight:'8.2%',
    
    '&:hover': {
  backgroundColor:'#16a980d4',
  fontStyle:"bold" 
  },
  },
  submit1: {
    marginTop: '4%',
    marginBottom: '2%',
    backgroundColor:'#1e83a9',
    color:'#fff',
    '&:hover': {
  backgroundColor:'#1e83a9',
  fontStyle:"bold"
  },
  },
  request: {
    marginTop: '2%',
    marginBottom: '2%',
    backgroundColor:'#1e83a9',
    color:'#fff',
    //width:'40%'
    //float:'left',
    //marginRight:'8.2%',
    
    '&:hover': {
  backgroundColor:'#1e83a9',
  fontStyle:"bold" 
  },
  },
  
  comment: {
    flexGrow: 1,
    AlignItems:'left',
    padding:'10px'
  },
  formControl: {
    marginBottom: theme.spacing(3),
   /// minWidth: 400,
    backgroundColor:'#aaaaaa2e',
    width:'100%',
    
  },
  descrip:{
    marginBottom: theme.spacing(3),
    backgroundColor:'#aaaaaa2e',
    textAlign:'left'
   // height:'100px'
},
   input:{
    marginBottom: theme.spacing(3),
    backgroundColor:'#aaaaaa2e',
    textAlign:'left'
},
file:{
    backgroundColor:'#aaaaaa2e',
    borderColor:'none',
    width:'100%'
},
new:{
  marginLeft: theme.spacing(2),
  marginRight: theme.spacing(2),
},

modal: {
  display: 'flex',
  alignItems: 'center',
  justifyContent: 'center',
},
content: {
  backgroundColor: theme.palette.background.paper,
  border: '2px solid #000',
  boxShadow: theme.shadows[5],
  padding: theme.spacing(2, 4, 3),
},
head:{
textAlign:'center',
marginBottom:'3%',
color:'#1e83a9'
},
btn:{
  marginBottom:'3%'
  },

  timer:{
    flexGrow: 1,
  ///  textAlign:'center',
    //marginRight:'5%',
   // marginTop:'2%',
    //backgroundColor:'#1ab558d4',
    color:'white',
  },
  countdown:{
    //:'#1e83a9',
  }
}));

const ViewTicket = (props) =>  {
  const classes = useStyles();
  const [open, setOpen] = React.useState(false);
  const [onn, setOnn] = React.useState(false);
  const [modal, setModal] = React.useState(false);

  const [getTicketId,setTicketId]=React.useState('')
  const [getcomment,setComment]=React.useState('')
  const [getSLAReason,setSLAReason]=React.useState('')

  /*******Hardware  Request****** */

  const [getEquip,setEquip]=React.useState('')
  const [getQuantity,setQuantity]=React.useState('')
  const [getHardwareReason,setHardwareReason]=React.useState('')
  const [getRefNo,setRefNo]=React.useState('')
  const [ getPicture, setPicture ] = React.useState('');
  const [ getPicturePath, setPicturePath ] = React.useState('');


  const BaseURL="http://83.136.219.147/trafiksol/public/api/v1"

  const postData1=async(url,body)=>{
      var response=await fetch(`${BaseURL}/${url}`,{
      method:"POST",
      mode:"cors",
      headers:{
       // "Authorization":"Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOlwvXC9hYXJvZ3lhaW5kaWEuaW5cL3RyYWZpa3NvbFwvcHVibGljXC9hcGlcL3YxXC9sb2dpbiIsImlhdCI6MTU5OTEyMzA1MiwiZXhwIjoxNjAxNzE1MDUyLCJuYmYiOjE1OTkxMjMwNTIsImp0aSI6IlJQVmttMHhmcFRzZHpHaGoiLCJzdWIiOjEsInBydiI6Ijg3ZTBhZjFlZjlmZDE1ODEyZmRlYzk3MTUzYTE0ZTBiMDQ3NTQ2YWEifQ.DFp2qevRXpL5DKJNSyrryVAnGD0BWXm-GiMVQeNq5LQ",
       "Authorization": "Bearer " + token,
       "Content-Type": "application/json; charset=utf-8"},
       body:JSON.stringify(body)})
       var result=await response.json()
    return result
  }
  
    let rec=JSON.parse(localStorage.getItem('LOGIN'))
     console.log(rec)
     let token=rec.data.user.token;
  
  
    let siteId=JSON.parse(localStorage.getItem('SITEID'))

     const [SLATime,setSLATime]=React.useState('')
 
  const [Ticketlist,setTicketlist]=React.useState([])
  const [Commentlist,setCommentlist]=React.useState([])

    const ViewTicket=async(id)=>{
    
      var body={
        "ticket_id":id,
        "site_id":siteId
         
       }
       let result= await postData1('tickets/getTicketWithComment',body)
       console.log('result',result)
       
       if(result.status==1){
        setTicketlist(result.data.ticket)
        setCommentlist(result.data.comments)
       let RemainHrs = result.data.ticket.map(item=>(
                  item.remaining_hours
        ))
        console.log(RemainHrs)
        if(RemainHrs!=''){
          setSLATime(RemainHrs)
        }else{setSLATime('00')}
        

        }
   
   
     }

     const [getImage,setImage] = React.useState('')
  const [getImagePath,setImagePath] = React.useState('')

  const handleImage=(event)=>{
    if(event.target.files && event.target.files[0]) {
      setImagePath(URL.createObjectURL(event.target.files[0]))
      setImage(event.target.files[0].name)
    }
   } 
   
   const handlePicture=(event)=>{
    if(event.target.files && event.target.files[0]) {
      setPicturePath(event.target.files[0])
      setPicture(event.target.files[0].name)
    }
   } 

  const handleComment=async()=>{

    if (getTicketId!='' && getcomment!='' && getImage!=''){

    const formData=new FormData()
    formData.append('ticket_id',getTicketId)
    formData.append('comment',getcomment)
    formData.append('file[]',getImage)

    const config={headers:{
       'Authorization' : "Bearer " + token,
      'content-type':'multipart/form-data'}}
    const result=await postDataAndImage('tickets/addComment',formData,config)

    console.log(formData)
    console.log(result)
    clearComment()
    ViewTicket(props.id)
    }else{
      alert('All field are Required')
    }
      }
     
     const clearComment=()=>{
      setComment('')
     }

     const [closeComment,setCloseComment] = React.useState('')
     const closeTicket=async()=>{
      var body={
        "ticket_id":getTicketId,
        "site_id":siteId,
        "description":closeComment
         
       }
   console.log(body)
       let result= await postData1('tickets/ticketCloseRequest',body)
       console.log('closeComment',result)
       if(result.status==1){
         alert('You have requested for Close the Ticket')
         removeDescrip()
         setModal(false)
       }
     }

     const removeDescrip=()=>{
      setCloseComment('')
    }

   useEffect(function(){
    try{
      ViewTicket(props.id)
        //setSla(props.sla_end)
        setTicketId(props.id)
    }
    catch(e){}

},[])


  const handleOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };

  const handleOnn = () => {
    setOnn(true);
  };

  const handleOff = () => {
    setOnn(false);
  };

  const ModalClose = () => {
    setModal(false);
  };

  const ModalOpen = () => {
    setModal(true);
  };

  const [up, setUp] = React.useState(false);

  const handleTooltipClose = () => {
    setUp(false);
  };

  const handleTooltipOpen = () => {
    setUp(true);
  };


  const SLAReasonPause = async() => {
   
    var body = {
      'ticket_id' : getTicketId,
      'reason' : getSLAReason
    }
    console.log(body)

    let result = await postData1('tickets/ticketPauseRequest',body)
    console.log(result)

    if(result){
      clearSLAPause()
      setOnn(false);
    }else{
      alert('not')
    }};

    const clearSLAPause=()=>{
      setSLAReason('')
    }

  const hardwareRequest = async() => {
  
    let formData = new FormData();
    formData.append('ticket_id', getTicketId);
    // formData.append('equipment_required', getEquip);
    formData.append('quantity', getQuantity);
    formData.append('reason', getHardwareReason);
    formData.append('ref_no', getRefNo);
    // formData.append('file[]', getPicturePath);
    formData.append('hardware_image', getPicturePath);
    console.log(...formData)
    const config={headers:{
      'Authorization' : "Bearer " + token,
     'content-type':'multipart/form-data'}}
   const result=await postDataAndImage('tickets/hardwareRequest',formData,config)
   console.log(result)
    if(result && result.data.status === 1){
      clearHardwareReq()
      setOpen(false);
    }else{
      alert('something went wrong')
    }};

    const clearHardwareReq=()=>{
      setEquip('')
      setQuantity('')
      setHardwareReason('')
      setRefNo('')
      setPicture('')
    }


  const [timerDays, setTimerDays] = useState('00');
  const [timerHours, setTimerHours] = useState('00');
  const [timerMinutes, setTimerMinutes] = useState('00');
  const [timerSeconds, setTimerSeconds] = useState('00');

  let interval = useRef();

  const startTimer = () => {
    // const countdownDate = new Date(SLATime).getTime();
    const countdownDate = new Date(SLATime).getTime();

    interval = setInterval(() => {
        const now = new Date().getTime();
        const distance = countdownDate - now;
        
        const days = Math.floor(distance / (1000 * 60 * 60 * 24));
        const hours = Math.floor((distance % (1000 * 60 * 60 * 24) / (1000 * 60 * 60)));
        const minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
        const seconds = Math.floor((distance % (1000 * 60 )) / 1000);

        if (distance < 0){
          //stop timer
          clearInterval(interval.current);
        }else{
          //update timer
          setTimerDays(days);
           setTimerHours(hours);
           setTimerMinutes(minutes);
           setTimerSeconds(seconds);

        }

    }, 1000);
  };

  //componentDidMount
  useEffect(() => {
    startTimer();
    return () => {
      clearInterval(interval.current);
    }

  })



  return (
    console.log(Commentlist),
   console.log("rrrrrrrrrrrrrrrrrrrrrrrrrrrr", Ticketlist),
    <Container component="main">
      <CssBaseline />
      
      <div className={classes.paper}>

      <Paper>
      
       <div className={classes.new}>

         <Grid container spacing={3} className={classes.countdown}>
            <Grid item xs={12} sm={4} md={4}>

            <Typography variant="h6" className={classes.title}>Time Left for SLA</Typography>
            {/* <Typography variant="h12" className={classes.title}>{timerDays}d: {timerHours}h: {timerMinutes}m: {timerSeconds}s</Typography>
               */}
            <Typography variant="h12" className={classes.title}> {SLATime}</Typography>
              </Grid>
              <Grid item xs={12} sm={4} md={4}>
              <Typography variant="h6" color="inherit" noWrap className={classes.title}>
            Services Ticket
            <Typography component="h6" variant="h12" color="inherit" noWrap className={classes.title}>
            Issue Details
          </Typography>
          </Typography>
              </Grid>

              <Grid item xs={12} sm={4} md={4}>
              <Button onClick={ModalOpen} variant="contained" color="primary" size="small" style={{marginTop:'2%'}}> 
             Close</Button>
              </Grid>

              </Grid>

              <ColoredLine color="#1e83a9" />
          
              {Ticketlist.slice(0,1).map(option=>(
              

      
        <form className={classes.form} noValidate>
          <Grid container spacing={2}>

            <Grid item xs={12} sm={12} md={6}>

            <Grid container spacing={2}>
            <Grid item xs={12} sm={6} md={4}>
              <Typography className={classes.field}>Ticket No.</Typography>
             
              </Grid>
              <Grid item xs={12} sm={6} md={6}>
             
              <Typography className={classes.input}>{option.id}</Typography>
              </Grid>
              </Grid>
            
              <Grid container spacing={2}>
            <Grid item xs={12} sm={6} md={4}>
              <Typography className={classes.field}>Ticket Category</Typography>
              </Grid>
              <Grid item xs={12} sm={6} md={6}>
             
              <Typography className={classes.input}>{option.ticket_category_name}</Typography>
              </Grid>
              </Grid>

           <Grid container spacing={2}>
            <Grid item xs={12} sm={6} md={4}>
              <Typography className={classes.field}>Site Name</Typography>
              </Grid>
              <Grid item xs={12} sm={6} md={6}>
              <Typography className={classes.input}>{option.site_name}</Typography>
              </Grid>
              </Grid>

              <Grid container spacing={2}>
            <Grid item xs={12} sm={6} md={4}>
              <Typography className={classes.field}>Equipment Name</Typography>
              </Grid>
              <Grid item xs={12} sm={6} md={6}>
              <Typography className={classes.input}>{option.equipment_title}</Typography>
              </Grid>
              </Grid>

              <Grid container spacing={2}>
            <Grid item xs={12} sm={6} md={4}>
              <Typography className={classes.field}>Priority</Typography>
              </Grid>
              <Grid item xs={12} sm={6} md={6}>
              <Typography className={classes.input}>{option.priority}</Typography>
              </Grid>
              </Grid>

               <Grid container spacing={2}>
            <Grid item xs={12} sm={6} md={4}>
              <Typography className={classes.field}>Assigned To</Typography>
              </Grid>
              <Grid item xs={12} sm={6} md={6}>
              <Typography className={classes.input}>{option.assig_user}</Typography>
              </Grid>
              </Grid>

             

            </Grid>

            


           <Grid item xs={12} sm={12} md={6}>

           <Grid container spacing={2}>
            <Grid item xs={12} sm={6} md={4}>
              <Typography className={classes.field}>Ticket Subject</Typography>
              </Grid>
              <Grid item xs={12} sm={6} md={6}>
              <Typography className={classes.input}>{option.subject}</Typography>
              </Grid>
              </Grid>
            
              <Grid container spacing={2}>
            <Grid item xs={12} sm={6} md={4}>
              <Typography className={classes.field}>Stretch Point</Typography>
              </Grid>
              <Grid item xs={12} sm={6} md={6}>
              <Typography className={classes.input}>{option.stretch_point}</Typography>
              </Grid>
              </Grid>

              <Grid container spacing={2}>
            <Grid item xs={12} sm={6} md={4}>
              <Typography className={classes.field}>Issue Type</Typography>
              </Grid>
              <Grid item xs={12} sm={6} md={6}>
              <Typography className={classes.input}>{option.issue_type_name}</Typography>
              </Grid>
              </Grid>
              
    

          
        <Grid container spacing={2}>
            <Grid item xs={12} sm={6} md={4}>
              <Typography className={classes.field}>Created By</Typography>
              </Grid>
              <Grid item xs={12} sm={6} md={6}>
              <Typography className={classes.input}>{option.username}</Typography>
              </Grid>
              </Grid>

              <Grid container spacing={2}>
            <Grid item xs={12} sm={6} md={4}>
              <Typography className={classes.field}>Created At</Typography>
              </Grid>
              <Grid item xs={12} sm={6} md={6}>
              <Typography className={classes.input}>{option.created_at}</Typography>
              </Grid>
              </Grid>



            </Grid>


            
          </Grid>

         

          <Grid container spacing={2}>
          <Grid item xs={12} sm={6} md={2}>
          <Typography className={classes.field}>Problem</Typography>
          </Grid>
          <Grid item xs={12} sm={6} md={9}>
          <Typography className={classes.descrip}>{option.description}</Typography>
          </Grid>
        </Grid> 
        <Grid container spacing={2}>
          <Grid item xs={12} sm={6} md={2}>
          <Typography className={classes.field}>Image</Typography>
          </Grid>
          <Grid item xs={6} sm={4} md={2}>
            <Card><img src={option.t_image} alt="image" style={{height:100}}/></Card>
          </Grid>
        </Grid> 

       
 

          {/* <Button
          
              type="submit"
              
              variant="contained"
              color="green"
              className={classes.submit}
            >
              Reopen
            </Button> */}
         
        </form>
  ) )}
        <ColoredLine color="#1e83a9" />


<form className={classes.form2}  style={{ padding: "20px 20px" }} noValidate>
 <Grid container spacing={3}>

 <Grid item xs={12} sm={6} md={2}>
      <Typography variant="h5">Requests</Typography>
     </Grid>

     
   <Grid item xs={12} sm={12} md={4}>

    
   <Grid container spacing={2}>
   <Grid item xs={12} sm={6} md={4}>
   <Tooltip disableFocusListener title="Hardware Replacement Request">
     <Button onClick={handleOpen}
     variant="contained"
     color="primary"
     size="small"
    //className={classes.submit}
     >
     <LoopIcon/>
   </Button>
   </Tooltip>

<Modal
aria-labelledby="transition-modal-title"
aria-describedby="transition-modal-description"
className={classes.modal}
open={open}
onClose={handleClose}
closeAfterTransition
BackdropComponent={Backdrop}
BackdropProps={{
 timeout: 500,
}}
>
<Fade in={open}>
 <div className={classes.content}>
  {/*---------Modal Content Start-------*/}

   <div className={classes.new}>
   <Typography component="h1" variant="h6" color="inherit" noWrap className={classes.head}>
   Request for Hardware Replacement
 </Typography>

 <ColoredLine color="#1e83a9" />

<form className={classes.form2} noValidate>
 
 <Grid container>

   <Grid item xs={12} sm={12}>

   <Grid container spacing={2}>
   <input value={getTicketId} onChange={(event)=>setTicketId(event.target.value)} style={{display:'none'}}  type="text" />
  
   {/* <Grid item xs={12} sm={4}>
     <Typography>Ticket Id</Typography>
     </Grid>
     <Grid item xs={12} sm={6}>
     <Input
     value={getTicketId}
     onChange={(event)=>setTicketId(event.target.value)}
     className={classes.input}
     placeholder="Ticket Id" 
   required
   fullWidth 
   name="id"
   type="number"
   inputProps={{ 'aria-label': 'description' }} />
     </Grid> */}
     </Grid>
     <Grid container spacing={2}>
   <Grid item xs={12} sm={4} md={4}>
     <Typography>Equipment Required</Typography>
     </Grid>
     <Grid item xs={12} sm={8} md={8}>
    
     <Input
     value={getEquip}
     onChange={(event)=>setEquip(event.target.value)}
   className={classes.input}
   placeholder="Equipment Required" 
   required
   fullWidth 
   name="equipment"
   type="text"
   inputProps={{ 'aria-label': 'description' }} />
     </Grid>
     </Grid>

  <Grid container spacing={2}>
   <Grid item xs={12} sm={4} md={4}>
     <Typography>Quantity</Typography>
     </Grid>
     <Grid item xs={12} sm={8} md={8}>
     <Input
     value={getQuantity}
     onChange={(event)=>setQuantity(event.target.value=Math.max(0, parseInt(event.target.value) ).toString().slice(0,10))}
   className={classes.input}
   placeholder="Quantity" 
   required
   fullWidth 
   name="qty"
   type="number"
   inputProps={{ 'aria-label': 'description' }} />
     </Grid>
     </Grid>

     <Grid container spacing={2}>
   <Grid item xs={12} sm={4} md={4}>
     <Typography>Reason</Typography>
     </Grid>
     <Grid item xs={12} sm={8} md={8}>
     <Input
     value={getHardwareReason}
     onChange={(event)=>setHardwareReason(event.target.value.substring(0,100))}
   className={classes.input}
   placeholder="Reason" 
   required
   fullWidth 
   name="reason"
   type="text"
   inputProps={{ 'aria-label': 'description' }} />
     </Grid>
     </Grid>

 <Grid container spacing={2}>
   <Grid item xs={12} sm={4} md={4}>
     <Typography>Model No</Typography>
     </Grid>
     <Grid item xs={12} sm={8} md={8}>
     <Input
     value={getRefNo}
     onChange={(event)=>setRefNo(event.target.value.substring(0,30))}
   className={classes.input}
   placeholder="Model No" 
   required
   fullWidth 
   name="Model_No"
   type="text"
   inputProps={{ 'aria-label': 'description' }} />
     </Grid>
     </Grid>
     <Grid container spacing={2}>
   <Grid item xs={12} sm={4} md={4}>
     <Typography>Product Code</Typography>
     </Grid>
     <Grid item xs={12} sm={8} md={8}>
     <Input
     value={getRefNo}
     onChange={(event)=>setRefNo(event.target.value.substring(0,30))}
   className={classes.input}
   placeholder="Product Code" 
   required
   fullWidth 
   name="Product Code"
   type="text"
   inputProps={{ 'aria-label': 'description' }} />
     </Grid>
     </Grid>


<Grid container spacing={3}>
 <Grid item xs={12} sm={4} md={4}>
 <Typography>Upload Images/Docs</Typography>
 </Grid>
 <Grid item xs={12} sm={5} md={5}>
    <Input
         value={getPicture}
       // onChange={(event)=>handlePicture(event)}
       disabled 
       id="standard-disabled"
            style={{marginTop:'4%',marginBottom:'3%'}}
            placeholder="Image" 
            required
            fullWidth 
            type="text"
            inputProps={{ 'aria-label': 'description' }} />
       </Grid>
    <Grid item xs={6} sm={1} md={2}>
    <input
        required
        accept="image/*"
        className={classes.hideInput}
        id="Picture"
        multiple
        type="file"
        onChange={(event)=>handlePicture(event)}
        fullWidth
      />
      <label htmlFor="Picture">
        <Button variant="contained" component="span" className={classes.commentPic} >
         Upload
        </Button>
      </label>
    </Grid>
 
</Grid>

 <Button variant="contained" onClick={hardwareRequest}
 className={classes.submit1}>
 Save
</Button>
   
 </Grid>


</Grid>


</form>
</div>

  {/*----------Modal Content End---------*/}
 </div>
</Fade>
</Modal>
     </Grid>
     <Grid item xs={12} sm={6} md={4}>

     <Tooltip disableFocusListener title="SLA Pause Request">
     <Button onClick={handleOnn}
     variant="contained"
     color="primary"
     size="small"
   //  className={classes.submit}
     >
    <PauseIcon/>
   </Button>
  </Tooltip>

<Modal
aria-labelledby="transition-modal-title"
aria-describedby="transition-modal-description"
className={classes.modal}
open={onn}
onClose={handleOff}
closeAfterTransition
BackdropComponent={Backdrop}
BackdropProps={{
 timeout: 500
}}
>
<Fade in={onn}>
 <div className={classes.content}>
 <form>
  <Box p={2}>
  <Typography style={{color:'#1e83a9'}}>SLA pause reason</Typography>
  <ColoredLine color="#1e83a9" />
  
   </Box>
   <Box p={2}>
   <input value={getTicketId} onChange={(event)=>setTicketId(event.target.value)} style={{display:'none'}}  type="text" />
  
   <Input
   className={classes.input}
   placeholder="Reason" 
   required
   fullWidth 
   value={getSLAReason}
   onChange={(event)=>setSLAReason(event.target.value)}
   name="SLA_reason"
   type="text"
   inputProps={{ 'aria-label': 'description' }} />
   </Box>
   <Box p={2}>
   <Button  variant="contained" onClick={SLAReasonPause}
     className={classes.request}>
   Save
 </Button></Box>
</form>
 </div>
</Fade>
</Modal>
     </Grid>


     </Grid>

    

   </Grid>

   

   
 </Grid>

</form>


         <ColoredLine color="#1e83a9" />

         <Typography variant="h5" className={classes.comment}>Comments</Typography>
           
           <Grid className={classes.grid}>

         <Paper style={{ padding: "40px 20px" }}>

         {Commentlist.map(item=>(
        <Grid container wrap="nowrap" spacing={2}>
         
          <Grid justifyContent="left" item xs zeroMinWidth>
         <h4 style={{ margin: 0, textAlign: "left" }}>{item.name}</h4>
            <p style={{ textAlign: "left" }}>
              {item.comment}{" "}
            </p>
            <p style={{ textAlign: "left", color: "gray" }}>
            {item.created_at}
            </p>
            <p style={{ textAlign: "left", color: "gray" }}>
            
            <Card><img src={item.image} alt="image" style={{height:100}}/></Card>
            </p>
          </Grid>
        </Grid>
         ) )}
         
     
        <Divider variant="fullWidth" style={{ margin: "30px 0" }} />

      </Paper>

      
      </Grid>
      

    <form className={classes.form2}  style={{ padding: "20px 20px" }} noValidate>
          

       <Grid container>

          <Grid item xs={12} sm={12} md={12}>
          
            <input value={getTicketId} onChange={(event)=>setTicketId(event.target.value)} style={{display:'none'}} type="text" />
          <TextField
          value={getcomment}
          onChange={(event)=>setComment(event.target.value.substring(0,200))}
          id="standard-full-width"
          style={{ margin: 8 }}
          placeholder="Write a Comment"
          fullWidth
          margin="normal"
          InputLabelProps={{
            //shrink: true,
          }}
        />
          </Grid>
                   
               </Grid>

               <Grid container spacing={3}>
               
    
    <Grid item xs={12} sm={9} md={8}>
    <Input
         value={getImagePath}
       // onChange={(event)=>handlePicture(event)}
       disabled 
       id="standard-disabled"
            style={{marginTop:'4%',marginBottom:'3%'}}
            placeholder="Image" 
            required
            fullWidth 
            type="text"
            inputProps={{ 'aria-label': 'description' }} />
       </Grid>
    <Grid item xs={6} sm={1} md={4}>
    <input
        required
        accept="image/*"
        className={classes.hideInput}
        id="Image"
        multiple
        type="file"
        onChange={(event)=>handleImage(event)}
        fullWidth
      />
      <label htmlFor="Image">
        <Button variant="contained" component="span" color="primary" style={{marginBottom:'10%'}} className={classes.commentPic} >
         Upload
        </Button>
      </label>
    </Grid>

</Grid>

     <Grid container>

  <Grid item xs={7} sm={7} md={3}>
  <Button onClick={handleComment} variant="contained" color="primary" size="small"
//className={classes.submit}
> Save
</Button>
 </Grid>
     </Grid>

      </form>               

    

        <Grid container spacing={2}>

      <Grid item>
          <ClickAwayListener onClickAway={handleTooltipClose}>
            <div>
              <Tooltip
                PopperProps={{
                  disablePortal: true,
                }}
                onClose={handleTooltipClose}
                open={open}
                disableFocusListener
                title="Add"
              >
                <Button onClick={handleTooltipOpen}></Button>
              </Tooltip>
            </div>
          </ClickAwayListener>
        </Grid>
        </Grid>


     {/* -------- Modal for Close Ticket Request -------*/}

     <Modal
aria-labelledby="transition-modal-title"
aria-describedby="transition-modal-description"
className={classes.modal}
open={modal}
onClose={ModalClose}
closeAfterTransition
BackdropComponent={Backdrop}
BackdropProps={{
 timeout: 500
}}
>
<Fade in={modal}>
 <div className={classes.content}>
 <form>
  <Box p={2}>
  <Typography style={{color:'#1e83a9'}}>Close Ticket</Typography>
  <ColoredLine color="#1e83a9" />
  
   </Box>
   <Box p={2}>
   <input value={getTicketId} onChange={(event)=>setTicketId(event.target.value)} style={{display:'none'}} type="text" />

   <Input
   className={classes.input}
   placeholder="Description" 
   required
   fullWidth 
   value={closeComment}
   onChange={(event)=>setCloseComment(event.target.value.substring(0,200))}
   type="text"
   inputProps={{ 'aria-label': 'description' }} />
   </Box>
   <Box p={2}>
   <Button onClick={closeTicket} variant="contained" color="primary" size="small"> 
             Close</Button></Box>
</form>
 </div>
</Fade>
</Modal>


     {/* {End of Modal for Close Ticket} */}

        </div>

        </Paper>

        
      </div>
      
    </Container>
  );
}

export default ViewTicket;