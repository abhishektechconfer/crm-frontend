import React, { useEffect } from 'react';
import { fade, makeStyles } from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import {
  TablePagination, Box, Link, TableRow, Button, Grid, Typography, Divider,
  Select, MenuItem, Tooltip, Hidden
} from '@material-ui/core';
import AddIcon from '@material-ui/icons/Add';
import CreateItem from './CreateItem';
import ReturnItem from './ReturnItem';
import HardwareRequest from './HardwareRequest';
import AssignmentLateOutlinedIcon from '@material-ui/icons/AssignmentLateOutlined';
import ArrowBackIosIcon from '@material-ui/icons/ArrowBackIos';

const ColoredLine = ({ color }) => (
  <hr
    style={{
      color: color,
      backgroundColor: color,
      height: 5
    }}
  />
);



const useStyles = makeStyles((theme) => ({
  root: {
    width: '100%',
  },
  title: {
    flexGrow: 1,
    textAlign: 'center',
    /// marginRight:'5%',
    padding: '10px',
    //backgroundColor:'#1e83a9',
    color: '#1e83a9',
  },
  AddIcon: {
    marginRight: theme.spacing(1),
  },
  container: {
    maxHeight: 440,
    overflowX: 'auto',
  },
  submit: {
    // marginLeft: theme.spacing(115),
    backgroundColor: '#ff6a00',
    color: '#fff',
    width: '10%',
    float: 'right',

    '&:hover': {
      backgroundColor: '#ff6a00',
      fontStyle: "bold"
    },
  },
  rowdata: {
    color: '#1e83a9',
    minWidth: 100,
  },
  staff: {
    backgroundColor: '#ff6a00',
    textAlign: 'center'
  },

  select: {
    marginBottom: theme.spacing(3),
    minWidth: 120,
    backgroundColor: '#aaaaaa2e'
  },
  new1: {
    //margin:theme.spacing(2),
    marginTop: theme.spacing(1)
  },
  text: {
    //paddingTop:'26px',
    paddingBottom: '17px'
  },
  Add: {

    backgroundColor: '#1e83a9',
    color: '#fff',
    marginTop: '10%',

    '&:hover': {
      color: 'primary',
      fontStyle: "bold",
      backgroundColor: '#1e83a9',
    },
  },
  addRespons: {
    backgroundColor: '#1e83a9',
    color: 'white',
    padding: '0px',
    textAlign: 'center',
    fontSize: '12px',
    margin: '4px 2px',
  },
}));

export default function MyTicket(props) {

  const classes = useStyles();


  const columns = [

    { id: 'itemname', label: 'Item Name', minWidth: 70 },
    {
      id: 'itemcode',
      label: 'Item Code',
      minWidth: 70,
      // align: 'right',
      format: (value) => value.toLocaleString('en-US'),
    },
    {
      id: 'brand',
      label: 'Brand',
      minWidth: 70,
      // align: 'right',
      format: (value) => value.toLocaleString('en-US'),
    },
    {
      id: 'model',
      label: 'Model',
      minWidth: 70,
      //align: 'right',
      format: (value) => value.toLocaleString('en-US'),
    },

    {
      id: 'equip_name',
      label: 'Equipment',
      minWidth: 70,
      //  align: 'right',
      format: (value) => value.toLocaleString('en-US'),
    },
    {
      id: 'docket_no',
      label: 'Docket No.',
      minWidth: 70,
      //  align: 'right',
      format: (value) => value.toLocaleString('en-US'),
    },
    {
      id: 'quantity',
      label: 'Quantity',
      minWidth: 70,
      // align: 'right',
      // Cell: (value) => (<button>Edit</button>),
      // format: (value) => value.toFixed(2),
    },
  ];



  const [page, setPage] = React.useState(0);
  const [rowsPerPage, setRowsPerPage] = React.useState(10);

  const handleClick = (view) => {
    props.changeView(view)
  }


  const handleChangePage = (event, newPage) => {
    setPage(newPage);
  };

  const handleChangeRowsPerPage = (event) => {
    setRowsPerPage(+event.target.value);
    setPage(0);
  };

  const [up, setUp] = React.useState(false);

  const handleTooltipClose = () => {
    setUp(false);
  };

  const handleTooltipOpen = () => {
    setUp(true);
  };

  const BaseURL = "http://83.136.219.147/trafiksol/public/api/v1"

  const postData1 = async (url, body) => {
    var response = await fetch(`${BaseURL}/${url}`, {
      method: "POST",
      mode: "cors",
      headers: {
        // "Authorization":"Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOlwvXC9hYXJvZ3lhaW5kaWEuaW5cL3RyYWZpa3NvbFwvcHVibGljXC9hcGlcL3YxXC9sb2dpbiIsImlhdCI6MTU5OTEyMzA1MiwiZXhwIjoxNjAxNzE1MDUyLCJuYmYiOjE1OTkxMjMwNTIsImp0aSI6IlJQVmttMHhmcFRzZHpHaGoiLCJzdWIiOjEsInBydiI6Ijg3ZTBhZjFlZjlmZDE1ODEyZmRlYzk3MTUzYTE0ZTBiMDQ3NTQ2YWEifQ.DFp2qevRXpL5DKJNSyrryVAnGD0BWXm-GiMVQeNq5LQ",
        "Authorization": "Bearer " + token,
        "Content-Type": "application/json; charset=utf-8"
      },
      body: JSON.stringify(body)
    })
    var result = await response.json()
    console.log('result :', result);
    return result
  }

  let rec = JSON.parse(localStorage.getItem('LOGIN'))
  console.log(rec)
  let token = rec.data.user.token;


  let siteId = JSON.parse(localStorage.getItem('SITEID'))

  const [rows, setRows] = React.useState([])
  const [error, setError] = React.useState(false)
  const [table, setTable] = React.useState(false)

  const readAllRecords = async () => {

    var body = {
      "site_id": siteId,
      "type": 1
    }
    console.log(body)

    let result = await postData1('stores/list', body)
    console.log(result)
    if (result.status == 1) {
      setRows(result.data)
      setTable(true)
    } else {
      setError(true)
    }

  }

  useEffect(() => {
    readAllRecords()
  }, [])

  const View = () => {

    return (
      console.log(rows),
      <div>
        <Paper style={{ marginTop: '32px' }}>

          <Grid container spacing={3}>


            <Grid item xs={6} sm={6} md={7}>
              <Typography component="h1" variant="h6" color="inherit" noWrap className={classes.title}>
                Item List
              </Typography>
            </Grid>
            <Grid item xs={3} sm={3} md={1}>


              {/* <Button
size="medium"
color="primary"
aria-label="add"
className={classes.Add}
onClick={()=>handleClick(<HardwareRequest changeView={props.changeView}/>)}> */}
              <Tooltip disableFocusListener title="Assign Hardware">
                {/* <AssignmentLateOutlinedIcon onClick={()=>handleClick(<HardwareRequest changeView={props.changeView}/>)}/> */}
                <img src="./images/manpower.png" onClick={() => handleClick(<HardwareRequest changeView={props.changeView} equipmentData={ rows} />)} />
              </Tooltip>
              {/* </Button> */}

            </Grid>

            <Grid item xs={3} sm={3} md={1}>

              <Hidden mdDown implementation="css">
                <Button
                  size="medium"
                  color="primary"
                  aria-label="add"
                  className={classes.Add}
                  onClick={() => handleClick(<CreateItem changeView={props.changeView} />)}>
                  <AddIcon className={classes.AddIcon} /> Item</Button>
              </Hidden>
              <Hidden mdUp implementation="css">

                <button size="small" aria-label="add" className={classes.addRespons}>
                  <AddIcon onClick={() => handleClick(<CreateItem changeView={props.changeView} />)}
                    style={{ margin: '4px 2px', textAlign: 'center' }} /></button>
              </Hidden>

            </Grid>
            <Grid item xs={3} sm={3} md={1}>

              <Hidden mdDown implementation="css">
                <Button
                  size="medium"
                  color="primary"
                  aria-label="add"
                  className={classes.Add}
                  onClick={() => handleClick(<ReturnItem />)}>
                  Return </Button>
              </Hidden>
              <Hidden mdUp implementation="css">

                <button size="small" aria-label="add" className={classes.addRespons}>
                  <AddIcon onClick={() => handleClick(<ReturnItem changeView={props.changeView} />)}
                    style={{ margin: '4px 2px', textAlign: 'center' }} /></button>
              </Hidden>

            </Grid>
          </Grid>
          <ColoredLine color="#1e83a9" />
        </Paper>

        {error && <div style={{ color: 'red', marginTop: '3%' }}>no record found</div>
        }
        {table && <div>

          <div className={classes.new1}>

            <Paper className={classes.root}>
              <Divider />
              {/* <div className={classes.search}>
            <div className={classes.searchIcon}>
              <SearchIcon />
            </div>
            <InputBase
              placeholder="Search…"
              classes={{
                root: classes.inputRoot,
                input: classes.inputInput,
              }}
              inputProps={{ 'aria-label': 'search' }}
            />
          </div> */}

              <TableContainer className={classes.container}>
                <Table stickyHeader aria-label="sticky table">
                  <TableHead>
                    <TableRow>
                      {columns.map((column) => (
                        <TableCell
                          key={column.id}
                          align={column.align}
                          style={{ minWidth: column.minWidth }}
                        >
                          {column.label}

                        </TableCell>

                      ))}
                    </TableRow>
                  </TableHead>
                  <TableBody>

                    {rows.slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage).map((row) => {
                      return (
                        <TableRow hover role="checkbox" tabIndex={-1} key={row.code}>
                          {/* {columns.map((column) => {
                    const value = row[column.id];
                    return (
                      <TableCell key={column.id} align={column.align} className={classes.rowdata}>
                        {column.format && typeof value === 'number' ? column.format(value) : value}
                      
                      </TableCell>
                      
                      ticketno,ticketname, equip_type,equip_location, status
                    );
                  })} */}

                          <TableCell className={classes.rowdata}>{row.item_name}</TableCell>
                          <TableCell className={classes.rowdata}>{row.item_code}</TableCell>
                          <TableCell className={classes.rowdata}>{row.brand_name}</TableCell>
                          <TableCell className={classes.rowdata}>{row.model}</TableCell>
                          <TableCell className={classes.rowdata}>{row.title}</TableCell>
                          <TableCell className={classes.rowdata}>{row.docket_no}</TableCell>
                          <TableCell className={classes.rowdata}>{row.quantity}</TableCell>
                          {/* <TableCell className={classes.rowdata}> 
                  <Button variant="outlined" onClick={()=>handleClick(<ViewTicket changeView={props.changeView}/>)} color="primary" className={classes.button}>
                 View</Button>
                </TableCell> */}


                        </TableRow>
                      );
                    })}

                    {/* {
          
             // icon:'edit',
              //tooltip:'Edit Ingredient',
            <Button  onClick={()=>handleClick(<ViewTicket changeView={props.changeView}/>)}>Click</Button>
          
             } */}
                  </TableBody>
                </Table>
              </TableContainer>
              <TablePagination
                rowsPerPageOptions={[10, 25, 100]}
                component="div"
                count={rows.length}
                rowsPerPage={rowsPerPage}
                page={page}
                onChangePage={handleChangePage}
                onChangeRowsPerPage={handleChangeRowsPerPage}
              />
            </Paper>
          </div>
        </div>
        }
      </div>
    )
  }

  return (
    <div>{View()}</div>
  );
}
