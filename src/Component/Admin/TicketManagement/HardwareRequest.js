import React, { useEffect, useState } from "react";
import Avatar from "@material-ui/core/Avatar";
import Button from "@material-ui/core/Button";
import CssBaseline from "@material-ui/core/CssBaseline";

import LockOutlinedIcon from "@material-ui/icons/LockOutlined";
import {
  TextareaAutosize,
  Select,
  MenuItem,
  Typography,
  Container,
  InputBase,
  Grid,
  TextField,
  AppBar,
  Toolbar,
  Paper,
  FormControl,
  InputLabel,
  Hidden,
  Input,
  Link,
} from "@material-ui/core";
import ArrowBackIosIcon from "@material-ui/icons/ArrowBackIos";

import { makeStyles } from "@material-ui/core/styles";
import { getData, postData } from "../../FetchServices";
import StoreItemList from "./StoreItemList";

const ColoredLine = ({ color }) => (
  <hr
    style={{
      color: color,
      backgroundColor: color,
      height: 5,
    }}
  />
);
const useStyles = makeStyles((theme) => ({
  appBar: {
    zIndex: theme.zIndex.drawer + 1,
    transition: theme.transitions.create(["width", "margin"], {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen,
    }),
    backgroundColor: "#16a980d4",
  },
  toolbar: {
    paddingRight: 24, // keep right padding when drawer closed
  },
  paper: {
    marginTop: theme.spacing(4),
    display: "flex",
    flexDirection: "column",
    width: "100%",
    //marginLeft:'10%'
    // alignItems: 'center',
  },
  addRespons: {
    backgroundColor: "#1e83a9",
    color: "white",
    padding: "0px",
    textAlign: "center",
    fontSize: "12px",
    marginTop: "16%",
  },
  avatar: {
    width: 40,
    height: 40,
    marginLeft: "5%",
  },
  form: {
    width: "100%", // Fix IE 11 issue.
    marginTop: theme.spacing(3),
  },
  field: {
    float: "left",
  },
  submit: {
    marginTop: "4%",
    marginBottom: "10%",
    backgroundColor: "#1e83a9",
    color: "#fff",
    //marginLeft:'45%',
    //float:'right',

    "&:hover": {
      backgroundColor: "#1e83a9",
      fontStyle: "bold",
    },
  },

  title: {
    flexGrow: 1,
    textAlign: "center",
    /// marginRight:'5%',
    padding: "10px",
    //backgroundColor:'#1e83a9',
    color: "#1e83a9",
  },
  formControl: {
    marginBottom: theme.spacing(3),
    /// minWidth: 400,
    backgroundColor: "#aaaaaa2e",
    width: "100%",
  },
  input: {
    marginBottom: theme.spacing(3),
    backgroundColor: "#aaaaaa2e",
  },
  file: {
    backgroundColor: "#aaaaaa2e",
    borderColor: "none",
    width: "100%",
  },
  backIcon: {
    fontSize: "medium",
  },
  new: {
    marginLeft: theme.spacing(3),
    marginRight: theme.spacing(2),
  },
  back: {
    backgroundColor: "#1e83a9",
    color: "#fff",
    marginTop: "8%",
    "&:hover": {
      // color:'primary',
      fontStyle: "bold",
      backgroundColor: "#1e83a9",
    },
  },
}));

export default function CreateStore(props) {
  const classes = useStyles();

  //  const [equipmentData, setEquipmentData] = React.useState([])
  const equipmentData = props.equipmentData;

  console.log("Equipment :", equipmentData);
  const BaseURL = "http://83.136.219.147/trafiksol/public/api/v1";

  const postData1 = async (url, body) => {
    var response = await fetch(`${BaseURL}/${url}`, {
      method: "POST",
      mode: "cors",
      headers: {
        // "Authorization":"Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOlwvXC9hYXJvZ3lhaW5kaWEuaW5cL3RyYWZpa3NvbFwvcHVibGljXC9hcGlcL3YxXC9sb2dpbiIsImlhdCI6MTU5OTEyMzA1MiwiZXhwIjoxNjAxNzE1MDUyLCJuYmYiOjE1OTkxMjMwNTIsImp0aSI6IlJQVmttMHhmcFRzZHpHaGoiLCJzdWIiOjEsInBydiI6Ijg3ZTBhZjFlZjlmZDE1ODEyZmRlYzk3MTUzYTE0ZTBiMDQ3NTQ2YWEifQ.DFp2qevRXpL5DKJNSyrryVAnGD0BWXm-GiMVQeNq5LQ",
        Authorization: "Bearer " + token,
        "Content-Type": "application/json; charset=utf-8",
      },
      body: JSON.stringify(body),
    });
    var result = await response.json();
    //console.log('show:', result);
    return result;
  };

  let rec = JSON.parse(localStorage.getItem("LOGIN"));
  console.log(rec);
  let token = rec.data.user.token;
  let siteId = JSON.parse(localStorage.getItem("SITEID"));

  const [HardwareList, setHardwareList] = React.useState([]);
  const [ManpowerList, setManpowerList] = React.useState([]);

  const fetchHardwareList = async () => {
    var body = {
      site_id: siteId,
    };
    let result = await postData1("tickets/gethwrlist", body);
    console.log(result);
    setHardwareList(result.data);
  };

  // const fetchEquipmentList = async () => {

  //   var body = {
  //     "site_id": siteId,
  //     "type": 1
  //   }
  //   console.log(body)

  //   let result = await postData1('stores/list', body)
  //   console.log(result)
  //   if (result.status == 1) {
  //     setEquipmentData(result.data)
  //   }

  // }

  const fetchManpowerList = async () => {
    var body = {
      type: "all",
      site_id: siteId,
    };
    let result = await postData1("users/getManPower", body);
    console.log(result);
    setManpowerList(result.data.data);
  };

  useEffect(() => {
    fetchHardwareList();
    fetchManpowerList();
    // fetchEquipmentList()
  }, []);

  const [TicketId, setTicketId] = React.useState([]);
  const [subject, setSubject] = React.useState("");
  const [created_at, setCreatedAt] = React.useState("");
  const [equip_required, setEquipRequired] = React.useState("");
  const [reason, setReason] = React.useState("");
  const [quantity, setQuantity] = React.useState("");
  const [refrnc, setRefrncNo] = React.useState("");
  const [requestBy, setRequestedBy] = React.useState("");
  const [assignTo, setAssignedTo] = React.useState("");
  const [requestId, setRequestId] = React.useState("");
  console.log(equip_required);
  const handleChangeTicket = (event) => {
    setTicketId(event.target.value);
    setSubject(event.target.value.subject);
    setCreatedAt(event.target.value.created_at);
    setEquipRequired(event.target.value.equipment_required);
    setReason(event.target.value.reason);
    setQuantity(event.target.value.quantity);
    setRefrncNo(event.target.value.ref_no);
    setRequestedBy(event.target.value.username);
    setRequestId(event.target.value.id);
  };
  //  const handleChangeEquipment=(event)=>{

  //    document.write([event.target.name])

  //  }

  const hardwareAssign = async () => {
    var body = {
      "id": requestId,
      "user_id": assignTo,
      "equipment_id":equip_required
    };
    let result = await postData1("tickets/assignHardware", body);
    console.log("myresult :", result);
    if (result.status == 1) {
      alert("Hardware is assigned");
    } else {
      alert("Error:",result.message);
    }
  };
  const handleClick = (view) => {
    props.changeView(view);
  };

  return (
    console.log(TicketId),
    (
      <div>
        <Container component="main">
          <CssBaseline />

          <div className={classes.paper}>
            <Paper>
              <Grid container spacing={2}>
                <Grid item sm={2} xs={3} md={2}>
                  <Hidden mdDown implementation="css">
                    <Button
                      className={classes.back}
                      onClick={() =>
                        handleClick(
                          <StoreItemList changeView={props.changeView} />
                        )
                      }
                    >
                      <ArrowBackIosIcon className={classes.backIcon} />
                      Back
                    </Button>
                  </Hidden>
                  <Hidden mdUp implementation="css">
                    <button
                      size="small"
                      aria-label="add"
                      className={classes.addRespons}
                    >
                      <ArrowBackIosIcon
                        onClick={() =>
                          handleClick(
                            <StoreItemList changeView={props.changeView} />
                          )
                        }
                      />
                    </button>
                  </Hidden>
                </Grid>
                <Grid item sm={8} xs={8} md={8}>
                  <Typography
                    component="h1"
                    variant="h6"
                    color="inherit"
                    noWrap
                    className={classes.title}
                  >
                    Hardware
                  </Typography>
                </Grid>
              </Grid>

              <ColoredLine color="#1e83a9" />

              <div className={classes.new}>
                <form className={classes.form} noValidate>
                  <Grid container spacing={2}>
                    <Grid item xs={12} sm={4} md={2}>
                      <Typography className={classes.field}>
                        Ticket Id
                      </Typography>
                    </Grid>
                    <Grid item xs={12} sm={8} md={3}>
                      <FormControl fullWidth className={classes.formcontrol}>
                        <TextField
                          className={classes.input}
                          id="outlined-size-small"
                          defaultValue="Small"
                          size="small"
                          select
                          value={TicketId}
                          onChange={(event) => handleChangeTicket(event)}
                          // variant="outlined"
                        >
                          {HardwareList.map((option) => (
                            <MenuItem value={option}>
                              {option.ticket_id}
                            </MenuItem>
                          ))}
                        </TextField>
                      </FormControl>
                    </Grid>
                  </Grid>

                  <Grid container spacing={2}>
                    <Grid item xs={12} sm={12} md={6}>
                      <Grid container spacing={2}>
                        <Grid item xs={12} sm={4} md={4}>
                          <Typography className={classes.field}>
                            Ticket Subject
                          </Typography>
                        </Grid>
                        <Grid item xs={12} sm={8} md={6}>
                          <TextField
                            disabled
                            id="standard-disabled"
                            fullWidth
                            className={classes.input}
                            value={subject}
                          />
                          {/* <Typography className={classes.input}>{subject}</Typography> */}
                        </Grid>
                      </Grid>
                    </Grid>

                    <Grid item xs={12} sm={12} md={6}>
                      <Grid container spacing={2}>
                        <Grid item xs={12} sm={4} md={4}>
                          <Typography className={classes.field}>
                            Created At
                          </Typography>
                        </Grid>
                        <Grid item xs={12} sm={8} md={6}>
                          <TextField
                            disabled
                            id="standard-disabled"
                            fullWidth
                            className={classes.input}
                            value={created_at}
                          />
                          {/* <Typography className={classes.input}>{created_at}</Typography> */}
                        </Grid>
                      </Grid>
                    </Grid>
                  </Grid>

                  <Grid container spacing={2}>
                    <Grid item xs={12} sm={12} md={6}>
                      {/* <Grid container spacing={2}>
                      <Grid item xs={12} sm={4} md={4}>
                        <Typography className={classes.field}>Equipment</Typography>
                      </Grid>
                      <Grid item xs={12} sm={8} md={6}>
                        <TextField disabled id="standard-disabled" fullWidth className={classes.input} value={equip_required} />
                       <Typography className={classes.input}>{equip_required}</Typography> 
                      </Grid>
                    </Grid> */}

                      <Grid container spacing={2}>
                        <Grid item xs={12} sm={4} md={4}>
                          <Typography className={classes.field}>
                            Equipment
                          </Typography>
                        </Grid>
                        <Grid item xs={12} sm={8} md={6}>
                          <FormControl
                            fullWidth
                            className={classes.formcontrol}
                          >
                            <TextField
                              className={classes.input}
                              id="outlined-size-small"
                              defaultValue="Small"
                              size="small"
                              select
                              value={equip_required}
                              //onChange={(event) => handleChangeEquipment(event)}
                              onChange={(event) =>
                                setEquipRequired(event.target.value)
                              }
                              // variant="outlined"
                            >
                              {equipmentData.map((option) => {
                                return (
                                  <MenuItem value={option.id}>
                                    {option.item_name}
                                  </MenuItem>
                                );
                              })}
                            </TextField>
                          </FormControl>
                        </Grid>
                      </Grid>

                      <Grid container spacing={2}>
                        <Grid item xs={12} sm={4} md={4}>
                          <Typography className={classes.field}>
                            Quantity
                          </Typography>
                        </Grid>
                        <Grid item xs={12} sm={8} md={6}>
                          <TextField
                            disabled
                            id="standard-disabled"
                            fullWidth
                            className={classes.input}
                            value={quantity}
                          />
                          {/* <Typography className={classes.input}>{quantity}</Typography> */}
                        </Grid>
                      </Grid>

                      <Grid container spacing={2}>
                        <Grid item xs={12} sm={4} md={4}>
                          <Typography className={classes.field}>
                            Assigned To
                          </Typography>
                        </Grid>
                        <Grid item xs={12} sm={8} md={6}>
                          <FormControl
                            fullWidth
                            className={classes.formcontrol}
                          >
                            <TextField
                              className={classes.input}
                              id="outlined-size-small"
                              defaultValue="Small"
                              size="small"
                              select
                              value={assignTo}
                              onChange={(event) =>
                                setAssignedTo(event.target.value)
                              }
                              // variant="outlined"
                            >
                              {ManpowerList.map((option) => (
                                <MenuItem value={option.id}>
                                  {option.name}
                                </MenuItem>
                              ))}
                            </TextField>
                          </FormControl>
                        </Grid>
                      </Grid>
                    </Grid>

                    <Grid item xs={12} sm={12} md={6}>
                      <Grid container spacing={2}>
                        <Grid item xs={12} sm={4} md={4}>
                          <Typography className={classes.field}>
                            Reason
                          </Typography>
                        </Grid>
                        <Grid item xs={12} sm={8} md={6}>
                          <TextField
                            disabled
                            id="standard-disabled"
                            fullWidth
                            className={classes.input}
                            value={reason}
                          />
                          {/* <Typography className={classes.input}>{reason}</Typography> */}
                        </Grid>
                      </Grid>

                      <Grid container spacing={2}>
                        <Grid item xs={12} sm={4} md={4}>
                          <Typography className={classes.field}>
                            Reference No.
                          </Typography>
                        </Grid>
                        <Grid item xs={12} sm={8} md={6}>
                          <TextField
                            disabled
                            id="standard-disabled"
                            fullWidth
                            className={classes.input}
                            value={refrnc}
                          />
                          {/* <Typography className={classes.input}>{refrnc} </Typography> */}
                        </Grid>
                      </Grid>

                      <Grid container spacing={2}>
                        <Grid item xs={12} sm={4} md={4}>
                          <Typography className={classes.field}>
                            Requested By
                          </Typography>
                        </Grid>
                        <Grid item xs={12} sm={8} md={6}>
                          <TextField
                            disabled
                            id="standard-disabled"
                            fullWidth
                            className={classes.input}
                            value={requestBy}
                          />
                          {/* <Typography className={classes.input}>{requestBy}</Typography> */}
                        </Grid>
                      </Grid>

                      {/* <Grid container spacing={2}>
            <Grid item xs={12} sm={6} md={4}>
              <Typography>Docket No.</Typography>
              </Grid>
              <Grid item xs={12} sm={6} md={6}>
              <Input
               value={DocketNo}
               onChange={(event)=>setDocketNo(event.target.value)}
            className={classes.input}
            placeholder="             Docket No." 
            required
            fullWidth 
            type="text"
            inputProps={{ 'aria-label': 'description' }} />
              </Grid>
              </Grid> */}
                    </Grid>
                  </Grid>

                  <Button
                    onClick={hardwareAssign}
                    variant="contained"
                    color="green"
                    className={classes.submit}
                  >
                    Assign
                  </Button>
                </form>
              </div>
            </Paper>
          </div>
        </Container>
      </div>
    )
  );
}
