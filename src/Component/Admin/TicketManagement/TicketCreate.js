import React, {useEffect} from 'react';
import Avatar from '@material-ui/core/Avatar';
import Button from '@material-ui/core/Button';
import CssBaseline from '@material-ui/core/CssBaseline';

import LockOutlinedIcon from '@material-ui/icons/LockOutlined';
import {TextareaAutosize ,Select,MenuItem,Typography,Container,Backdrop,Grid,TextField,AppBar,Toolbar,Paper
,FormControl,Fade,Modal,Input}
 from '@material-ui/core';

import { makeStyles } from '@material-ui/core/styles';
import {postDataAndImage,getData } from '../../FetchServices';
import OpenTicket from './OpenTicket';

const ColoredLine = ({ color }) => (
  <hr
      style={{
          color: color,
          backgroundColor: color,
          height: 5
      }}
  />
);


const useStyles = makeStyles((theme) => ({
    appBar: {
        zIndex: theme.zIndex.drawer + 1,
        transition: theme.transitions.create(['width', 'margin'], {
          easing: theme.transitions.easing.sharp,
          duration: theme.transitions.duration.leavingScreen,

        }),
        backgroundColor:'#16a980d4',
      },
      toolbar: {
        paddingRight: 24, // keep right padding when drawer closed
      },
  paper: {
    marginTop: theme.spacing(4),
    display: 'flex',
    flexDirection: 'column',
    width:'100%',
    //marginLeft:'10%'
   // alignItems: 'center',
  },
 avatar: {
    width: 40,
    height: 40,
    marginLeft:'5%'
  },
  form: {
    width: '100%', // Fix IE 11 issue.
    marginTop: theme.spacing(3),
  },
  submit: {
    marginTop: '4%',
    marginBottom: '2%',
    backgroundColor:'#1e83a9',
    color:'#fff',
    //width:'40%'
   // float:'right',
   // marginLeft:'45%',

    '&:hover': {
  backgroundColor:'#1e83a9',
  fontStyle:"bold"
  },
  },

  title: {
    flexGrow: 1,
    textAlign:'center',
    padding:'10px',
   // backgroundColor:'#1e83a9',
    color:'#1e83a9',
  },
  title1: {
    flexGrow: 1,
    textAlign:'center',
    padding:'10px',
    //backgroundColor:'#1ab558d4',
    color:'#1e83a9',
  },
  formControl: {
   // marginBottom: theme.spacing(3),
   /// minWidth: 400,
    //backgroundColor:'#aaaaaa2e',
    width:'100%',

  },
   input:{
    marginBottom: theme.spacing(3),
    backgroundColor:'#aaaaaa2e'
},
file:{
    backgroundColor:'#aaaaaa2e',
    borderColor:'none',
    width:'100%'
},
field: {
  float:'left',  
},
new:{
  marginLeft: theme.spacing(3),
  marginRight: theme.spacing(2),
},
image: {
  display: 'none',
},
upload:{
  backgroundColor:'#1e83a9',
  color:'#fff',
  borderRadius:'0px',
  boxShadow:'0px',
  height:'33px',

  '&:hover': {
    // color:'primary',
     fontStyle:"bold",
     backgroundColor:'#1e83a9',
     },
},
modal: {
  display: 'flex',
  alignItems: 'center',
  justifyContent: 'center',
},
content: {
  backgroundColor: theme.palette.background.paper,
  border: '2px solid #000',
  boxShadow: theme.shadows[5],
  padding: theme.spacing(2, 4, 3),
},
back:{
  backgroundColor:'#1e83a9',
  color:'#fff',
  marginTop:'12%',

  '&:hover': {
 // color:'primary',
  fontStyle:"bold",
  backgroundColor:'#1e83a9',
  },
  },

}));

export default function TicketCreate(props) {
  const classes = useStyles();

  const handleClick=(view)=>{
    props.changeView(view)
    }

  const [TicketSubject,setTicketSubject] = React.useState('')
  const [categoryId,setCategoryId] = React.useState('')
  const [StretchPoint,setStretchPoint] = React.useState('')
  const [IssueTypeId,setIssueTypeId] = React.useState('')
  const [equip_id,setEquipId] = React.useState('')
  const [priority,setPriority] = React.useState('')
  const [description,setDescription] = React.useState('')
  const [getPicture,setPicture] = React.useState('')
  const [getPicturePath,setPicturePath] = React.useState('')

  const [clearText,setclearText] = React.useState('')

  const [onn, setOnn] = React.useState(false);

  const handleOff = () => {
    setOnn(false);
  };

  const BaseURL="http://83.136.219.147/trafiksol/public/api/v1"

const postData1=async(url,body)=>{
    var response=await fetch(`${BaseURL}/${url}`,{
    method:"POST",
    mode:"cors",
    headers:{
     // "Authorization":"Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOlwvXC9hYXJvZ3lhaW5kaWEuaW5cL3RyYWZpa3NvbFwvcHVibGljXC9hcGlcL3YxXC9sb2dpbiIsImlhdCI6MTU5OTEyMzA1MiwiZXhwIjoxNjAxNzE1MDUyLCJuYmYiOjE1OTkxMjMwNTIsImp0aSI6IlJQVmttMHhmcFRzZHpHaGoiLCJzdWIiOjEsInBydiI6Ijg3ZTBhZjFlZjlmZDE1ODEyZmRlYzk3MTUzYTE0ZTBiMDQ3NTQ2YWEifQ.DFp2qevRXpL5DKJNSyrryVAnGD0BWXm-GiMVQeNq5LQ",
     "Authorization": "Bearer " + token,
     "Content-Type": "application/json; charset=utf-8"},
     body:JSON.stringify(body)})
     var result=await response.json()
  return result
}

  let rec=JSON.parse(localStorage.getItem('LOGIN'))
   console.log(rec)
   let token=rec.data.user.token;


  let siteId=JSON.parse(localStorage.getItem('SITEID'))

  const createTicket=async()=>{

    if(TicketSubject!='' && categoryId!='' && StretchPoint!='' && IssueTypeId!='' && equip_id!='' 
    && priority!='' && description!='' && getPicture!=''){

    const formData=new FormData()
    formData.append('subject',TicketSubject)
    formData.append('ticket_category_id',categoryId)
    formData.append('stretch_point',StretchPoint)
    formData.append('issue_type_id',IssueTypeId)
    formData.append('equipment_id',equip_id)
    formData.append('priority',priority)
    formData.append('description',description)
    formData.append('file[]',getPicture)
    formData.append('site_id',siteId)
    
    
    const config={headers:{
      //'Authorization':'Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOlwvXC9hYXJvZ3lhaW5kaWEuaW5cL3RyYWZpa3NvbFwvcHVibGljXC9hcGlcL3YxXC9sb2dpbiIsImlhdCI6MTU5OTEyMzA1MiwiZXhwIjoxNjAxNzE1MDUyLCJuYmYiOjE1OTkxMjMwNTIsImp0aSI6IlJQVmttMHhmcFRzZHpHaGoiLCJzdWIiOjEsInBydiI6Ijg3ZTBhZjFlZjlmZDE1ODEyZmRlYzk3MTUzYTE0ZTBiMDQ3NTQ2YWEifQ.DFp2qevRXpL5DKJNSyrryVAnGD0BWXm-GiMVQeNq5LQ',
      'Authorization' : "Bearer " + token,
      'content-type':'multipart/form-data'}}
    const result=await postDataAndImage('tickets/create',formData,config)

    console.log(formData)
    console.log(result)
      clearValues()
     // setOnn(true)
      }
      else if (TicketSubject!='' && categoryId!='' && StretchPoint!='' && IssueTypeId!='' && equip_id!='' 
      && priority!='' && description!='' && getPicture==''){
         
        const formData=new FormData()
    formData.append('subject',TicketSubject)
    formData.append('ticket_category_id',categoryId)
    formData.append('stretch_point',StretchPoint)
    formData.append('issue_type_id',IssueTypeId)
    formData.append('equipment_id',equip_id)
    formData.append('priority',priority)
    formData.append('description',description)
    formData.append('file[]','')
    formData.append('site_id',siteId)
    
    
    const config={headers:{
      'Authorization' : "Bearer " + token,
      'content-type':'multipart/form-data'}}
    const result=await postDataAndImage('tickets/create',formData,config)

    console.log(formData)
    console.log(result)
      clearValues()
      setOnn(true)
      }
     else{
       alert('All Fields are Required...')
     }}  
           
     

   const clearValues=()=>{

    setTicketSubject('')
    setCategoryId('')
    setStretchPoint('')
    setIssueTypeId('')
    setEquipId('')
    setPriority('')
    setDescription('')
    setPicturePath('')
    setPicture('')

          }

          const handlePicture=(event)=>{
            setPicturePath(URL.createObjectURL(event.target.files[0]))
            setPicture(event.target.files[0])
           }            

          const [issue,setIssue]=React.useState([])

            const fetchIssue=async()=>{

            let result= await getData('tickets/getIssueTypeList')
               console.log(result)
               setIssue(result.data)
             }

  const [getList,setList]=React.useState([])

    const fetchCategory=async()=> {
      let result= await getData('tickets/category')
      console.log(result)

      setList(result.data);
    }

    const [getEquipment,setEquipment]=React.useState([])

    const fetchEquipment=async()=> {
      var body ={
        "site_id":siteId
      }

      let result= await postData1('getEquipmentBySite',body)
      console.log(result)

      setEquipment(result.data);
    }

    useEffect(()=>{
      fetchIssue()
      fetchCategory()
      fetchEquipment()
  },[])


  const [charsLeft,setCharsLeft] = React.useState('')

  const handleDescription=(event)=>{
   setDescription(event.target.value.substring(0,400))
   setCharsLeft(description.length)
  }
   const handleEquip =(item)=>{
    setEquipId(item.equipment_id)
    setStretchPoint(item.chainage)
   }

  return (

    console.log(siteId),
    <Container component="main">
      <CssBaseline />

      <div className={classes.paper}>

      <Paper>
        {/* {displayList()} */}

      <Typography component="h1" variant="h6" color="inherit" noWrap className={classes.title}>
            Service Ticket
            {/* <Typography component="h6" variant="h12" color="inherit" noWrap className={classes.title1}>
            Please Provide the details of the problem
          </Typography> */}
          </Typography>
          <ColoredLine color="#1e83a9" />
       <div className={classes.new}>
        <form className={classes.form} noValidate>
          <Grid container spacing={2}>

            <Grid item xs={12} sm={12} md={6}>

            <Grid container spacing={2}>
            <Grid item xs={12} sm={4} md={4}>
              <Typography className={classes.field}>Ticket Subject*</Typography>
              </Grid>
              <Grid item xs={12} sm={8} md={6}>
              <Input
            className={classes.input}
            placeholder="             Ticket Subject"
            required
            fullWidth
            name="subject"
            type="text"
            value={TicketSubject}
            onChange={(event)=>setTicketSubject(event.target.value)}
            inputProps={{ 'aria-label': 'description' }} />
              </Grid>
              </Grid>

              <Grid container spacing={2}>
            <Grid item xs={12} sm={4} md={4}>
              <Typography className={classes.field}>Ticket Category*</Typography>
              </Grid>
              <Grid item xs={12} sm={8} md={6}>


      <FormControl fullWidth className={classes.formcontrol}>

        <TextField
       className={classes.input}
        id="outlined-size-small"
        defaultValue="Small"
        size="small"
        select
        value={categoryId}
        onChange={(event)=>setCategoryId(event.target.value)}
       // variant="outlined"
        >
         {getList.map(option=>(
         <MenuItem key={option.id} value={option.id}>
         {option.name}
         </MenuItem>
        ))}
        </TextField>
      </FormControl>
              </Grid>
              </Grid>


              <Grid container spacing={2}>
            <Grid item xs={12} sm={4} md={4}>
              <Typography className={classes.field}>Chainage No.*</Typography>
              </Grid>
              <Grid item xs={12} sm={8} md={6}>
              <Input
            className={classes.input}
            placeholder="             Chainage No."
            required
            fullWidth
            name="stretch"
            type="text"
            value={StretchPoint}
           // onChange={(event)=>setStretchPoint(event.target.value)}
            inputProps={{ 'aria-label': 'description' }} />
              </Grid>
              </Grid>

              {/* <Grid container spacing={2}>
            <Grid item xs={12} sm={6} md={4}>
              <Typography>Equipment Location</Typography>
              </Grid>
              <Grid item xs={12} sm={6} md={6}>
              <FormControl className={classes.formControl}>

        <NativeSelect
          defaultValue={0}
          inputProps={{
            name: 'name',
            id: 'uncontrolled-native',
          }}
        >
         <option value={0}></option>
          <option value={10}>Ten</option>
          <option value={20}>Twenty</option>
          <option value={30}>Thirty</option>
        </NativeSelect>

      </FormControl>
              </Grid>
              </Grid> */}
            </Grid>



           <Grid item xs={12} sm={12} md={6}>

            <Grid container spacing={2}>
            <Grid item xs={12} sm={4} md={4}>
              <Typography className={classes.field}>Issue Type*</Typography>
              </Grid>
              <Grid item xs={12} sm={8} md={6}>


     <FormControl fullWidth className={classes.formcontrol}>
        <TextField

       className={classes.input}
        id="outlined-size-small"
        defaultValue="Small"
        size="small"
        select
        value={IssueTypeId}
        onChange={(event)=>setIssueTypeId(event.target.value)}

       // variant="outlined"
        >
         {issue.map(item=>(
         <MenuItem key={item.id} value={item.id}>
         {item.name}
         </MenuItem>
        ))}
        </TextField>
      </FormControl>
              </Grid>
              </Grid>
              {/* <Grid container spacing={2}>
            <Grid item xs={12} sm={6} md={4}>
              <Typography>Ticket SubCategory</Typography>
              </Grid>
              <Grid item xs={12} sm={6} md={6}>
              <FormControl className={classes.formControl}>

        <NativeSelect
          defaultValue={0}
          inputProps={{
            name: 'name',
            id: 'uncontrolled-native',
          }}
        >
          <option value={0}></option>
          <option value={20}>Twenty</option>
          <option value={30}>Thirty</option>
        </NativeSelect>

      </FormControl>
              </Grid>
              </Grid>
               */}
              <Grid container spacing={2}>
            <Grid item xs={12} sm={4} md={4}>
              <Typography className={classes.field}>Equipment Name*</Typography>
              </Grid>
              <Grid item xs={12} sm={8} md={6}>
              <FormControl className={classes.formControl}>

        
              <TextField
       className={classes.input}
        id="outlined-size-small"
        defaultValue="Small"
        size="small"
        select
        value={equip_id}
        //onChange={(event)=>setEquipId(event.target.value)}
       // variant="outlined"
        >
         {getEquipment.map(item=>(
         <MenuItem value={item.equipment_id} onClick={()=>handleEquip(item)}>
         {item.equipment_name} - ({item.chainage})
         </MenuItem>
        ))}
       
        </TextField>
      </FormControl>
              </Grid>
              </Grid>

        <Grid container spacing={2}>
            <Grid item xs={12} sm={4} md={4}>
              <Typography className={classes.field}>Priority*</Typography>
              </Grid>
              <Grid item xs={12} sm={8} md={6}>
              <FormControl className={classes.formControl}>

              <TextField
       className={classes.input}
        id="outlined-size-small"
        defaultValue="Small"
        size="small"
        select
        value={priority}
          onChange={(event)=>setPriority(event.target.value)}
       // variant="outlined"
        >
        
        <MenuItem value='high'>High</MenuItem>
          <MenuItem value='medium'>Medium</MenuItem>
          <MenuItem value='low'>Low</MenuItem>
        </TextField>

      </FormControl>



              </Grid>
              </Grid>


            </Grid>


          </Grid>

          <Grid container>

<Grid item xs={12} sm={12}>
    <Grid container spacing={4}>
    <Grid item xs={12} sm={4} md={2}>
    <Typography  className={classes.field}>Image</Typography>
    </Grid>
    <Grid item xs={12} sm={5} md={7}>
    <Input
         value={getPicturePath}
       // onChange={(event)=>handlePicture(event)}
       disabled 
       id="standard-disabled"
            className={classes.input}
            placeholder="Image" 
            required
            fullWidth 
            type="text"
            inputProps={{ 'aria-label': 'description' }} />
       </Grid>
    <Grid item xs={12} sm={1} md={2}>
    <input
        required
        accept="image/*"
        className={classes.image}
        id="Picture"
        multiple
        type="file"
        onChange={(event)=>handlePicture(event)}
        fullWidth
      />
      <label htmlFor="Picture">
        <Button variant="contained" component="span" className={classes.upload} >
         Upload
        </Button>
      </label>
    </Grid>

    {/* <Grid item xs={12} sm={6} md={4}>
    { <Avatar variant="square" alt="Picture" src={getPicturePath} className={classes.bigAvatar} value={getPicturePath} style={{width:120,height:100}}/> }
      </Grid> */}
</Grid>

 <Grid container spacing={2}>
          <Grid item xs={12} sm={4} md={2}>
          <Typography className={classes.field}>Description*</Typography>
          </Grid>
          <Grid item xs={12} sm={8} md={9}>
          <TextField
               className={classes.file}
              fullWidth
              required
              value={description}
              onChange={(event)=>handleDescription(event)}
             // onChange={(event)=>setDescription(event.target.value.substring(0,200))}
              id="standard-multiline-static"
         // label="Multiline"
          multiline
          rows={4}
          placeholder="Max 400 Words"
        /><Typography style={{textAlign:'right'}}>{charsLeft}/400</Typography>
          </Grid>
   </Grid>
   </Grid>
   </Grid>

   {/* ---Modal for confirmation to create more ticket */}

   <Modal
        aria-labelledby="transition-modal-title"
        aria-describedby="transition-modal-description"
        className={classes.modal}
        open={onn}
        onClose={handleOff}
        closeAfterTransition
        BackdropComponent={Backdrop}
        BackdropProps={{
          timeout: 500,
        }}
      > 
        <Fade in={onn}>
          <div className={classes.content}>
            <h2 id="transition-modal-title">Do you Want to create more ticket ?</h2>
            
            <Grid item xs={4} md={2}>
            <Button className={classes.back} onClick={handleOff}>
        yes</Button> </Grid>
        <Grid item xs={4} md={2}>
            <Button className={classes.back}
        onClick={()=>handleClick(<OpenTicket changeView={props.changeView}/>)}>
        No</Button> 
        </Grid>
           
           </div>
        </Fade>
      </Modal>

          <Button

             // type="submit"

              variant="contained"
              color="green"
              className={classes.submit}
              onClick={createTicket}
            >
              Create
            </Button>

        </form>
        </div>
        </Paper>
      </div>

    </Container>
  );
}