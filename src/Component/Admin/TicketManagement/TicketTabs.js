import React from 'react';
import PropTypes from 'prop-types';
import { makeStyles } from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import Typography from '@material-ui/core/Typography';
import Box from '@material-ui/core/Box';
import OpenTicket from '../TicketManagement/OpenTicket';
import AnsweredTicket from '../TicketManagement/AnsweredTicket';
import MyTicket from '../TicketManagement/MyTicket';
import ClosedTicket from '../TicketManagement/ClosedTicket';

function TabPanel(props) {
  const { children, value, index, ...other } = props;

  return (
    <div
      role="tabpanel"
      hidden={value !== index}
      id={`scrollable-auto-tabpanel-${index}`}
      aria-labelledby={`scrollable-auto-tab-${index}`}
      {...other}
    >
      {value === index && (
        <Box p={3}>
          <Typography>{children}</Typography>
        </Box>
      )}
    </div>
  );
}

TabPanel.propTypes = {
  children: PropTypes.node,
  index: PropTypes.any.isRequired,
  value: PropTypes.any.isRequired,
};

function a11yProps(index) {
  return {
    id: `scrollable-auto-tab-${index}`,
    'aria-controls': `scrollable-auto-tabpanel-${index}`,
  };
}

const useStyles = makeStyles((theme) => ({
  root: {
    //flexGrow: 1,
    width: '98%',
    backgroundColor: theme.palette.background.paper,
  },
  
}));

export default function ScrollableTabsButtonAuto() {
  const classes = useStyles();
  const [value, setValue] = React.useState(0);

  const handleChange = (event, newValue) => {
    setValue(newValue);
  };

  return (
    <div className={classes.root}>
      <AppBar position="static" color="default" className={classes.head}>
        <Tabs
          value={value}
          onChange={handleChange}
          indicatorColor="primary"
          textColor="primary"
          variant="scrollable"
          scrollButtons="auto"
          aria-label="scrollable auto tabs example"
        >
          <Tab label="Open" {...a11yProps(0)} />
          <Tab label="Answered" {...a11yProps(1)} />
          <Tab label="MyTicket" {...a11yProps(2)} />
          <Tab label="Closed" {...a11yProps(3)} />
          
        </Tabs>
      </AppBar>
      <TabPanel value={value} index={0}>
      <OpenTicket/>
      </TabPanel>
      <TabPanel value={value} index={1}>
      <AnsweredTicket/>
      </TabPanel>
      <TabPanel value={value} index={2}>
      <MyTicket/>
      </TabPanel>
      <TabPanel value={value} index={3}>
      <ClosedTicket/>
      </TabPanel>
     
    </div>
  );
}
