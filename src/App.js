import React from 'react';
import AdminRouter from './Component/AdminRouter';
import './App.css';

function App() {
  return (
    <div className="App">
      <AdminRouter/>
    </div>
  );
}

export default App;